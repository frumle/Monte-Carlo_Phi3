module process_info_type

 ! Z0 = EIGENVECS*Y0

  type process_info_bundle
   integer:: n
   real(8):: sqrt_s_gev
   real(8):: cross_section
   real(8), allocatable:: Z0(:),Px0(:),Py0(:)
   real(8), allocatable:: zDisp(:),zyTransformMatrix(:,:)
!~    real(8), allocatable:: D2px(:,:),D2py(:,:)
  end type process_info_bundle
  
contains
 
subroutine alloc_bundle(bundle,n)
  integer:: n
  type(process_info_bundle):: bundle
  
  bundle%n = n
  if(n.ne.0) then
    allocate(bundle%Z0(1:n),bundle%Px0(1:n),bundle%Py0(1:n))
    allocate(bundle%zDisp(1:n),bundle%zyTransformMatrix(1:n,1:n))
!~     allocate(bundle%D2y(1:n,1:n),bundle%D2px(1:n+1,1:n+1),bundle%D2py(1:n+1,1:n+1))
  endif
end subroutine alloc_bundle
 
subroutine recycle_bundle(bundle)
  type(process_info_bundle):: bundle
  if((bundle%n.ne.0).and.(allocated(bundle%Z0))) then
    deallocate(bundle%Z0,bundle%Px0,bundle%Py0)
    deallocate(bundle%zDisp,bundle%zyTransformMatrix)
!~     deallocate(bundle%D2y,bundle%D2px,bundle%D2py)
  endif
end subroutine recycle_bundle

subroutine print_short(bundle)
 type(process_info_bundle):: bundle
 print*, "n = ", bundle%n, "sqrt_s = ", bundle%sqrt_s_gev, "sigma = ", bundle%cross_section
end subroutine print_short

subroutine write_bundle(device, bundle)
 integer:: device
 type(process_info_bundle):: bundle
 
 ! Format:
 ! #bundle
 ! n, sqrt_s, sigma
 ! z0
 ! zDisp
 ! zyTransformMatrix
 
  write(device,*) "#bundle"
  write(device,*) bundle%n, bundle%sqrt_s_gev, bundle%cross_section
  write(device,*) bundle%Z0(1:bundle%n)
  write(device,*) bundle%zDisp(1:bundle%n)
  do i = 1,bundle%n
   write(device,*) bundle%zyTransformMatrix(i,1:bundle%n)
  enddo
end subroutine write_bundle

subroutine read_bundle(device, bundle)
 integer:: device
 type(process_info_bundle):: bundle
 
 ! Format:
 ! #bundle
 ! n, sqrt_s, sigma
 ! z0
 ! zDisp
 ! zyTransformMatrix
 
 read(device,*)
 read(device,*) bundle%n, bundle%sqrt_s_gev, bundle%cross_section
 read(device,*) bundle%Z0(1:bundle%n)
 read(device,*) bundle%zDisp(1:bundle%n)
 do i = 1,bundle%n
  read(device,*) bundle%zyTransformMatrix(i,1:bundle%n)
 enddo
 
end subroutine read_bundle


end module process_info_type
