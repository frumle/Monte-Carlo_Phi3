subroutine sub_d2psmall(n,X,d2p0,d2p_par)
  use constants_module
  implicit none
 
  ! input
  integer:: n
  real(8):: X(1:3*n+2)
  
  ! output
  real(8):: d2p0(1:n,1:3*n+2,1:3*n+2),d2p_par(1:n,1:3*n+2,1:3*n+2)

  ! temp
  real(8):: px(1:n),py(1:n),y(1:n)
  integer:: c,nomper1,nomper2
  real(8):: m_c,dm_c_nomper1,dm_c_nomper2,d2m_p1,d2m_p2,d2m 
  real(8):: coshy,sinhy

y=X(1:n)
px=X(n+1:2*n)
py=X(2*n+1:3*n)


!######################## d2p0 po vsem
do c = 1,n
  m_c = sqrt(SQR_MPI+px(c)*px(c)+py(c)*py(c))
  coshy = cosh(y(c))
  sinhy = sinh(y(c))
  do nomper1=1,3*n+2
   do nomper2=1,3*n+2

    dm_c_nomper1 = (px(c)*delta(nomper1,n+c)+py(c)*delta(nomper1,2*n+c))
    dm_c_nomper1 = dm_c_nomper1/m_c
  
    dm_c_nomper2 = (px(c)*delta(nomper2,n+c)+py(c)*delta(nomper2,2*n+c))
    dm_c_nomper2 = dm_c_nomper2/m_c
 
    d2m_p1 = (delta(nomper1,n+c)*delta(nomper2,n+c)+delta(nomper1,2*n+c)*delta(nomper2,2*n+c))
    d2m_p1 = d2m_p1/m_c
    d2m_p2 = (px(c)*delta(nomper1,n+c)+py(c)*delta(nomper1,2*n+c))*(px(c)*delta(nomper2,n+c)+py(c)*delta(nomper2,2*n+c))
    d2m_p2 = d2m_p2/(m_c**3.0)
    d2m = d2m_p1 - d2m_p2
  
    d2p0(c,nomper1,nomper2) = d2m*coshy + dm_c_nomper1*sinhy*delta(c,nomper2)
    d2p0(c,nomper1,nomper2) = d2p0(c,nomper1,nomper2) + dm_c_nomper2*sinhy*delta(c,nomper1)
    d2p0(c,nomper1,nomper2) = d2p0(c,nomper1,nomper2) + m_c*coshy*delta(c,nomper1)*delta(c,nomper2)
  
  
    d2p_par(c,nomper1,nomper2) = d2m*sinhy + dm_c_nomper1*coshy*delta(c,nomper2)
    d2p_par(c,nomper1,nomper2) = d2p_par(c,nomper1,nomper2) + dm_c_nomper2*coshy*delta(c,nomper1)
    d2p_par(c,nomper1,nomper2) = d2p_par(c,nomper1,nomper2) + m_c*sinhy*delta(c,nomper1)*delta(c,nomper2)
  
  enddo
 enddo
enddo





contains

integer function delta(a,b)
integer:: a,b
  if (a.EQ.b) then
   delta = 1
  else
   delta = 0
  endif
end function delta


end subroutine sub_d2psmall
