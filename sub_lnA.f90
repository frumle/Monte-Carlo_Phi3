subroutine sub_lnA(n,sqrt_s,X,P3,lnA)
 
 use constants_module
 
 implicit none
 
 
 ! input
 integer:: n
 real(8):: sqrt_s
 real(8):: X(1:3*n+2)
 
 ! output
 real(8):: lnA

 ! temp
 integer:: b,c
 real(8):: P3(0:3)
 real(8):: px(1:n),py(1:n),y(1:n),PaX,PaY,P1(0:3)
 real(8):: slag1,slag2,slag3,slag4
 real(8):: p0(1:n),p_par(1:n)
 real(8):: temp

 y = X(1:n)
 px = X(n+1:2*n)
 py = X(2*n+1:3*n)
 PaX = X(3*n+1)
 PaY = X(3*n+2)


 P1(0) = sqrt_s/2
 P1(1) = 0.0
 P1(2) = 0.0
 P1(3) = sqrt(((sqrt_s/2)**2.0)-SQR_M)


 lnA = 0.0
 slag1 = 0
 slag2 = 0
 slag3 = 0
 slag4 = 0
 
 do b = 1,n
   temp = sqrt(SQR_MPI + px(b)*px(b) + py(b)*py(b))
   p0(b) = temp*cosh(y(b))
   p_par(b) = temp*sinh(y(b))
 enddo

do b = 0,n

  slag1 = P1(0) - P3(0)
  do c = 1, b
    slag1 = slag1 - p0(c)
  enddo
 
  slag2 = P1(1) - P3(1)
  do c = 1, b
    slag2 = slag2 - px(c)
  enddo

  slag3 = P1(2) - P3(2)
  do c = 1, b
    slag3 = slag3 - py(c)
  enddo

  slag4 = P1(3) - P3(3)
  do c = 1, b
    slag4 = slag4 - p_par(c)
  enddo

  lnA = lnA - log(SQR_MPI-(slag1**2.0)+(slag2**2.0)+(slag3**2.0)+(slag4**2.0))

enddo


end subroutine sub_lnA
