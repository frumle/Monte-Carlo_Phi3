
! TODO: reflect matrix instead of calculate n^3 & n^4 components

subroutine sub_d34lnAinterf(n,AInterf,D2AInterf,D3AInterf,D4AInterf,d3lnAInterf,d4lnAInterf)
 
 implicit none
 
 ! input
 integer:: n
 real(8):: AInterf
 real(8):: D2AInterf(1:n,1:n),D3AInterf(1:n,1:n,1:n),D4AInterf(1:n,1:n,1:n,1:n)

 ! output
  real(8):: d3lnAInterf(1:n,1:n,1:n),d4lnAInterf(1:n,1:n,1:n,1:n)

 ! temp
 integer:: a,b,c,d
 real(8):: temp

 
 d3lnAInterf = 0.0
 d4lnAInterf = 0.0
 
do a = 1,n
 do b = 1,n
  do c = 1,n
   d3lnAInterf(a,b,c) = D3AInterf(a,b,c)
   do d = 1,n
    temp =  - D2AInterf(a,b)*D2AInterf(c,d) - D2AInterf(a,c)*D2AInterf(b,d);
    d4lnAInterf(a,b,c,d) =  temp - D2AInterf(a,d)*D2AInterf(b,c) + AInterf*D4AInterf(a,b,c,d)
   enddo
  enddo
 enddo
enddo

 
 temp = 1.0/AInterf
 d3lnAInterf = d3lnAInterf*temp
 d4lnAInterf = d4lnAInterf*(temp**2)
 
!~ contains

!~ subroutine reflect3(n,M)
!~  integer:: n
!~  real(8):: M(1:n,1:n,1:n)
 
 

!~ end subroutine reflect3

end subroutine sub_d34lnAinterf
