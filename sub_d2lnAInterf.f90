subroutine sub_d2lnAInterf(n,AInterf,Dinterf,Dpxinterf,lnDinteref,lnDpxinteref)

 implicit none

 ! input
 integer:: n
 real(8):: AInterf, Dinterf(1:n,1:n), Dpxinterf(1:n+1,1:n+1)
 
 ! output
 real(8):: lnDinteref(1:n,1:n),lnDpxinteref(1:n+1,1:n+1)

 ! temp
 integer:: i,j
 real(8):: temp

 temp = 1.0/AInterf

do i=1,n
 do j=1,n 
  lnDinteref(i,j)=Dinterf(i,j)*temp
 enddo
enddo

do i=1,n+1
 do j=1,n+1
  lnDpxinteref(i,j)=Dpxinterf(i,j)*temp
 enddo
enddo

end subroutine sub_d2lnAInterf

