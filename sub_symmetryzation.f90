subroutine sub_symmetrization(n,M3,M4)
 
 implicit none
 
 ! input
 integer:: n
 
 ! output
 real(8):: M4(1:n,1:n,1:n,1:n),M3(1:n,1:n,1:n)

 ! temp
 integer:: a,b,c,d
 real(8):: temp
 
 
do a = 1,n
 do b = a,n
  do c = b,n
  
   temp = (M3(a,b,c)+M3(a,c,b)+M3(b,a,c)+M3(b,c,a)+M3(c,a,b)+M3(c,b,a))/6.0
    M3(a,b,c) = temp
    M3(a,c,b) = temp
    M3(b,a,c) = temp
    M3(b,c,a) = temp
    M3(c,a,b) = temp
    M3(c,b,a) = temp
  
   do d = c,n
   
    temp = M4(a,b,c,d)+M4(a,b,d,c)+M4(a,c,b,d)+M4(a,c,d,b)+M4(a,d,b,c)+M4(a,d,c,b)
    temp = temp + M4(b,a,c,d)+M4(b,a,d,c)+M4(b,c,a,d)+M4(b,c,d,a)+M4(b,d,a,c)+M4(b,d,c,a)
    temp = temp + M4(c,a,b,d)+M4(c,a,d,b)+M4(c,b,a,d)+M4(c,b,d,a)+M4(c,d,a,b)+M4(c,d,b,a)
    temp = temp + M4(d,a,b,c)+M4(d,a,c,b)+M4(d,b,a,c)+M4(d,b,c,a)+M4(d,c,a,b)+M4(d,c,b,a)
    temp = temp/24.0
   
M4(a,b,c,d)= temp
M4(a,b,d,c)= temp
M4(a,c,b,d)= temp
M4(a,c,d,b)= temp
M4(a,d,b,c)= temp
M4(a,d,c,b)= temp

M4(b,a,c,d)= temp
M4(b,a,d,c)= temp
M4(b,c,a,d)= temp
M4(b,c,d,a)= temp
M4(b,d,a,c)= temp
M4(b,d,c,a)= temp

M4(c,a,b,d)= temp
M4(c,a,d,b)= temp
M4(c,b,a,d)= temp
M4(c,b,d,a)= temp
M4(c,d,a,b)= temp
M4(c,d,b,a)= temp

M4(d,a,b,c)= temp
M4(d,a,c,b)= temp
M4(d,b,a,c)= temp
M4(d,b,c,a)= temp
M4(d,c,a,b)= temp
M4(d,c,b,a)= temp
   
   
   enddo
  enddo
 enddo
enddo

end subroutine sub_symmetrization


