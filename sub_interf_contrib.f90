subroutine sub_interf_contrib(n,n_groups,npa,X,perCount,perMatrices,Y0_form,D2,D2px,D3,D4,contrib)

  implicit none
  
  ! input
  integer:: n, n_groups, npa(1:n_groups)
  real(8):: X(1:3*n+2) 
  integer:: perCount,perMatrices(1:perCount,1:n_groups,1:n_groups)
  real(8):: D2(1:n,1:n),D2px(1:n+1,1:n+1)
  real(8):: D3(1:n,1:n,1:n),D4(1:n,1:n,1:n,1:n)
 
  ! output
  real(8):: contrib
  
  ! temp
  integer:: ind,a,zap,nvs
  integer:: indexi1(1:npa(1)),indexi2(1:npa(2)),indexi3(1:npa(3)),Per(1:n)
  integer:: gra1,gra2,gra3
  real(8):: mnojitel1,mnojitel2,mnojitel3,mnojitel
  real(8):: Y(1:n)
  real(8):: interfvklad
  real(8):: interfvkladpx
  real(8):: Y0_form(1:perCount,1:n)
  
interface

subroutine sub_determinant(n1,D,detD)

implicit none

integer::n1,nol,znak,raz1,raz2
real(8)::D(1:n1,1:n1),detD,mnojitel
real(8),allocatable::D1(:,:),D2(:,:),D3(:,:)

end subroutine sub_determinant

subroutine sub_maximize4form(n,Y0_Ainterf,Y0_form_approx,Per,D2,D3,D4,D2Form,power)
  implicit none
  
  ! input
  integer:: n, Per(1:n)
  real(8):: D2(1:n,1:n),D3(1:n,1:n,1:n),D4(1:n,1:n,1:n,1:n)
  real(8):: Y0_Ainterf(1:n), Y0_form_approx(1:n)
  
  ! output
  real(8):: D2Form(1:n,1:n), power
  
  ! temp
  
  integer:: i,j,k,l,t,d
  real(8):: Y0_Ainterfp(1:n)
  real(8):: D2per(1:n,1:n),D3per(1:n,1:n,1:n), D4per(1:n,1:n,1:n,1:n)
  real(8):: Tetta(1:n),sumtetta,precsn,max_step
  integer:: rk2,rk3
  real(8):: temp
  
end subroutine sub_maximize4form

end interface
  

contrib=0.0
nvs = 3*n+2

do a=1,npa(1)
indexi1(a)=a
enddo

do a=1,npa(2)
indexi2(a)=npa(1)+a
enddo

do a=1,npa(3)
indexi3(a)=npa(1)+npa(2)+a
enddo

gra1=0
gra2=0
gra3=0

do ind = 1, perCount

mnojitel1=refactorial(npa(1))
mnojitel1=mnojitel1/refactorial(perMatrices(ind,1,1))
mnojitel1=mnojitel1/refactorial(perMatrices(ind,1,2))
mnojitel1=mnojitel1/refactorial(perMatrices(ind,1,3))

mnojitel2=refactorial(npa(2))
mnojitel2=mnojitel2/refactorial(perMatrices(ind,2,1))
mnojitel2=mnojitel2/refactorial(perMatrices(ind,2,2))
mnojitel2=mnojitel2/refactorial(perMatrices(ind,2,3))

mnojitel3=refactorial(npa(3))
mnojitel3=mnojitel3/refactorial(perMatrices(ind,3,1))
mnojitel3=mnojitel3/refactorial(perMatrices(ind,3,2))
mnojitel3=mnojitel3/refactorial(perMatrices(ind,3,3))

mnojitel=mnojitel1*mnojitel2*mnojitel3

do a=1,perMatrices(ind,1,1)
Per(a)=indexi1(a)
enddo

gra1=perMatrices(ind,1,1)
zap=gra1

do a=1,perMatrices(ind,2,1)
Per(zap+a)=indexi2(a)
enddo
gra2=perMatrices(ind,2,1)
zap=zap+gra2

do a=1,perMatrices(ind,3,1)
Per(zap+a)=indexi3(a)
enddo

gra3=perMatrices(ind,3,1)
zap=zap+gra3

do a=1,perMatrices(ind,1,2)
Per(zap+a)=indexi1(gra1+a)
enddo

gra1=gra1+perMatrices(ind,1,2)
zap=zap+perMatrices(ind,1,2)

do a=1,perMatrices(ind,2,2)
Per(zap+a)=indexi2(gra2+a)
enddo

gra2=gra2+perMatrices(ind,2,2)
zap=zap+perMatrices(ind,2,2)

do a=1,perMatrices(ind,3,2)
Per(zap+a)=indexi3(gra3+a)
enddo

gra3=gra3+perMatrices(ind,3,2)
zap=zap+perMatrices(ind,3,2)


do a=1,perMatrices(ind,1,3)
Per(zap+a)=indexi1(gra1+a)
enddo
!gra1=gra1+perMatrices(ind,1,2)
zap=zap+perMatrices(ind,1,3)

do a=1,perMatrices(ind,2,3)
Per(zap+a)=indexi2(gra2+a)
enddo

!gra2=gra2+perMatrices(ind,2,2)
zap=zap+perMatrices(ind,2,3)

do a=1,perMatrices(ind,3,3)
Per(zap+a)=indexi3(gra3+a)
enddo

Y(1:n) = X(1:n)
call vklad(n,Per,Y,Y0_form(ind,1:n),D2,D3,D4,interfvklad)
call vkladpx(n,D2px,Per,interfvkladpx)


contrib = contrib + interfvklad*interfvkladpx*mnojitel
enddo

! TODO: seems to be the error! maybe push it inside cycle: interfvklad*interfvkladpx*mnojitel ?
! contrib = contrib*mnojitel


contains

function refactorial(n1)

integer:: n1,i
real(8):: refactorial

refactorial=0.0

do i=2,n1
refactorial=refactorial+log(REAL(i))
enddo

refactorial=exp(refactorial)

end function refactorial

subroutine vklad(n,Per,Y0_Ainterf,Y0_form,D2,D3,D4,interfvklad)

integer:: n,Per(1:n)
real(8):: Y0_Ainterf(1:n),Y0_form(1:n)
real(8):: D2(1:n,1:n),D3(1:n,1:n,1:n),D4(1:n,1:n,1:n,1:n)
real(8):: D2Form(1:n,1:n)
real(8):: power,interfvklad,detD2Form

call sub_maximize4form(n,Y0_Ainterf,Y0_form,Per,D2,D3,D4,D2Form,power)
call sub_determinant(n,D2Form,detD2Form)

interfvklad=exp(power)/sqrt(abs(detD2Form))

end subroutine vklad

subroutine vkladpx(n,Dpx,Per,interfvkladpx)

integer:: n,Per(1:n),a,b
real(8):: Dpx(1:n+1,1:n+1),Dper(1:n+1,1:n+1),interfvkladpx
real(8):: Dsum(1:n+1,1:n+1),det_Dsum

do a=1,n
do b=1,n
Dper(Per(a),Per(b))=Dpx(a,b)
enddo
enddo

do a=1,n
Dper(n+1,Per(a))=Dpx(n+1,a)
Dper(Per(a),n+1)=Dpx(a,n+1)
enddo

Dper(n+1,n+1)=Dpx(n+1,n+1)

Dsum=Dpx+Dper
call sub_determinant(n+1,Dsum,det_Dsum)

interfvkladpx=1.0/abs(det_Dsum)
end subroutine vkladpx

end subroutine sub_interf_contrib
