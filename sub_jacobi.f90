subroutine sub_jacobi(matrix_size,matrix,eigenvalues,eigenvectors)

 logical, parameter:: CHECK_DIAGONALIZATION = .FALSE.
 logical, parameter:: PRINT_TIME_ELAPSED = .FALSE.
 logical, parameter:: PRESYMMETRIZATION_ENABLED = .TRUE.
 real(8), parameter:: DIAGONALIZATION_PRECISION = 1e-8


 integer:: matrix_size,n,iteration,index_i,index_j
 real(8):: matrix(1:matrix_size,1:matrix_size),eigenvalues(1:matrix_size)
 real(8):: eigenvectors(1:matrix_size,1:matrix_size),eigenvectorsOld(1:matrix_size,1:matrix_size)
 real(8):: matrixNew(1:matrix_size,1:matrix_size),matrixOld(1:matrix_size,1:matrix_size)

 real(8):: maxElement,maxElementOld
 real(8):: cosFi,sinFi,summ,signsin,b,Kangle,difference,fi
 integer:: exec_begining_time,exec_end_time,time_scale
 real(8):: sqr_b

 if(PRINT_TIME_ELAPSED) then
   time_scale=1000
   call system_clock(exec_begining_time, time_scale)
 endif


iteration = 0
n = matrix_size
matrixOld = matrix
 
if(PRESYMMETRIZATION_ENABLED) then
Symmetrization: do i = 1,n
                 do j = i+1,n
     difference = abs(matrixOld(i,j)-matrixOld(j,i))
     if(difference.gt.DIAGONALIZATION_PRECISION) then
       matrixOld(i,j) = (matrixOld(i,j) + matrixOld(j,i))/2
       matrixOld(j,i) = matrixOld(i,j)
     endif
end do
end do Symmetrization
endif

matrixNew = matrixOld
 
  
 eigenvectors = 0.0
 do i = 1,n
  eigenvectors(i,i) = 1.0
 enddo
 
 
Rotations: do
  iteration = iteration + 1
   
  maxElementOld = maxElement
  maxElement = 0.0

  FindMax: do i = 1,n
   do j = i+1,n
    If(abs(maxElement).le.abs(matrixOld(i,j))) Then
      maxElement = matrixOld(i,j)
      index_i = i
      index_j = j
    EndIf
   enddo
  end do FindMax
  
  
  
 
  signsin = 1.0
  if(matrixOld(index_i,index_j).LT.0.0) signsin = -1.0
  
  

  
  b = 0.5*(matrixOld(index_i,index_i) - matrixOld(index_j,index_j))/matrixOld(index_i,index_j)
!~   Kangle = 1.0/sqrt(1.0 + 1.0/(b*b))
  sqr_b = b*b
  Kangle =sqrt(sqr_b/(1.0 + sqr_b))
  sinFi = signsin*sqrt((1.0 - Kangle)/2.0)
  cosFi = sqrt((1.0 + Kangle)/2.0)
  fi = 0.5*atan(b)   
  matrixNew = matrixOld
 
 do k = 1,n
  matrixNew(k,index_i) = matrixOld(k,index_i)*cosFi+matrixOld(k,index_j)*sinFi
  matrixNew(k,index_j) = -matrixOld(k,index_i)*sinFi+matrixOld(k,index_j)*cosFi
  
  eigenvectors(k,index_i) = eigenvectorsOld(k,index_i)*cosFi + eigenvectorsOld(k,index_j)*sinFi
  eigenvectors(k,index_j) = -eigenvectorsOld(k,index_i)*sinFi + eigenvectorsOld(k,index_j)*cosFi
 enddo
  
 eigenvectorsOld = eigenvectors 
 matrixOld = matrixNew
  
 do k = 1,n
  matrixNew(index_i,k) = matrixOld(index_i,k)*cosFi + matrixOld(index_j,k)*sinFi
  matrixNew(index_j,k) = -matrixOld(index_i,k)*sinFi + matrixOld(index_j,k)*cosFi
 enddo


summ = 0.0
do i = 1,n
 do j = i+1,n
  summ = summ + matrixNew(i,j)*matrixNew(i,j)
 enddo
enddo

 if (summ.LT.diagonalization_precision) exit Rotations 
 
 
 matrixOld=matrixNew
 
 
end do Rotations

 do i = 1,n
  eigenvalues(i)=matrixNew(i,i)
 enddo

 if(PRINT_TIME_ELAPSED) then
    call system_clock(exec_end_time)
    print*, "Jacobi diagonalization completed. Time elapsed (ms): ", real((exec_end_time-exec_begining_time))

 endif

end subroutine sub_jacobi
