module profiler

   integer:: time_scale = 1000
   integer, parameter:: MAX_COUNTERS = 10
   integer:: counters_start_time(1:MAX_COUNTERS)
   
   private:: time_scale, counters_start_time
   public:: startCounter, timeElapsed, MAX_COUNTERS
   
contains
   
   subroutine startCounter(counter)
      integer:: counter
    
      call system_clock(counters_start_time(counter), time_scale)
   end subroutine startCounter
      
   function timeElapsed(counter)
      integer:: counter
      real(8):: timeElapsed
      integer:: exec_end_time 
    
      call system_clock(exec_end_time, time_scale)
      timeElapsed = (exec_end_time - counters_start_time(counter))
   end function timeElapsed   
     

end module profiler
