subroutine sub_d2Ainterf(n,c,npa,D,Dpx,Dinterf,Dpxinterf)
implicit none

integer:: n,c,npa(1:c)
real(8):: D(1:n,1:n),Dpx(1:n+1,1:n+1),Dinterf(1:n,1:n),Dpxinterf(1:n+1,1:n+1),sumdia,diafacto
integer:: ind(1:c,1:n),i,i1,j,c1,c2
real(8):: sumnedia,nediafacto,proizv_facto,dla_raznix_c
real(8):: sumdiapx,sumnediapx,dla_raznix_c_px,sumPanti

ind=0
i1=1
do i=1,c
do j=1,npa(i)
ind(i,j)=i1
!print*,i,j,ind(i,j)
i1=i1+1
enddo
enddo 

proizv_facto=refactorial(npa(1))
do c1=2,c
proizv_facto=proizv_facto*refactorial(npa(c1))
enddo

do c1=1,c
do c2=1,c
if (c1 .eq. c2) then
diafacto=proizv_facto/REAL(npa(c1))   !refactorial(npa(c1)-1)
if (npa(c1) .gt. 1) nediafacto=diafacto/ REAL(npa(c1)-1)  !refactorial(npa(c1)-2)

sumdia=D(ind(c1,1),ind(c1,1))
sumdiapx=Dpx(ind(c1,1),ind(c1,1))
do i=2,npa(c1)
sumdia=sumdia+D(ind(c1,i),ind(c1,i))
sumdiapx=sumdiapx+Dpx(ind(c1,i),ind(c1,i))
enddo

do i=1,npa(c1)
Dinterf(ind(c1,i),ind(c1,i))=diafacto*sumdia
Dpxinterf(ind(c1,i),ind(c1,i))=diafacto*sumdiapx
enddo

sumnedia=0.0
sumnediapx=0.0
do i=1,npa(c1)
do j=1,npa(c1)
if (ind(c1,i) .ne. ind(c1,j)) then
sumnedia=sumnedia+D(ind(c1,i),ind(c1,j))
sumnediapx=sumnediapx+Dpx(ind(c1,i),ind(c1,j))
endif
enddo
enddo

do i=1,npa(c1)
do j=1,npa(c1)
if (ind(c1,i) .ne. ind(c1,j)) then
Dinterf(ind(c1,i),ind(c1,j))=sumnedia*nediafacto
Dpxinterf(ind(c1,i),ind(c1,j))=sumnediapx*nediafacto
endif
enddo
enddo

else
dla_raznix_c=0.0
dla_raznix_c_px=0.0
do i=1,npa(c1)
do j=1,npa(c2)
dla_raznix_c=dla_raznix_c+D(ind(c1,i),ind(c2,j))
dla_raznix_c_px=dla_raznix_c_px+Dpx(ind(c1,i),ind(c2,j))
enddo
enddo

do i=1,npa(c1)
do j=1,npa(c2)
Dinterf(ind(c1,i),ind(c2,j))=dla_raznix_c*proizv_facto/(REAL(npa(c1)*npa(c2)))
Dpxinterf(ind(c1,i),ind(c2,j))=dla_raznix_c_px*proizv_facto/(REAL(npa(c1)*npa(c2)))
enddo
enddo



endif

enddo
enddo

do c1=1,c
sumPanti=0.0
do i=1,npa(c1)
sumPanti=sumPanti+Dpx(n+1,ind(c1,i))
enddo
sumPanti=sumPanti*proizv_facto/REAL(npa(c1))
do i=1,npa(c1)
Dpxinterf(n+1,ind(c1,i))=sumPanti
Dpxinterf(ind(c1,i),n+1)=sumPanti
enddo
enddo

Dpxinterf(n+1,n+1)=Dpx(n+1,n+1)*proizv_facto

contains

function refactorial(z)

integer:: z,i
real(8):: refactorial
real(8):: summalog

summalog=0.0

do i=2,z
summalog=summalog+log(REAL(i))
enddo

refactorial=exp(summalog)

end function refactorial


end subroutine sub_d2Ainterf
