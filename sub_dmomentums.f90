subroutine sub_dmomentums(n,sqrt_s,X,P3,P4,dP3,dP4)
 
 use constants_module
 
 implicit none

  ! input
  integer:: n
  real(8):: sqrt_s
  real(8):: X(1:3*n+2)
 
  ! output
  real(8):: P3(0:3), P4(0:3)
  real(8):: dP3(0:3,1:3*n+2),dP4(0:3,1:3*n+2)
 
  ! temp
  integer :: i
  real(8):: px(1:n),py(1:n),y(1:n),PaX,PaY
  real(8):: Panti_x,Panti_y,l,k,t,A1,B1,D,P3perp_kvad,P4perp_kvad 
  real(8):: P3par,P4par,P30,P40,controlEnergy,podkor
  real(8):: dP3par_po_dt,dP3par_po_dP3x,dP3par_po_dP3y
  real(8):: dP3par_po_dPpar_pi,dP3par_po_dk,dE_pi_po_dyj(1:n)
  real(8):: dE_pi_po_dpxj(1:n),dE_pi_po_dpyj(1:n)
  real(8):: dPpar_pi_po_dyj(1:n),dPpar_pi_po_dpxj(1:n),dPpar_pi_po_dpyj(1:n)
  real(8):: dt_po_dyj(1:n),dt_po_dpxj(1:n),dt_po_dpyj(1:n)
  real(8):: dP3perpx_po_dpxj(1:n),dP3perpx_po_dPanti_x,dP3perpy_po_dPanti_y,dP3perpy_po_dpyj(1:n)
  real(8):: dP4perpx_po_dpxj(1:n),dP4perpx_po_dPanti_x,dP4perpy_po_dpyj(1:n),dP4perpy_po_dPanti_y
  real(8):: dk_po_dpxj(1:n),dk_po_dpyj(1:n),dk_po_dPanti_x,dk_po_dPanti_y
  real(8):: dl_po_dyj(1:n),dl_po_dpxj(1:n),dl_po_dpyj(1:n)
  real(8):: dP3par_po_dl
  real(8):: dPpar_pi_po_vsem(1:3*n+2),dl_po_vsem(1:3*n+2),dk_po_vsem(1:3*n+2),dt_po_vsem(1:3*n+2)
  real(8):: dP3perpx_po_vsem(1:3*n+2),dP3perpy_po_vsem(1:3*n+2),dP3par_po_vsem(1:3*n+2)
  real(8):: dP4par_po_vsem(1:3*n+2),dP4perpx_po_vsem(1:3*n+2),dP4perpy_po_vsem(1:3*n+2)
  real(8):: dP40_po_vsem(1:3*n+2),dP30_po_vsem(1:3*n+2)
  real(8):: dP3parvs(1:3*n+2),dP4parvs(1:3*n+2)
  real(8):: dP30(1:3*n+2),dP40(1:3*n+2)

y(1:n) = X(1:n)
px(1:n) = X(n+1:2*n)
py(1:n) = X(2*n+1:3*n)
PaX = X(3*n+1)
PaY = X(3*n+2)


Panti_x=PaX
Panti_y=PaY
call E_pi(n,SQR_MPI,px,py,y,A1)
call Ppar_pi(n,SQR_MPI,px,py,y,B1)




t=(sqrt_s-A1)*(sqrt_s-A1)
l=B1*B1-t
P3perp_kvad=P3perp_x(px,Panti_x,n)*P3perp_x(px,Panti_x,n)+P3perp_y(py,Panti_y,n)*P3perp_y(py,Panti_y,n)
P4perp_kvad=P4perp_x(px,Panti_x,n)*P4perp_x(px,Panti_x,n)+P4perp_y(py,Panti_y,n)*P4perp_y(py,Panti_y,n)
k=P4perp_kvad-P3perp_kvad


D=sqrt(t)*sqrt(4.0*(SQR_M + P3perp_kvad)*l+(l+k)*(l+k))
P3par=(B1*(l+k)+D)/(-2.0*l)
P4par=-P3par-B1
P30=sqrt(sqr_M+P3par*P3par+P3perp_kvad)
P40=sqrt(sqr_M+P4par*P4par+P4perp_kvad)
controlEnergy=sqrt_s-(P30+P40+A1)

P3(0)=P30
P3(1)=P3perp_x(px,Panti_x,n)
P3(2)=P3perp_y(py,Panti_y,n)
P3(3)=P3par

P4(0)=P40
P4(1)=P4perp_x(px,Panti_x,n)
P4(2)=P4perp_y(py,Panti_y,n)
P4(3)=P4par

dP3par_po_dt=(-1.0/(4.0*l*sqrt(t)))*sqrt(4.0*(sqr_M+P3perp_kvad)*l+(l+k)*(l+k))
!print*,"dP3par_po_dt",dP3par_po_dt
dP3par_po_dP3x=-2.0*sqrt(t)*P3perp_x(px,Panti_x,n)/sqrt(4.0*(sqr_M+P3perp_kvad)*l+(l+k)*(l+k))
!print*,"dP3par_po_dP3x",dP3par_po_dP3x
dP3par_po_dP3y=-2.0*sqrt(t)*P3perp_y(py,Panti_y,n)/sqrt(4.0*(sqr_M+P3perp_kvad)*l+(l+k)*(l+k))
!print*,"dP3par_po_dP3y",dP3par_po_dP3y
dP3par_po_dPpar_pi=(l+k)/(-2.0*l)
!print*,"dP3par_po_dPpar_pi",dP3par_po_dPpar_pi
dP3par_po_dk=B1/(-2.0*l)+(l+k)*sqrt(t)/(-2.0*l*sqrt(4.0*(sqr_M+P3perp_kvad)*l+(l+k)*(l+k)))

do i=1,n
dE_pi_po_dyj(i)=mperp(SQR_MPI,px(i),py(i))*sinh(y(i))
enddo

do i=1,n
dE_pi_po_dpxj(i)=px(i)/(mperp(SQR_MPI,px(i),py(i)))*cosh(y(i))
enddo

do i=1,n
dE_pi_po_dpyj(i)=py(i)/(mperp(SQR_MPI,px(i),py(i)))*cosh(y(i))
enddo

do i=1,n
dPpar_pi_po_dyj(i)=mperp(SQR_MPI,px(i),py(i))*cosh(y(i))
enddo

do i=1,n
dPpar_pi_po_dpxj(i)=px(i)/(mperp(SQR_MPI,px(i),py(i)))*sinh(y(i))
enddo

do i=1,n
dPpar_pi_po_dpyj(i)=py(i)/(mperp(SQR_MPI,px(i),py(i)))*sinh(y(i))
enddo

dPpar_pi_po_vsem=0.0
do i=1,n
dPpar_pi_po_vsem(i)=dPpar_pi_po_dyj(i)
enddo
do i=n+1,2*n
dPpar_pi_po_vsem(i)=dPpar_pi_po_dpxj(i-n)
enddo
do i=2*n+1,3*n
dPpar_pi_po_vsem(i)=dPpar_pi_po_dpyj(i-2*n)
enddo

do i=1,n
dt_po_dyj(i)=-2.0*(sqrt_s-A1)*dE_pi_po_dyj(i)
enddo

do i=1,n
dt_po_dpxj(i)=-2.0*(sqrt_s-A1)*dE_pi_po_dpxj(i)
enddo

do i=1,n
dt_po_dpyj(i)=-2.0*(sqrt_s-A1)*dE_pi_po_dpyj(i)
enddo

dt_po_vsem=0.0
do i=1,n
dt_po_vsem(i)=dt_po_dyj(i)
enddo
do i=n+1,2*n
dt_po_vsem(i)=dt_po_dpxj(i-n)
enddo
do i=2*n+1,3*n
dt_po_vsem(i)=dt_po_dpyj(i-2*n)
enddo

do i=1,n
dP3perpx_po_dpxj(i)=-0.5
enddo

dP3perpx_po_dPanti_x=1.0


dP3perpx_po_vsem=0.0
do i=n+1,2*n
dP3perpx_po_vsem(i)=dP3perpx_po_dpxj(i-n)
enddo
dP3perpx_po_vsem(3*n+1)=dP3perpx_po_dPanti_x



do i=1,n
dP3perpy_po_dpyj(i)=-0.5
enddo

dP3perpy_po_dPanti_y=1.0


dP3perpy_po_vsem=0.0
do i=2*n+1,3*n
dP3perpy_po_vsem(i)=dP3perpy_po_dpyj(i-2*n)
enddo
dP3perpy_po_vsem(3*n+2)=dP3perpy_po_dPanti_y


do i=1,n
dP4perpx_po_dpxj(i)=-0.5
enddo



dP4perpx_po_dPanti_x=-1.0

dP4perpx_po_vsem=0.0
do i=n+1,2*n
dP4perpx_po_vsem(i)=dP4perpx_po_dpxj(i-n)
enddo
dP4perpx_po_vsem(3*n+1)=dP4perpx_po_dPanti_x


do i=1,n
dP4perpy_po_dpyj(i)=-0.5
enddo

dP4perpy_po_dPanti_y=-1.0


dP4perpy_po_vsem=0.0
do i=2*n+1,3*n
dP4perpy_po_vsem(i)=dP4perpy_po_dpyj(i-2*n)
enddo
dP4perpy_po_vsem(3*n+2)=dP4perpy_po_dPanti_y


do i=1,n
dk_po_dpxj(i)=P3perp_x(px,Panti_x,n)-P4perp_x(px,Panti_x,n)
enddo


do i=1,n
dk_po_dpyj(i)=P3perp_y(py,Panti_y,n)-P4perp_y(py,Panti_y,n)
enddo

dk_po_dPanti_x=-2.0*(P3perp_x(px,Panti_x,n)+P4perp_x(px,Panti_x,n))

dk_po_dPanti_y=-2.0*(P3perp_y(py,Panti_y,n)+P4perp_y(py,Panti_y,n))

dk_po_vsem=0.0
do i=n+1,2*n
dk_po_vsem(i)=dk_po_dpxj(i-n)
enddo
do i=2*n+1,3*n
dk_po_vsem(i)=dk_po_dpyj(i-2*n)
enddo
dk_po_vsem(3*n+1)=dk_po_dPanti_x
dk_po_vsem(3*n+2)=dk_po_dPanti_y

podkor=4.0*(sqr_M+P3perp_kvad)*l+(l+k)*(l+k)

dP3par_po_dl=B1*(l+k)/(2.0*l*l)-B1/(2.0*l)+sqrt(t*podkor)/(2.0*l*l)
dP3par_po_dl=dP3par_po_dl-sqrt(t)*(4.0*(sqr_M+P3perp_kvad)+2.0*(l+k))/(4.0*l*sqrt(podkor))


do i=1,n
dl_po_dyj(i)=2.0*B1*dPpar_pi_po_dyj(i)-dt_po_dyj(i)
enddo

do i=1,n
dl_po_dpxj(i)=2.0*B1*dPpar_pi_po_dpxj(i)-dt_po_dpxj(i)
enddo

do i=1,n
dl_po_dpyj(i)=2.0*B1*dPpar_pi_po_dpyj(i)-dt_po_dpyj(i)
enddo

dl_po_vsem=0.0
do i=1,n
dl_po_vsem(i)=dl_po_dyj(i)
enddo
do i=n+1,2*n
dl_po_vsem(i)=dl_po_dpxj(i-n)
enddo
do i=2*n+1,3*n
dl_po_vsem(i)=dl_po_dpyj(i-2*n)
enddo

do i=1,3*n+2
dP3par_po_vsem(i)=dP3par_po_dPpar_pi*dPpar_pi_po_vsem(i)+dP3par_po_dl*dl_po_vsem(i)+dP3par_po_dk*dk_po_vsem(i)
dP3par_po_vsem(i)=dP3par_po_vsem(i)+dP3par_po_dt*dt_po_vsem(i)+dP3par_po_dP3x*dP3perpx_po_vsem(i)+dP3par_po_dP3y*dP3perpy_po_vsem(i)
enddo

do i=1,3*n+2
dP4par_po_vsem(i)=-dP3par_po_vsem(i)-dPpar_pi_po_vsem(i)
enddo

do i=1,3*n+2
dP40_po_vsem(i)=(P4par*dP4par_po_vsem(i)+P4perp_x(px,Panti_x,n)*dP4perpx_po_vsem(i)+P4perp_y(py,Panti_y,n)*dP4perpy_po_vsem(i))/P40
enddo
do i=1,3*n+2
enddo


do i=1,3*n+2
dP30_po_vsem(i)=(P3par*dP3par_po_vsem(i)+P3perp_x(px,Panti_x,n)*dP3perpx_po_vsem(i)+P3perp_y(py,Panti_y,n)*dP3perpy_po_vsem(i))/P30
enddo
do i=1,3*n+2
enddo

dP30=dP30_po_vsem
dP3parvs=dP3par_po_vsem
dP4parvs=dP4par_po_vsem
dP40=dP40_po_vsem



dP3(0,1:3*n+2)=dP30(1:3*n+2)
dP3(1,1:3*n+2)=dP3perpx_po_vsem(1:3*n+2)
dP3(2,1:3*n+2)=dP3perpy_po_vsem(1:3*n+2)
dP3(3,1:3*n+2)=dP3parvs(1:3*n+2)

dP4(0,1:3*n+2)=dP40(1:3*n+2)
dP4(1,1:3*n+2)=dP4perpx_po_vsem(1:3*n+2)
dP4(2,1:3*n+2)=dP4perpy_po_vsem(1:3*n+2)
dP4(3,1:3*n+2)=dP4parvs(1:3*n+2)

contains

function mperp(sqr_mass,x1,y1)

real(8)::sqr_mass,mperp,x1,y1

mperp=sqrt(sqr_mass+x1*x1+y1*y1)

end function mperp

function P3perp_x(px,Panti_x,n)

real(8):: px(1:n),Panti_x,P3perp_x,summa
integer :: n,i

summa=0.0

do i=1,n
summa=summa+px(i)
enddo

P3perp_x=-0.5*summa+Panti_x

end function P3perp_x

function P3perp_y(py,Panti_y,n)

real(8):: py(1:n),Panti_y,P3perp_y,summa1
integer :: n,i

summa1=0.0

do i=1,n
summa1=summa1+py(i)
enddo

P3perp_y=-0.5*summa1+Panti_y

end function P3perp_y

function P4perp_x(px,Panti_x,n)

real(8):: px(1:n),Panti_x,P4perp_x,summa
integer :: n,i

summa=0.0

do i=1,n
summa=summa+px(i)
enddo

P4perp_x=-0.5*summa-Panti_x

end function P4perp_x

function P4perp_y(py,Panti_y,n)

real(8):: py(1:n),Panti_y,P4perp_y,summa1
integer :: n,i

summa1=0.0

do i=1,n
summa1=summa1+py(i)
enddo

P4perp_y=-0.5*summa1-Panti_y

end function P4perp_y

subroutine E_pi(n,sqr_mpi,px,py,y,A)
implicit none

real(8):: px(1:n),py(1:n),y(1:n),A
real(8):: sqr_mpi
integer :: n,i

A=0.0
do i=1,n
A=A+mperp(sqr_mpi,px(i),py(i))*cosh(y(i))
enddo

end subroutine E_pi

subroutine Ppar_pi(n,sqr_mpi,px,py,y,B)
implicit none

real(8):: px(1:n),py(1:n),y(1:n),B
real(8):: sqr_mpi
integer :: n,i

B=0.0
do i=1,n
B=B+mperp(sqr_mpi,px(i),py(i))*sinh(y(i))
enddo

end subroutine Ppar_pi

end subroutine sub_dmomentums
