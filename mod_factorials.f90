module factorials_module  
implicit none 
  
   integer, parameter:: NFACT_MAX = 12
   integer:: mod_factorials(0:NFACT_MAX)
   
   public:: calculateFactorials, factorial
   public:: mod_factorials,NFACT_MAX
   
contains

 subroutine calculateFactorials()
   integer:: i
   mod_factorials(0) = 1
   do i = 1,NFACT_MAX
     mod_factorials(i) = mod_factorials(i - 1)*i
   enddo
 end subroutine calculateFactorials
 
 function factorial(n)
   integer:: n,factorial,i
   factorial = 1
   do i = 2,n
     factorial = factorial*i
   enddo
 end function factorial
  
end module factorials_module 
