subroutine sub_d34Ainterf(n,c,npa,M3,M4,Mi3,Mi4)
 implicit none
 
 integer:: i,i1,i2,i3,i4
 integer:: j1,j2,j3,j4
 integer:: c1,c2,c3,c4,c
 real(8):: npa_fact
 integer:: n
 integer:: npa(1:c)
 real(8):: M3(1:n,1:n,1:n),M4(1:n,1:n,1:n,1:n)
 real(8):: Mi3(1:n,1:n,1:n),Mi4(1:n,1:n,1:n,1:n)
 real(8):: maindiag,diag12,diag13,diag23,nondiag
 real(8):: m4element(1:15),m4coef(1:15)
 integer:: counter
 real(8):: maindiagCoef,diag12Coef,diag13Coef,diag23Coef,nondiagCoef
 real(8):: temp

 interface
 
 subroutine sub_symmetrization(n,M3,M4)
 
 implicit none
 
 ! input
 integer:: n
 
 ! output
 real(8):: M4(1:n,1:n,1:n,1:n),M3(1:n,1:n,1:n)

 ! temp
 integer:: a,b,c,d
 real(8):: temp
 
 end subroutine sub_symmetrization
 
 
 end interface


 Mi3 = 0.0
 Mi4 = 0.0
 i1 = 1
 i2 = 1
 i3 = 1
 i4 = 1

 npa_fact = refactorial(npa(1))
 do i = 2,c
  npa_fact = npa_fact*refactorial(npa(i))
 enddo
counter = 0


!Next for 3D matrix
 do c1 = 1,c
  i2 = 1
  do c2 = 1,c
   i3 = 1     
   do c3 = 1,c
     
     maindiag = 0.0
     diag12 = 0.0
     diag13 = 0.0
     diag23 = 0.0
     nondiag = 0.0
     
     maindiagCoef= 0.0
     diag12Coef = 0.0
     diag13Coef = 0.0
     diag23Coef = 0.0
     nondiagCoef = 0.0

     do j1 = i1, i1 + npa(c1) - 1
      do j2 = i2, i2 + npa(c2) - 1
       do j3 = i3,i3 + npa(c3) - 1
       
       ! 1
        if ((j1.eq.j2).and.(j1.eq.j3)) then      
          maindiag = maindiag + M3(j1,j2,j3)
          
       ! 2  
        else if ((j1.eq.j2).and.(j1.ne.j3)) then  
          diag12 = diag12 + M3(j1,j2,j3)
          
       ! 3   
        else if ((j1.eq.j3).and.(j1.ne.j2)) then
          diag13 = diag13 + M3(j1,j2,j3)    
          
       ! 4   
        else if ((j2.eq.j3).and.(j2.ne.j1)) then  
          diag23 = diag23 + M3(j1,j2,j3)   
          
       ! 5   
        else if ((j1.ne.j2).and.(j1.ne.j3)) then  
          nondiag = nondiag + M3(j1,j2,j3)
          
        endif

         
       counter = counter + 1  
       enddo
      enddo
     enddo
    
    
! # = #* koef

  if( (c1.eq.c2).and.(c1.eq.c3) ) then
       
       maindiagCoef = npa_fact/npa(c1)
       if(npa(c1).ge.2) then
         diag12Coef = npa_fact/npa(c1)/(npa(c1)-1)
         diag13Coef = diag12Coef
         diag23Coef = diag12Coef
       endif
       if(npa(c1).gt.2) then
         nondiagCoef = npa_fact/npa(c1)/(npa(c1)-1)/(npa(c1)-2)
       endif
              
  else if( (c1.eq.c2).and.(c1.ne.c3)) then
     
!       if((diag13.ne.0.0).or.(diag23.ne.0.0)) print*, "ERRRRRRROOOOOOOOORRRRRRRRR 34INTERF"
     
       diag12Coef = npa_fact/npa(c1)/npa(c3)  
       
       if(npa(c1).gt.1) then
        nondiag = npa_fact/npa(c1)/(npa(c1)-1)/npa(c3)
       endif
     
  else if( (c1.eq.c3).and.(c1.ne.c2) ) then
     
       diag13Coef = npa_fact/npa(c1)/npa(c2)
       
       if(npa(c1).gt.1) then
        nondiag = npa_fact/npa(c1)/(npa(c1)-1)/npa(c2)
       endif
     
     else if( (c2.eq.c3).and.(c2.ne.c1) ) then
     
       diag23Coef = npa_fact/npa(c2)/npa(c1)
       
       if(npa(c2).gt.1) then
        nondiag = npa_fact/npa(c2)/(npa(c2)-1)/npa(c1)
       endif
     
   else if( (c1.ne.c2).and.(c1.ne.c3).and.(c2.ne.c3) ) then
        nondiagCoef = npa_fact/npa(c1)/npa(c2)/npa(c3)
   endif
     
     maindiag = maindiag*maindiagCoef
     diag12 = diag12*diag12Coef
     diag13 = diag13*diag13Coef
     diag23 = diag23*diag23Coef
     nondiag = nondiag*nondiagCoef
       
       
       
    ! Writing 'interf' elements to a new matrix "Mi3", 
    ! with accounting of factorial coefficients.
       
     do j1 = i1, i1 + npa(c1) - 1
      do j2 = i2, i2 + npa(c2) - 1
       do j3 = i3,i3 + npa(c3) - 1
       
        if ((j1.eq.j2).and.(j1.eq.j3)) then      
          Mi3(j1,j2,j3) = maindiag
                    
        else if ((j1.eq.j2).and.(j1.ne.j3)) then  
          Mi3(j1,j2,j3) = diag12
                    
        else if ((j1.eq.j3).and.(j1.ne.j2)) then  
          Mi3(j1,j2,j3) = diag13 
                    
        else if ((j2.eq.j3).and.(j2.ne.j1)) then  
          Mi3(j1,j2,j3) = diag23
           
        else if ((j1.ne.j2).and.(j1.ne.j3).and.(j2.ne.j3)) then  
          Mi3(j1,j2,j3) = nondiag
        endif



       enddo
      enddo
     enddo


          i3 = i3 + npa(c3)
   
      enddo

       i2 = i2 + npa(c2)   
   
  enddo
  
    i1 = i1 + npa(c1)
   
enddo

! Next for 4D matrix

m4element = 0.0
Mi4 = 0.0


 i1 = 1
 do c1 = 1,c
  i2 = 1
  do c2 = 1,c
   i3 = 1     
   do c3 = 1,c
    i4 = 1
    do c4 = 1,c  

     m4element = 0.0
     m4coef = 0.0
  
     do j1 = i1, i1 + npa(c1) - 1
      do j2 = i2, i2 + npa(c2) - 1
       do j3 = i3,i3 + npa(c3) - 1
        do j4 = i4,i4 + npa(c4) - 1
         
         !1 
           if( (j1.eq.j2).and.(j1.eq.j3).and.(j1.eq.j4) ) then
             m4element(1) = m4element(1) + M4(j1,j2,j3,j4)
           endif
         
         !2
           if( (j1.eq.j2).and.(j1.eq.j3).and.(j1.ne.j4) ) then
             m4element(2) = m4element(2) + M4(j1,j2,j3,j4)
           endif 
           
          !3
           if( (j1.eq.j2).and.(j1.eq.j4).and.(j1.ne.j3) ) then
             m4element(3) = m4element(3) + M4(j1,j2,j3,j4)
           endif   
           
          !4
           if( (j1.eq.j3).and.(j1.eq.j4).and.(j1.ne.j2) ) then
             m4element(4) = m4element(4) + M4(j1,j2,j3,j4)
           endif   
           
          !5
           if( (j2.eq.j3).and.(j2.eq.j4).and.(j2.ne.j1) ) then
            m4element(5) = m4element(5) + M4(j1,j2,j3,j4)
           endif   
           
          !6
           if( (j1.eq.j2).and.(j3.eq.j4).and.(j1.ne.j3) ) then
             m4element(6) = m4element(6) + M4(j1,j2,j3,j4)
           endif   
           
          !7
           if( (j1.eq.j2).and.(j1.ne.j3).and.(j1.ne.j4).and.(j3.ne.j4) ) then
            m4element(7) = m4element(7) + M4(j1,j2,j3,j4)
           endif   
           
          !8
           if( (j1.eq.j3).and.(j2.eq.j4).and.(j1.ne.j2) ) then
             m4element(8) = m4element(8) + M4(j1,j2,j3,j4)
           endif   
           
          !9
           if( (j1.eq.j3).and.(j1.ne.j2).and.(j1.ne.j4).and.(j2.ne.j4) ) then
              m4element(9) = m4element(9) + M4(j1,j2,j3,j4)
           endif   
           
          !10
           if( (j1.eq.j4).and.(j2.eq.j3).and.(j1.ne.j2) ) then
              m4element(10) = m4element(10) + M4(j1,j2,j3,j4)
           endif   
           
          !11
           if( (j1.eq.j4).and.(j1.ne.j2).and.(j1.ne.j3).and.(j2.ne.j3) ) then
              m4element(11) = m4element(11) + M4(j1,j2,j3,j4)
           endif   
           
          !12
           if( (j2.eq.j4).and.(j2.ne.j3).and.(j2.ne.j1).and.(j3.ne.j1) ) then
              m4element(12) = m4element(12) + M4(j1,j2,j3,j4)
           endif   
           
          !13
           if( (j3.eq.j4).and.(j3.ne.j2).and.(j3.ne.j1).and.(j2.ne.j1) ) then
              m4element(13) = m4element(13) + M4(j1,j2,j3,j4)
           endif   
           
          !14
           if( (j2.eq.j3).and.(j2.ne.j4).and.(j2.ne.j1).and.(j4.ne.j1) ) then
              m4element(14) = m4element(14) + M4(j1,j2,j3,j4)
           endif  
           
          !15
           if( (j1.ne.j2).and.(j1.ne.j3).and.(j1.ne.j4).and.(j2.ne.j3).and.(j2.ne.j4).and.(j3.ne.j4) ) then
               m4element(15) = m4element(15) + M4(j1,j2,j3,j4)
           endif   

        enddo
       enddo
      enddo
     enddo
      
         !COEFITIENTS
       
       select case (getCase(c1,c2,c3,c4))
         
        case (1)
          !________________________________1
           temp = npa_fact/npa(c1)
            m4coef(1) = temp
           if(npa(c1).gt.1) then
            temp = temp/(npa(c1)-1)
            m4coef(2) = temp
            m4coef(3) = temp
            m4coef(4) = temp
            m4coef(5) = temp
            m4coef(6) = temp
            m4coef(8) = temp
            m4coef(10) = temp
           endif
           
           if(npa(c1).gt.2) then
            temp = temp/(npa(c1) - 2)
            m4coef(7) = temp
            m4coef(9) = temp
            m4coef(11) = temp
            m4coef(12) = temp
            m4coef(13) = temp
            m4coef(14) = temp
           endif
           
           if(npa(c1).gt.3) then
            m4coef(15) = temp/(npa(c1) - 3)
           endif
          !################################
          

        case (2)
          !________________________________2
           temp = npa_fact/(npa(c1)*npa(c4))
           m4coef(2) = temp
           if(npa(c1).gt.1) then
            temp = temp/(npa(c1) - 1)
            m4coef(7) = temp
            m4coef(9) = temp
            m4coef(14) = temp
           endif
           
           if(npa(c1).gt.2) then
            m4coef(15) = temp/(npa(c1)-2)
           endif
          !################################
          
        case (3)
          !________________________________3
           temp = npa_fact/(npa(c1)*npa(c3))
           m4coef(3) = temp
           if(npa(c1).gt.1) then
            temp = temp/(npa(c1) - 1)
            m4coef(7) = temp
            m4coef(11) = temp
            m4coef(12) = temp
           endif
           
           if(npa(c1).gt.2) then
            m4coef(15) = temp/(npa(c1)-2)
           endif
          !################################
 
        case (4)
          !________________________________4
           temp = npa_fact/(npa(c1)*npa(c2))
           m4coef(4) = temp
           if(npa(c1).gt.1) then
            temp = temp/(npa(c1) - 1)
            m4coef(9) = temp
            m4coef(11) = temp
            m4coef(13) = temp
           endif
           
           if(npa(c1).gt.2) then
            m4coef(15) = temp/(npa(c1)-2)
           endif
          !################################
          
        case (5)
          !________________________________5
           temp = npa_fact/(npa(c2)*npa(c1))
           m4coef(5) = temp
           if(npa(c2).gt.1) then
            temp = temp/(npa(c2) - 1)
            m4coef(12) = temp
            m4coef(13) = temp
            m4coef(14) = temp
           endif
           
           if(npa(c2).gt.2) then
            m4coef(15) = temp/(npa(c2)-2)
           endif
          !################################
 
        case (6)
          !________________________________6
           temp = npa_fact/(npa(c1)*npa(c3))
           m4coef(6) = temp
           if(npa(c3).gt.1) then
            m4coef(7) = temp/(npa(c3)-1)
           endif         
           if(npa(c1).gt.1) then
            m4coef(13) = temp/(npa(c1)-1) 
             if(npa(c3).gt.1) then
               m4coef(15) = temp/((npa(c1)-1)*(npa(c3)-1))
             endif
           endif
          !################################
          
        case (7)
          !________________________________7
           temp = npa_fact/(npa(c1)*npa(c3)*npa(c4))
           m4coef(7) = temp
           if(npa(c1).gt.1) then
            m4coef(15) = temp/(npa(c1)-1)
           endif         
          !################################ 
          
        case (8)
          !________________________________8
           temp = npa_fact/(npa(c1)*npa(c2))
           m4coef(8) = temp
           if(npa(c2).gt.1) then
            m4coef(9) = temp/(npa(c2)-1)
           endif         
           if(npa(c1).gt.1) then
            m4coef(12) = temp/(npa(c1)-1) 
             if(npa(c2).gt.1) then
               m4coef(15) = temp/((npa(c1)-1)*(npa(c2)-1))
             endif
           endif
          !################################
          
        case (9)
          !________________________________7
           temp = npa_fact/(npa(c1)*npa(c2)*npa(c4))
           m4coef(9) = temp
           if(npa(c1).gt.1) then
            m4coef(15) = temp/(npa(c1)-1)
           endif         
          !################################  
                   
        case (10)
          !________________________________10
           temp = npa_fact/(npa(c1)*npa(c2))
           m4coef(10) = temp
           if(npa(c2).gt.1) then
            m4coef(11) = temp/(npa(c2)-1)
           endif         
           if(npa(c1).gt.1) then
            m4coef(14) = temp/(npa(c1)-1) 
             if(npa(c2).gt.1) then
               m4coef(15) = temp/((npa(c1)-1)*(npa(c2)-1))
             endif
           endif
          !################################          
          
        case (11)
          !________________________________11
           temp = npa_fact/(npa(c1)*npa(c2)*npa(c3))
           m4coef(11) = temp
           if(npa(c1).gt.1) then
            m4coef(15) = temp/(npa(c1)-1)
           endif         
          !################################            
          
        case (12)
          !________________________________12
           temp = npa_fact/(npa(c2)*npa(c3)*npa(c1))
           m4coef(12) = temp
           if(npa(c2).gt.1) then
            m4coef(15) = temp/(npa(c2)-1)
           endif         
          !################################            
          
        case (13)
          !________________________________13
           temp = npa_fact/(npa(c3)*npa(c2)*npa(c1))
           m4coef(13) = temp
           if(npa(c3).gt.1) then
            m4coef(15) = temp/(npa(c3)-1)
           endif         
          !################################            
          
        case (14)
          !________________________________14
           temp = npa_fact/(npa(c2)*npa(c1)*npa(c4))
           m4coef(14) = temp
           if(npa(c2).gt.1) then
            m4coef(15) = temp/(npa(c2)-1)
           endif         
          !################################            
          
        case (15)
          !________________________________15
            m4coef(15) = npa_fact/(npa(c1)*npa(c2)*npa(c3)*npa(c4))
          !################################  
       end select
   
         
           do i = 1,15
            m4element(i) = m4element(i)*m4coef(i)
           enddo
         
    
     do j1 = i1, i1 + npa(c1) - 1
      do j2 = i2, i2 + npa(c2) - 1
       do j3 = i3,i3 + npa(c3) - 1
        do j4 = i4,i4 + npa(c4) - 1
         
         !1 
           if( (j1.eq.j2).and.(j1.eq.j3).and.(j1.eq.j4) ) then
             Mi4(j1,j2,j3,j4) = m4element(1)
           endif
         
         !2
           if( (j1.eq.j2).and.(j1.eq.j3).and.(j1.ne.j4) ) then
             Mi4(j1,j2,j3,j4) = m4element(2)
           endif 
           
          !3
           if( (j1.eq.j2).and.(j1.eq.j4).and.(j1.ne.j3) ) then
             Mi4(j1,j2,j3,j4) = m4element(3)
           endif   
           
          !4
           if( (j1.eq.j3).and.(j1.eq.j4).and.(j1.ne.j2) ) then
             Mi4(j1,j2,j3,j4) = m4element(4)
           endif   
           
          !5
           if( (j2.eq.j3).and.(j2.eq.j4).and.(j2.ne.j1) ) then
            Mi4(j1,j2,j3,j4) = m4element(5)
           endif   
           
          !6
           if( (j1.eq.j2).and.(j3.eq.j4).and.(j1.ne.j3) ) then
             Mi4(j1,j2,j3,j4) = m4element(6)
           endif   
           
          !7
           if( (j1.eq.j2).and.(j1.ne.j3).and.(j1.ne.j4).and.(j3.ne.j4) ) then
            Mi4(j1,j2,j3,j4) = m4element(7)
           endif   
           
          !8
           if( (j1.eq.j3).and.(j2.eq.j4).and.(j1.ne.j2) ) then
             Mi4(j1,j2,j3,j4) = m4element(8)
           endif   
           
          !9
           if( (j1.eq.j3).and.(j1.ne.j2).and.(j1.ne.j4).and.(j2.ne.j4) ) then
              Mi4(j1,j2,j3,j4) = m4element(9)
           endif   
           
          !10
           if( (j1.eq.j4).and.(j2.eq.j3).and.(j1.ne.j2) ) then
              Mi4(j1,j2,j3,j4) = m4element(10)
           endif   
           
          !11
           if( (j1.eq.j4).and.(j1.ne.j2).and.(j1.ne.j3).and.(j2.ne.j3) ) then
              Mi4(j1,j2,j3,j4) = m4element(11)
           endif   
           
          !12
           if( (j2.eq.j4).and.(j2.ne.j3).and.(j2.ne.j1).and.(j3.ne.j1) ) then
              Mi4(j1,j2,j3,j4) = m4element(12)
           endif   
           
          !13
           if( (j3.eq.j4).and.(j3.ne.j2).and.(j3.ne.j1).and.(j2.ne.j1) ) then
              Mi4(j1,j2,j3,j4) = m4element(13)
           endif   
           
          !14
           if( (j2.eq.j3).and.(j2.ne.j4).and.(j2.ne.j1).and.(j4.ne.j1) ) then
              Mi4(j1,j2,j3,j4) = m4element(14)
           endif  
           
          !15
           if( (j1.ne.j2).and.(j1.ne.j3).and.(j1.ne.j4).and.(j2.ne.j3).and.(j2.ne.j4).and.(j3.ne.j4) ) then
               Mi4(j1,j2,j3,j4) = m4element(15)
           endif  
    
    
         enddo
        enddo
       enddo
      enddo
    
     
     i4 = i4 + npa(c4)
    enddo !c4
   i3 = i3 + npa(c3)
  enddo ! c3
  i2 = i2 + npa(c2)   
 enddo !c2
 i1 = i1 + npa(c1)
enddo !c1


call sub_symmetrization(n,Mi3,Mi4)

contains

function refactorial(n)
integer:: n,i
real(8):: refactorial,i_1

refactorial=0.0

do i=2,n
i_1=i
refactorial=refactorial+log(i_1)
enddo
refactorial=exp(refactorial)
end function refactorial

integer function delta(a,b)
 integer:: a, b

delta = 0
 if(a.eq.b) delta = 1

end function delta

integer function getCase(i,k,l,m)
 integer:: i,k,l,m
 !1 
 if( (i.eq.k).and.(i.eq.l).and.(i.eq.m) ) then
  getCase = 1
  return
 !2
 else if( (i.eq.k).and.(i.eq.l).and.(i.ne.m) ) then
  getCase = 2
  return
 !3
 else if( (i.eq.k).and.(i.eq.m).and.(i.ne.l) ) then
  getCase = 3
  return
 !4
 else if( (i.eq.l).and.(i.eq.m).and.(i.ne.k) ) then
  getCase = 4
  return
 !5
 else if( (k.eq.l).and.(k.eq.m).and.(k.ne.i) ) then
  getCase = 5
  return
 !6
 else if( (i.eq.k).and.(l.eq.m).and.(i.ne.l) ) then
  getCase = 6
  return
 !7
 else if( (i.eq.k).and.(i.ne.l).and.(i.ne.m).and.(l.ne.m) ) then
  getCase = 7
  return
 !8
 else if( (i.eq.l).and.(k.eq.m).and.(i.ne.k) ) then
  getCase = 8
  return
 !9
 else if( (i.eq.l).and.(i.ne.k).and.(i.ne.m).and.(k.ne.m) ) then
  getCase = 9
  return
 !10
 else if( (i.eq.m).and.(k.eq.l).and.(i.ne.k) ) then
  getCase = 10
  return
 !11
 else if( (i.eq.m).and.(i.ne.k).and.(i.ne.l).and.(k.ne.l) ) then
  getCase = 11
  return
 !12
 else if( (k.eq.m).and.(k.ne.l).and.(k.ne.i).and.(l.ne.i) ) then
  getCase = 12
  return
 !13
 else if( (l.eq.m).and.(l.ne.k).and.(l.ne.i).and.(k.ne.i) ) then
  getCase = 13
  return
 !14
 else if( (k.eq.l).and.(k.ne.m).and.(k.ne.i).and.(m.ne.i) ) then
   getCase = 14
  return
 !15
 else if( (i.ne.k).and.(i.ne.l).and.(i.ne.m).and.(k.ne.l).and.(k.ne.m).and.(l.ne.m) ) then
  getCase = 15
  return
 endif
 
 print*, "sub_d34Ainterf: BAD CASE.";
 getCase = -1
 
 
 end function getCase

end subroutine sub_d34Ainterf
