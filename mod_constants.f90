module constants_module  
implicit none 

!~ Constants
   real(8), parameter :: PI = 3.1415926536  
   real(8), parameter :: e = 2.7182818285 

!~ Masses
   real(8), parameter :: M_PROTON = 0.938
   real(8), parameter :: M_PION = 0.139


!~ Nondimentionalization
   real(8), parameter :: DIMEN = M_PION   
   
   ! Dimentionless masses
   real(8), parameter :: M = M_PROTON/DIMEN
   real(8), parameter :: MPI = 1.0
   
      
!~ Sqr masses
   real(8), parameter:: SQR_M = M*M
   real(8), parameter:: SQR_MPI = MPI*MPI



!~ TODO: Amplitude coefficients 
!~    real(8), parameter :: amp_coef = (g*g/(1152.0*DIMEN*M*(PI**6.0)))**4.0
  
   
   
  
end module constants_module 
