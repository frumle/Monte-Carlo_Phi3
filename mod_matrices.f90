module matrices
  
contains

! git test

real(8) function checkSymmetry(n,M)
  integer:: n
  real(8):: M(1:n,1:n)
  integer:: i,k
  real(8):: min_element
  
  min_element = abs(M(1,2))
 
  checkSymmetry = 0.0
  do i = 1,n
   do k = i + 1,n
    min_element = min(min_element,min(abs(M(i,k)),abs(M(k,i))))
    checkSymmetry = checkSymmetry + abs(M(i,k) - M(k,i))
   enddo
  enddo
  checkSymmetry = checkSymmetry/min_element
    
end function checkSymmetry


real(8) function checkEqual(n,M1,M2)
  integer:: n
  real(8):: M1(1:n,1:n),M2(1:n,1:n)
  integer:: i,k
  real(8):: min_element
  
  min_element = abs(M1(1,2))
 
  checkEqual = 0.0
  do i = 1,n
   do k = 1,n
    min_element = min(min_element,min(abs(M1(i,k)),abs(M2(i,k))))
    checkEqual = checkEqual + abs(M1(i,k) - M2(i,k))
   enddo
  enddo
  checkEqual = checkEqual/min_element
    
end function checkEqual


end module matrices
