module mc_engine_module

 use process_info_type
 use constants_module
 use mt19937
 
 implicit none 

 ! Maximum available number of secondary particle
 integer, parameter:: MC_N_MAX = 12
 integer, parameter:: MC_N_GROUPS = 3

 
 ! Processes info for 0:N_MAX processes
 type(process_info_bundle), allocatable:: proc_info_array(:)
 real(8), allocatable:: cross_sections_sum(:)
 
 ! CMS energy (in GeV)
 real(8):: sqrt_s_gev

 ! Actual maximum secondary particles number for selected energy
 integer:: n_max
  
 ! Temp vars
 real(8):: cs_range
   
 ! Private vars
 private:: n_max, sqrt_s_gev, proc_info_array,cross_sections_sum,cs_range
 
 ! Interface
 public:: mc_init, mc_init_from_file, mc_close,mc_genRapidities
 public:: MC_N_MAX, MC_N_GROUPS
 
 
contains
 
subroutine mc_init(cms_energy, save_ , filename_)

  implicit none
  
  real(8):: cms_energy
  integer:: i, n
  real(8):: temp
  real(8):: cs_sum

  logical:: save_
  character (len=*):: filename_
  integer:: device_id
  
  
 interface
 
 subroutine sub_get_process_info(n,n_groups,sqrt_s_gev,info)

 use process_info_type
 use constants_module
 use factorials_module
 
 implicit none

 ! inpus
 integer:: n, n_groups
 real(8):: sqrt_s_gev
 
 real(8), allocatable:: X(:), Xmax(:), Y0_form_approx(:,:)

 ! output
 type(process_info_bundle):: info
 
 ! temp
 real(8):: sqrt_s,cross_section
 real(8):: maximization_step,maximization_precision

 integer, allocatable:: npa(:) 
 real(8):: P1(0:3), P2(0:3), P3(0:3), P4(0:3)
 real(8):: A, AInterf, interfFact
 real(8), allocatable:: D2A(:,:), D2Ay(:,:),D2Apx(:,:)
 real(8), allocatable:: D2AyInterf(:,:),D2ApxInterf(:,:)
 real(8), allocatable:: D3A(:,:,:),D4A(:,:,:,:)
 real(8), allocatable:: D3AInterf(:,:,:),D4AInterf(:,:,:,:)
 real(8), allocatable:: D2lnAyInterf(:,:),D2lnApxInterf(:,:)
 real(8), allocatable:: D3lnAInterf(:,:,:),D4lnAInterf(:,:,:,:)

 integer:: perCount
 integer, allocatable:: perMat(:,:,:)
 
 real(8):: P12,inv_flow
 integer:: i
 
 end subroutine sub_get_process_info
 
 end interface
 
  
  ! Check if energy is correct ( > 2*M_p)
  if(cms_energy.lt.2*M_PROTON) then
    print*, "MC_Engine: bad energy value 'sqrt_s < 2M_proton'."
    stop
  endif

  ! Set current energy
  sqrt_s_gev = cms_energy
    
  ! Calculate maximum available secondary particles number
  temp = cms_energy - 2*M_PROTON
  n = 0
  do i = 1, MC_N_MAX
    temp = temp - M_PION
    if(temp > 0.01) then
      n = n + 1
    else
      exit
    endif
  enddo
  
  ! Prepare procesess info array (0:n_)
  ! Check if info array had been already allocated
  if(allocated(proc_info_array)) then 
    ! [Allocated]
    ! Check if the previous 'n' and current 'n_' are different
    if(n.ne.n_max) then 
     ! [n != n_] - reallocate info array
     do i = 0,n_max                       
       call recycle_bundle(proc_info_array(i))  
     enddo
     deallocate(proc_info_array)
     
     allocate(proc_info_array(0:n))
     do i = 0,n
      call alloc_bundle(proc_info_array(i),i)
     enddo
   
    endif 
    ! [n == n_max] - don't touch info array at all
  else   
    ! [Not allocated]
    ! Allocate and prepare info array if it had not been allocated yet
     allocate(proc_info_array(0:n))
     do i = 0, n
      call alloc_bundle(proc_info_array(i),i)
     enddo 
  endif
 
 
  ! Prepare cross-sections sum array
  if(allocated(cross_sections_sum)) then
    if(n.ne.n_max) then
      deallocate(cross_sections_sum)
      allocate(cross_sections_sum(0:n))
    endif
  else
    allocate(cross_sections_sum(0:n))
  endif
 
 
 
  ! Set maximum available secondary particles number
  n_max = n
 
  ! Get processes info and save it into proc_info_array
  if(save_) then
    device_id = 1
    open(device_id,file=filename_)
    write(device_id,*) "#config:  n          sqrt_s"
    write(device_id,*) n_max, sqrt_s_gev
  endif
  
  
  cs_sum = 0.0
!~   do i = 0,n_max
  do i = 0,6
  
   ! TODO: fix cross-sections for n = 0..3
   if(i.ge.3) then 
     call sub_get_process_info(i,MC_N_GROUPS,sqrt_s_gev,proc_info_array(i))
   else
     proc_info_array(i)%n = i
     proc_info_array(i)%cross_section = 0.0
   endif
   
   ! calculate cross-sections sum for further 'n' generations
   cs_sum = cs_sum + proc_info_array(i)%cross_section
   cross_sections_sum(i) =  cs_sum
    
    
   ! If save option enabled - save current config to file
   if(save_) call write_bundle(device_id,proc_info_array(i))
  enddo

  cs_range = cross_sections_sum(n)


  if(save_) then
    close(device_id) 
  endif
  
  ! Generate new seed for prng
  call mt_init_seed()

end subroutine mc_init 

subroutine mc_init_from_file(filename)
  implicit none
  
  character (len=*):: filename
    
  integer:: i,device_id,n
  real(8):: cs_sum

  device_id = 1
  open(device_id,file=filename)
  
  
  read(device_id,*)
  read(device_id,*) n,sqrt_s_gev
  
  
  ! Prepare procesess info array (0:n_)
  ! Check if info array had been already allocated
  if(allocated(proc_info_array)) then 
    ! [Allocated]
    ! Check if the previous 'n' and current 'n_' are different
    if(n.ne.n_max) then 
     ! [n != n_] - reallocate info array
     do i = 0,n_max                     
       call recycle_bundle(proc_info_array(i))  
     enddo
     deallocate(proc_info_array)
     
     allocate(proc_info_array(0:n))
     do i = 0,n_max
      call alloc_bundle(proc_info_array(i),i)
     enddo
   
    endif 
    ! [n == n_] - don't touch info array at all
  else   
    ! [Not allocated]
    ! Allocate and prepare info array if it had not been allocated yet
     allocate(proc_info_array(0:n))
     do i = 0,n
      call alloc_bundle(proc_info_array(i),i)
     enddo 
  endif
  
  
    ! Prepare cross-sections sum array
  if(allocated(cross_sections_sum)) then
    if(n.ne.n_max) then
      deallocate(cross_sections_sum)
      allocate(cross_sections_sum(0:n))
    endif
  else
    allocate(cross_sections_sum(0:n))
  endif
 
  n_max = n
 
  cs_sum = 0.0
  do i = 0,n_max
    call read_bundle(device_id,proc_info_array(i))
    cs_sum = cs_sum + proc_info_array(i)%cross_section
    cross_sections_sum(i) =  cs_sum
  enddo
  
  cs_range = cross_sections_sum(n)
    
  ! Generate new seed for prng
  call mt_init_seed()
  
end subroutine mc_init_from_file

subroutine mc_close()
  integer:: i
  
  if(allocated(proc_info_array)) then 
     do i = 0,n_max                    
       call recycle_bundle(proc_info_array(i))  
     enddo
     deallocate(proc_info_array)
   endif

end subroutine mc_close
 
integer function mc_getn()
  
  implicit none

  real(8):: mtRand
  
  mtRand = cs_range*grnd()
  
  mc_getn = 0
  do mc_getn = 0,n_max
    if(mtRand.lt.cross_sections_sum(mc_getn)) then
      return 
    endif
  enddo
  
  
end function mc_getn
 
subroutine mc_genRapidities(n,y)

   implicit none
   
   integer:: n
   type(process_info_bundle):: bundle
   real(8):: y(1:n)
!~    real(8):: temp(1:n)
!~    real(8):: temp_M(1:n)
   real(8):: det
   
   
   y = 0.0
   bundle = proc_info_array(n)
   
   call sub_determinant(n,bundle%zyTransformMatrix,det)
   print*, det
   
!~    temp_M = MATMUL(bundle%D2y_eigenvecs,bundle%Y0)

end subroutine mc_genRapidities
 
end module mc_engine_module
