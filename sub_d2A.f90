subroutine sub_d2A(n,sqrt_s,X,A,d2A)
 
 use constants_module
 
 implicit none
 
 ! input
 integer:: n
 real(8):: sqrt_s,X(1:3*n+2)
 
 ! output
 real(8):: d2A(1:3*n+2,1:3*n+2)
 
 ! temp 
 integer:: nomper1,nomper2
 real(8):: P3(0:3),dP3(0:3,1:3*n+2)
 real(8):: P4(0:3),dP4(0:3,1:3*n+2)
 real(8):: A,dLnA(1:3*n+2),d2LnA(1:3*n+2,1:3*n+2)
 
 
interface

 subroutine sub_dmomentums(n,sqrt_s,X,P3,P4,dP3,dP4)
 
 use constants_module
 
 implicit none

  ! input
  integer:: n
  real(8):: sqrt_s
  real(8):: X(1:3*n+2)
 
  ! output
  real(8):: P3(0:3), P4(0:3)
  real(8):: dP3(0:3,1:3*n+2),dP4(0:3,1:3*n+2)
 
  ! temp
  integer :: i
  real(8):: px(1:n),py(1:n),y(1:n),PaX,PaY
  real(8):: Panti_x,Panti_y,l,k,t,A1,B1,D,P3perp_kvad,P4perp_kvad 
  real(8):: P3par,P4par,P30,P40,controlEnergy,podkor
  real(8):: dP3par_po_dt,dP3par_po_dP3x,dP3par_po_dP3y
  real(8):: dP3par_po_dPpar_pi,dP3par_po_dk,dE_pi_po_dyj(1:n)
  real(8):: dE_pi_po_dpxj(1:n),dE_pi_po_dpyj(1:n)
  real(8):: dPpar_pi_po_dyj(1:n),dPpar_pi_po_dpxj(1:n),dPpar_pi_po_dpyj(1:n)
  real(8):: dt_po_dyj(1:n),dt_po_dpxj(1:n),dt_po_dpyj(1:n)
  real(8):: dP3perpx_po_dpxj(1:n),dP3perpx_po_dPanti_x,dP3perpy_po_dPanti_y,dP3perpy_po_dpyj(1:n)
  real(8):: dP4perpx_po_dpxj(1:n),dP4perpx_po_dPanti_x,dP4perpy_po_dpyj(1:n),dP4perpy_po_dPanti_y
  real(8):: dk_po_dpxj(1:n),dk_po_dpyj(1:n),dk_po_dPanti_x,dk_po_dPanti_y
  real(8):: dl_po_dyj(1:n),dl_po_dpxj(1:n),dl_po_dpyj(1:n)
  real(8):: dP3par_po_dl
  real(8):: dPpar_pi_po_vsem(1:3*n+2),dl_po_vsem(1:3*n+2),dk_po_vsem(1:3*n+2),dt_po_vsem(1:3*n+2)
  real(8):: dP3perpx_po_vsem(1:3*n+2),dP3perpy_po_vsem(1:3*n+2),dP3par_po_vsem(1:3*n+2)
  real(8):: dP4par_po_vsem(1:3*n+2),dP4perpx_po_vsem(1:3*n+2),dP4perpy_po_vsem(1:3*n+2)
  real(8):: dP40_po_vsem(1:3*n+2),dP30_po_vsem(1:3*n+2)
  real(8):: dP3parvs(1:3*n+2),dP4parvs(1:3*n+2)
  real(8):: dP30(1:3*n+2),dP40(1:3*n+2)
  
  end subroutine sub_dmomentums

 subroutine sub_dlnA (n,sqrt_s,X,P3,dP3,dlnA)
  
  use constants_module
  
  implicit none 
 
  ! input
  integer:: n
  real(8):: sqrt_s
  real(8):: X(1:3*n+2)
  real(8):: P3(0:3),dP3(0:3,1:3*n+2)

  ! output
  real(8):: dlnA(1:3*n+2)
 
  ! temp
  integer:: b,c,nomper
  real(8):: px(1:n),py(1:n),y(1:n),PaX,PaY,P1(0:3)
  real(8):: p0(1:n),p_par(1:n),dp0(1:n,1:3*n+2),dpx(1:n,1:3*n+2),dpy(1:n,1:3*n+2),dp_par(1:n,1:3*n+2)
  real(8):: slag1,slag2,slag3,slag4,znamenatel
  real(8):: slaga1,slaga2,slaga3,slaga4
  real(8):: mnozh1,mnozh2,mnozh3,mnozh4,mnozh5,mnozh6,mnozh7,mnozh8
 end subroutine sub_dlnA

 subroutine sub_d2lnA (n,sqrt_s,X,d2lnA)

   use constants_module

   implicit none 
 
   ! input
   integer:: n
   real(8):: sqrt_s
   real(8):: X(1:3*n+2)
   
   ! output
   real(8):: d2lnA(1:3*n+2,1:3*n+2)
   
   ! temp
   integer::b,c,nomper1,nomper2
 
   real(8):: px(1:n),py(1:n),y(1:n),PaX,PaY,P1(0:3)
   real(8):: P3(0:3),P4(0:3),dP3(0:3,1:3*n+2),dP4(0:3,1:3*n+2)
   real(8):: d2P30(1:3*n+2,1:3*n+2),d2P40(1:3*n+2,1:3*n+2)
   real(8):: d2P3parvs(1:3*n+2,1:3*n+2),d2P4parvs(1:3*n+2,1:3*n+2)
 
   real(8):: p0(1:n),p_par(1:n),dp0(1:n,1:3*n+2),dpx(1:n,1:3*n+2),dpy(1:n,1:3*n+2),dp_par(1:n,1:3*n+2)
   real(8):: d2p0(1:n,1:3*n+2,1:3*n+2),d2p_par(1:n,1:3*n+2,1:3*n+2)
 
   real(8):: slag1,slag2,slag3,slag4,znamenatel
   real(8):: slaga1,slaga2,slaga3,slaga4
   real(8):: mnozh1,mnozh2,mnozh3,mnozh4,mnozh5,mnozh6,mnozh7,mnozh8
   real(8):: dmnozh1,dmnozh2,dmnozh3,dmnozh5,dmnozh7,dmnozh8
   real(8):: dslag1,dslag2,dslag3,dslag4,dznamenatel
   real(8):: dslaga1,dslaga2,dslaga3,dslaga4
   real(8):: Aa,dAa
 end subroutine sub_d2lnA
 
end interface
 

 d2A=0.0
 call sub_dmomentums(n,sqrt_s,X,P3,P4,dP3,dP4)
 call sub_dlnA (n,sqrt_s,X,P3,dP3,dlnA)
 call sub_d2lnA (n,sqrt_s,X,d2lnA)

 do nomper1=1,3*n+2
  do nomper2=1,3*n+2
   d2A(nomper1,nomper2)=A*(d2LnA(nomper1,nomper2)+dlnA(nomper1)*dlnA(nomper2))
  enddo
 enddo 

end subroutine sub_d2A
