subroutine sub_d34lnA(n,sqrt_s,y,d3lnA,d4lnA)

 use constants_module

 implicit none

 ! input
 integer:: n
 real(8):: sqrt_s
 real(8):: y(1:n)

 ! output
 real(8):: d3lnA(1:n,1:n,1:n),d4lnA(1:n,1:n,1:n,1:n)
  
 ! temp
 integer:: a,b,c,d,i
 real(8):: P1(0:3),S
 real(8):: P3(0:1),dP3(0:1,1:n),d2P3(0:1,1:n,1:n),d3P30(1:n,1:n,1:n)
 real(8):: d3P3_par(1:n,1:n,1:n),d4P30(1:n,1:n,1:n,1:n), d4P3_par(1:n,1:n,1:n,1:n)
 real(8):: sc,dsa,dsb,dsc,dsd,temp1
 real(8):: d4s
 real(8):: slag1,slag2 
 real(8):: mnozh1,mnozh2,mnozh3
 real(8):: coshy(1:n),sinhy(1:n)
 
interface

 subroutine sub_d34momentums(n,sqrt_s,y,P3,dP3,d2P3,d3P30,d3P3_par,d4P30,d4P3_par)
 
 use constants_module
 
 implicit none
 
 ! input
 integer:: n
 real(8):: sqrt_s
 real(8):: y(1:n)
 
 ! output
 real(8):: P3(0:1),dP3(0:1,1:n),d2P3(0:1,1:n,1:n) 
 real(8):: d3P30(1:n,1:n,1:n),d4P30(1:n,1:n,1:n,1:n)
 real(8):: d3P3_par(1:n,1:n,1:n),d4P3_par(1:n,1:n,1:n,1:n)

 ! temp 
 integer:: a,b,c,d,i
 real(8):: MPI2,MPI3,MPI4,E_,K,znam,temp,temp1,temp2,temp3
 
 ! Here P3(0:1)  , P3(0) = P3,0; P3(1) = P3_par ; P3y,P3y = 0 !
 real(8):: dK(1:n),d2K(1:n,1:n),d3K(1:n,1:n,1:n),d4K(1:n,1:n,1:n,1:n)
 real(8):: Pv,Ek,EP
 real(8):: P4(0:1)
 real(8):: dEk(1:n),dEP(1:n)
 real(8):: d2Ek(1:n,1:n),d2EP(1:n,1:n)
 real(8):: d3Ek(1:n,1:n,1:n),d3EP(1:n,1:n,1:n)
 real(8):: d4Ek(1:n,1:n,1:n,1:n),d4EP(1:n,1:n,1:n,1:n)

 ! 4speed
 real(8):: sinhy(1:n),coshy(1:n)
 real(8):: R_EP2,R_EP3,R_EP4
 real(8):: sqrt_znam
 real(8):: r_znam,r_sqrt_znam,r_znam15,r_znam25,r_znam35
 
 end subroutine sub_d34momentums

end interface

 
 P1(0) = sqrt_s/2.0
 P1(1) = 0.0
 P1(2) = 0.0
 P1(3) = sqrt(((sqrt_S/2)**2.0)-SQR_M)

 do i = 1,n
  coshy(i) = cosh(y(i))
  sinhy(i) = sinh(y(i))
 enddo

 d3lnA = 0.0
 d4lnA = 0.0

 call sub_d34momentums(n,sqrt_s,y,P3,dP3,d2P3,d3P30,d3P3_par,d4P30,d4P3_par)
 
do b = 1,n
 do c = b,n
  do d = c,n
    S = 0.0
    
    do i = 0,n
      sc = bracket(n,i,sinhy,coshy,P1,P3)
    
      dsb = dbracket(n,i,sinhy,coshy,P1,P3,dP3,b)
      dsc = dbracket(n,i,sinhy,coshy,P1,P3,dP3,c)
      dsd = dbracket(n,i,sinhy,coshy,P1,P3,dP3,d)
      
                      
      mnozh1 = d2bracket(n,i,sinhy,coshy,P1,P3,dP3,d2P3,b,c)*dsd
      mnozh2 = d2bracket(n,i,sinhy,coshy,P1,P3,dP3,d2P3,b,d)*dsc
      mnozh3 = d2bracket(n,i,sinhy,coshy,P1,P3,dP3,d2P3,c,d)*dsb
      
     temp1 = - 2.0*dsb*dsc*dsd/(sc**3.0) + (mnozh1 + mnozh2 + mnozh3)/(sc**2.0)
     S = S + temp1 - d3bracket(n,i,sinhy,coshy,P1,P3,dP3,d2P3,d3P30,d3P3_par,b,c,d)/sc
     
     
    enddo
    
    d3lnA(b,c,d) = S
    d3lnA(b,d,c) = S
    d3lnA(c,b,d) = S
    d3lnA(c,d,b) = S
    d3lnA(d,b,c) = S
    d3lnA(d,c,b) = S
   enddo
  enddo
 enddo
  
do a = 1,n 
 do b = a,n
  do c = b,n
   do d = c,n
    S = 0.0
    do i = 0,n
      sc = bracket(n,i,sinhy,coshy,P1,P3)
    
      dsa = dbracket(n,i,sinhy,coshy,P1,P3,dP3,a)
      dsb = dbracket(n,i,sinhy,coshy,P1,P3,dP3,b)
      dsc = dbracket(n,i,sinhy,coshy,P1,P3,dP3,c)
      dsd = dbracket(n,i,sinhy,coshy,P1,P3,dP3,d)

    S = S + 6.0*dsa*dsb*dsc*dsd/(sc**4.0)

   slag1 =       - dsa*dsb*d2bracket(n,i,sinhy,coshy,P1,P3,dP3,d2P3,c,d)
   slag1 = slag1 - dsa*dsc*d2bracket(n,i,sinhy,coshy,P1,P3,dP3,d2P3,b,d)
   slag1 = slag1 - dsa*dsd*d2bracket(n,i,sinhy,coshy,P1,P3,dP3,d2P3,b,c)
   slag1 = slag1 - dsb*dsc*d2bracket(n,i,sinhy,coshy,P1,P3,dP3,d2P3,a,d)
   slag1 = slag1 - dsb*dsd*d2bracket(n,i,sinhy,coshy,P1,P3,dP3,d2P3,a,c)
   slag1 = slag1 - dsc*dsd*d2bracket(n,i,sinhy,coshy,P1,P3,dP3,d2P3,a,b)
   slag1 = slag1*2.0/(sc**3.0)
   
   
   slag2 = d2bracket(n,i,sinhy,coshy,P1,P3,dP3,d2P3,a,b)*d2bracket(n,i,sinhy,coshy,P1,P3,dP3,d2P3,c,d)
   slag2 = slag2 + d2bracket(n,i,sinhy,coshy,P1,P3,dP3,d2P3,a,c)*d2bracket(n,i,sinhy,coshy,P1,P3,dP3,d2P3,b,d)
   slag2 = slag2 + d2bracket(n,i,sinhy,coshy,P1,P3,dP3,d2P3,a,d)*d2bracket(n,i,sinhy,coshy,P1,P3,dP3,d2P3,b,c)
   slag2 = slag2 + dsa*d3bracket(n,i,sinhy,coshy,P1,P3,dP3,d2P3,d3P30,d3P3_par,b,c,d)
   slag2 = slag2 + dsb*d3bracket(n,i,sinhy,coshy,P1,P3,dP3,d2P3,d3P30,d3P3_par,a,c,d)
   slag2 = slag2 + dsc*d3bracket(n,i,sinhy,coshy,P1,P3,dP3,d2P3,d3P30,d3P3_par,a,b,d)
   slag2 = slag2 + dsd*d3bracket(n,i,sinhy,coshy,P1,P3,dP3,d2P3,d3P30,d3P3_par,a,b,c)
   slag2 = slag2/(sc**2.0)



    d4s = d4bracket(n,i,sinhy,coshy,P1,P3,dP3,d2P3,d3P30,d3P3_par,d4P30,d4P3_par,a,b,c,d)
    S = S + slag1 + slag2 - d4s/sc     
    enddo
    
     d4lnA(a,b,c,d) = S         
     d4lnA(a,b,d,c) = S
     d4lnA(a,c,b,d) = S
     d4lnA(a,c,d,b) = S
     d4lnA(a,d,b,c) = S
     d4lnA(a,d,c,b) = S
     d4lnA(b,a,c,d) = S
     d4lnA(b,a,d,c) = S
     d4lnA(b,c,a,d) = S
     d4lnA(b,c,d,a) = S
     d4lnA(b,d,a,c) = S
     d4lnA(b,d,c,a) = S
     d4lnA(c,a,b,d) = S
     d4lnA(c,a,d,b) = S
     d4lnA(c,b,a,d) = S
     d4lnA(c,b,d,a) = S
     d4lnA(c,d,a,b) = S
     d4lnA(c,d,b,a) = S
     d4lnA(d,a,b,c) = S
     d4lnA(d,a,c,b) = S
     d4lnA(d,b,a,c) = S
     d4lnA(d,b,c,a) = S
     d4lnA(d,c,a,b) = S
     d4lnA(d,c,b,a) = S
    

   enddo
  enddo
 enddo
enddo

 contains
!i - index in total SUM of lnA. 
real(8) function bracket(n,i,sinhy,coshy,P1,P3)   !True
 implicit none
  integer:: n,i1,i
  real(8):: sinhy(1:n),coshy(1:n)
  real(8):: P1(0:3),P3(0:1)
  real(8):: slag1,slag2
  
  slag1 = P1(0)-P3(0)
  do i1=1,i 
  slag1 = slag1 - MPI*coshy(i1)
  enddo
 
  slag2 = P1(3) - P3(1)
  do i1 = 1,i
  slag2 = slag2 - MPI*sinhy(i1)
  enddo

  bracket = SQR_MPI - (slag1**2.0)+(slag2**2.0)

end function bracket

real(8) function dbracket(n,i,sinhy,coshy,P1,P3,dP3,d)
  
  integer:: n,d
  integer:: i,i1
  real(8):: P1(0:3), P3(0:1)
  real(8):: dP3(0:1,1:n)
  real(8):: skob1,skob2,skob3
  real(8):: sinhy(1:n),coshy(1:n)

  
  skob1 = P1(0)-P3(0)
  do i1 = 1,i
   skob1 = skob1 - MPI*coshy(i1)
  enddo
  
  skob2 = dP3(0,d)
   if(i.ge.d) skob2 = skob2 + MPI*sinhy(d)
  
  skob1 = skob1*skob2
  
  
  skob2 = P1(3) - P3(1)
  do i1 = 1,i
   skob2 = skob2 - MPI*sinhy(i1)
  enddo
  
  skob3 = dP3(1,d)
   if(i.ge.d) skob3 = skob3 + MPI*coshy(d)
  
  dbracket = 2.0*(skob1 - skob2*skob3)
  
end function dbracket

real(8) function d2bracket(n,i,sinhy,coshy,P1,P3,dP3,d2P3,c,d)
  implicit none
  integer:: n,c,d
  integer:: i,i1
  real(8):: P1(0:3), P3(0:1)
  real(8):: dP3(0:1,1:n),d2P3(0:1,1:n,1:n)
  real(8):: skob1,skob2
  real(8):: sinhy(1:n),coshy(1:n)

!1  
  skob1 = dP3(0,c)
   if(i.ge.c) skob1 = skob1 + MPI*sinhy(c)
  
  skob2 = dP3(0,d)
   if(i.ge.d) skob2 = skob2 + MPI*sinhy(d)
  
  d2bracket = - skob1*skob2

!2  
  skob1 = P1(0) - P3(0)
  do i1 = 1,i
   skob1 = skob1 - MPI*coshy(i1)
  enddo
  
  skob2 = d2P3(0,c,d)
   if((i.ge.d).and.(c.eq.d)) skob2 = skob2 + MPI*coshy(d)

  d2bracket = d2bracket + skob1*skob2

!3  
  skob1 = dP3(1,c)
   if(i.ge.c) skob1 = skob1 + MPI*coshy(c)
  
  skob2 = dP3(1,d)
   if(i.ge.d) skob2 = skob2 + MPI*coshy(d)
   
   d2bracket = d2bracket + skob1*skob2

!4   
  skob1 = P1(3) - P3(1)
  do i1= 1,i
   skob1 = skob1 - MPI*sinhy(i1)
  enddo
  
  skob2 = d2P3(1,c,d)
   if((i.ge.c).and.(c.eq.d)) skob2 = skob2 + MPI*sinhy(d)
  
  d2bracket = d2bracket - skob1*skob2
  d2bracket = 2.0*d2bracket

end function d2bracket

real(8) function d3bracket(n,i,sinhy,coshy,P1,P3,dP3,d2P3,d3P30,d3P3_par,b,c,d)
  implicit none 
  integer:: n,b,c,d
  integer:: i,i1
  real(8):: P1(0:3), P3(0:1)
  real(8):: dP3(0:1,1:n),d2P3(0:1,1:n,1:n),d3P30(1:n,1:n,1:n)
  real(8):: d3P3_par(1:n,1:n,1:n)
  real(8):: skob1,skob2
  real(8):: sinhy(1:n),coshy(1:n) 
!1 
  skob1 = d2P3(0,b,c)
   if((i.ge.c).and.(b.eq.c)) skob1 = skob1 + MPI*coshy(b)
  
  skob2 = dP3(0,d)
   if(i.ge.d) skob2 = skob2 + MPI*sinhy(d)
  
  d3bracket = - skob1*skob2
  
!2 
  
  skob1 = dP3(0,c)
   if(i.ge.c) skob1 = skob1 + MPI*sinhy(c)
  
  skob2 = d2P3(0,b,d)
   if((i.ge.b).and.(b.eq.d)) skob2 = skob2  + MPI*coshy(b)
  
  d3bracket = d3bracket - skob1*skob2
  
!3 
  
  skob1 = dP3(0,b)
   if(i.ge.b) skob1 = skob1 + MPI*sinhy(b)
   
  skob2 = d2P3(0,c,d)
   if((i.ge.c).and.(c.eq.d)) skob2 = skob2 + MPI*coshy(c)
   
  d3bracket = d3bracket - skob1*skob2
  
!4
 
  skob1 = P1(0) - P3(0)
   do i1 = 1,i
    skob1 = skob1 - MPI*coshy(i1)
   enddo
  
  skob2 = d3P30(b,c,d)
   if((i.ge.b).and.(b.eq.c).and.(b.eq.d)) skob2 = skob2 + MPI*sinhy(b)
   
  d3bracket = d3bracket + skob1*skob2

!5 

  skob1 = d2P3(1,b,c)
   if((i.ge.b).and.(b.eq.c)) skob1 = skob1 + MPI*sinhy(c)
  
  skob2 = dP3(1,d)
   if(i.ge.d) skob2 = skob2 + MPI*coshy(d)
   
  d3bracket = d3bracket + skob1*skob2
  
!6
 
  skob1 = dP3(1,c)
   if(i.ge.c) skob1 = skob1 + MPI*coshy(c)
   
  skob2 = d2P3(1,b,d)
   if((i.ge.d).and.(b.eq.d)) skob2 = skob2 + MPI*sinhy(d)
   
  d3bracket = d3bracket + skob1*skob2
  
!7
  
  skob1 = dP3(1,b)
   if(i.ge.b) skob1 = skob1 + MPI*coshy(b)
   
  skob2 = d2P3(1,c,d)
   if((i.ge.c).and.(c.eq.d)) skob2 = skob2 + MPI*sinhy(c)
   
  d3bracket = d3bracket + skob1*skob2
  
!8
   
  skob1 = P1(3) - P3(1)
   do i1 = 1,i
    skob1 = skob1 - MPI*sinhy(i1)
   enddo
  
  skob2 = d3P3_par(b,c,d)
   if((i.ge.b).and.(b.eq.c).and.(b.eq.d)) skob2 = skob2 + MPI*coshy(b)
   
  d3bracket = 2.0*(d3bracket - skob1*skob2)
 
  
end function d3bracket

real(8) function d4bracket(n,i,sinhy,coshy,P1,P3,dP3,d2P3,d3P30,d3P3_par,d4P30,d4P3_par,a,b,c,d)
  
  implicit none 
  integer:: n,a,b,c,d
  integer:: i,j
  real(8):: P1(0:3), P3(0:1)
  real(8):: dP3(0:1,1:n),d2P3(0:1,1:n,1:n),d3P30(1:n,1:n,1:n),d3P3_par(1:n,1:n,1:n)
  real(8):: d4P30(1:n,1:n,1:n,1:n),d4P3_par(1:n,1:n,1:n,1:n)
  real(8):: skob1,skob2
  real(8):: sinhy(1:n),coshy(1:n)
!1 slag

  skob1 = dP3(0,a)
   if(i.ge.a) skob1 = skob1 + MPI*sinhy(a)
   
  skob2 = d3P30(b,c,d)
    if((i.ge.b).and.(b.eq.c).and.(b.eq.d)) skob2 = skob2  + MPI*sinhy(b)

  d4bracket = - skob1*skob2

!2 slag

  skob1 = dP3(0,b)
   if(i.ge.b) skob1 = skob1 + MPI*sinhy(b)
   
  skob2 = d3P30(a,c,d)
   if((i.ge.a).and.(a.eq.c).and.(a.eq.d)) skob2 = skob2  + MPI*sinhy(a)

  d4bracket = d4bracket - skob1*skob2

!3 slag

  skob1 = dP3(0,c)
   if(i.ge.c) skob1 = skob1 + MPI*sinhy(c)
   
  skob2 = d3P30(a,b,d)
   if((i.ge.a).and.(a.eq.b).and.(a.eq.d)) skob2 = skob2  + MPI*sinhy(a)

  d4bracket = d4bracket - skob1*skob2

!4 slag

  skob1 = dP3(0,d)
   if(i.ge.d) skob1 = skob1 + MPI*sinhy(d)
   
  skob2 = d3P30(a,b,c)
  if((i.ge.a).and.(a.eq.b).and.(a.eq.c)) skob2 = skob2  + MPI*sinhy(a)

  d4bracket = d4bracket - skob1*skob2


!5 slag

   skob1 = d2P3(0,a,b)
     if((i.ge.a).and.(a.eq.b)) skob1 = skob1 + MPI*coshy(a)
   
   skob2 = d2P3(0,c,d)
     if((i.ge.c).and.(c.eq.d)) skob2 = skob2 + MPI*coshy(c)

   d4bracket = d4bracket - skob1*skob2

!6 slag

   skob1 = d2P3(0,a,c)
     if((i.ge.a).and.(a.eq.c)) skob1 = skob1 + MPI*coshy(a)
   
   skob2 = d2P3(0,b,d)
     if((i.ge.b).and.(b.eq.d)) skob2 = skob2 + MPI*coshy(b)

   d4bracket = d4bracket - skob1*skob2

!7 slag

   skob1 = d2P3(0,a,d)
     if((i.ge.a).and.(a.eq.d)) skob1 = skob1 + MPI*coshy(a)
   
   skob2 = d2P3(0,b,c)
     if((i.ge.b).and.(b.eq.c)) skob2 = skob2 + MPI*coshy(b)

   d4bracket = d4bracket - skob1*skob2

!!P3_par !!!!!!!!!!!!!!!!

!8 slag

  skob1 = dP3(1,a)
   if(i.ge.a) skob1 = skob1 + MPI*coshy(a)
   
  skob2 = d3P3_par(b,c,d)
   if((i.ge.b).and.(b.eq.c).and.(b.eq.d)) skob2 = skob2  + MPI*coshy(b)

  d4bracket = d4bracket + skob1*skob2

!9 slag

  skob1 = dP3(1,b)
   if(i.ge.b) skob1 = skob1 + MPI*coshy(b)
   
  skob2 = d3P3_par(a,c,d)
   if((i.ge.a).and.(a.eq.c).and.(a.eq.d)) skob2 = skob2  + MPI*coshy(a)

  d4bracket = d4bracket + skob1*skob2
!10 slag

  skob1 = dP3(1,c)
   if(i.ge.c) skob1 = skob1 + MPI*coshy(c)
   
  skob2 = d3P3_par(a,b,d)
   if((i.ge.a).and.(a.eq.b).and.(a.eq.d)) skob2 = skob2  + MPI*coshy(a)

  d4bracket = d4bracket + skob1*skob2

!11 slag

  skob1 = dP3(1,d)
   if(i.ge.d) skob1 = skob1 + MPI*coshy(d)
   
  skob2 = d3P3_par(a,b,c)
   if((i.ge.a).and.(a.eq.b).and.(a.eq.c)) skob2 = skob2  + MPI*coshy(a)

  d4bracket = d4bracket + skob1*skob2

!12 slag

   skob1 = d2P3(1,a,b)
     if((i.ge.a).and.(a.eq.b)) skob1 = skob1 + MPI*sinhy(a)
   
   skob2 = d2P3(1,c,d)
     if((i.ge.c).and.(c.eq.d)) skob2 = skob2 + MPI*sinhy(c)

   d4bracket = d4bracket + skob1*skob2

!13 slag

   skob1 = d2P3(1,a,c)
     if((i.ge.a).and.(a.eq.c)) skob1 = skob1 + MPI*sinhy(a)
   
   skob2 = d2P3(1,b,d)
     if((i.ge.b).and.(b.eq.d)) skob2 = skob2 + MPI*sinhy(b)

   d4bracket = d4bracket + skob1*skob2

!14 slag

   skob1 = d2P3(1,a,d)
     if((i.ge.a).and.(a.eq.d)) skob1 = skob1 + MPI*sinhy(a)
   
   skob2 = d2P3(1,b,c)
     if((i.ge.b).and.(b.eq.c)) skob2 = skob2 + MPI*sinhy(b)

   d4bracket = d4bracket + skob1*skob2

!15 slag
   
   skob1 = P1(0) - P3(0)
    do j = 1,i
     skob1 = skob1 - MPI*coshy(j)
    enddo  
   
   skob2 = d4P30(a,b,c,d)
    if((i.ge.a).and.(a.eq.b).and.(a.eq.c).and.(a.eq.d)) skob2 = skob2 + MPI*coshy(a)
   
   d4bracket = d4bracket + skob1*skob2

  
  
!16 slag
   
   skob1 = P1(3) - P3(1)
    do j = 1,i
     skob1 = skob1 - MPI*sinhy(j)
    enddo  
   skob2 = d4P3_par(a,b,c,d)
    if((i.ge.a).and.(a.eq.b).and.(a.eq.c).and.(a.eq.d)) skob2 = skob2 + MPI*sinhy(a)
   
   d4bracket = d4bracket - skob1*skob2

   d4bracket = d4bracket*2.0

end function d4bracket

end subroutine sub_d34lnA

