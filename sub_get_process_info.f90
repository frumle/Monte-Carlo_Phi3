subroutine sub_get_process_info(n,n_groups,sqrt_s_gev,info)

 use process_info_type
 use constants_module
 use factorials_module
 use matrices
 
 implicit none

 ! inpus
 integer:: n, n_groups
 real(8):: sqrt_s_gev
 
 real(8), allocatable:: X(:), Xmax(:), Y0_form_approx(:,:)

 ! output
 type(process_info_bundle):: info
 
 ! temp
 real(8):: sqrt_s,cross_section
 real(8):: maximization_step,maximization_precision

 integer, allocatable:: npa(:) 
 real(8):: P1(0:3), P2(0:3), P3(0:3), P4(0:3)
 real(8):: A, AInterf, interfFact
 real(8), allocatable:: D2A(:,:), D2Ay(:,:),D2Apx(:,:)
 real(8), allocatable:: D2AyInterf(:,:),D2ApxInterf(:,:)
 real(8), allocatable:: D3A(:,:,:),D4A(:,:,:,:)
 real(8), allocatable:: D3AInterf(:,:,:),D4AInterf(:,:,:,:)
 real(8), allocatable:: D2lnAyInterf(:,:),D2lnApxInterf(:,:)
 real(8), allocatable:: D3lnAInterf(:,:,:),D4lnAInterf(:,:,:,:)

 integer:: perCount
 integer, allocatable:: perMat(:,:,:)
 
 real(8):: P12,inv_flow, integral_coef, amplitude_coef
 integer:: i
 
 real(8), allocatable:: d2y_eigenvals(:), d2y_eigenvecs(:,:)
 real(8), allocatable:: vec(:), newVec(:), Mtemp(:,:)
 real(8):: diff
 integer:: k
 
 interface
 
 subroutine fillGroups(n,n_groups,npa)
  
  ! input
  integer:: n,n_groups
  
  ! output
  integer:: npa(1:n_groups)
  
 end subroutine fillGroups
 
 subroutine sub_symmetrization2D(n,M2)

 implicit none

 ! input
  integer:: n
  real(8):: M2(1:n,1:n)

 ! temp
 integer:: na,b
 real(8):: temp
 
 end subroutine sub_symmetrization2D
 
 subroutine phi3_partial_maximization(n,n_groups,npa,sqrt_s,m_step,m_precision,X0,Xmax)
   
   implicit none
   
   !~ Set "PRINT_TIME_ELAPSED = .TRUE." to print the time elapsed for maximization.
   logical, parameter:: PRINT_TIME_ELAPSED = .TRUE.
   
   ! Enable/Disable verification of the maximization 
   logical, parameter:: VERIFY_MAXIMUM = .TRUE.
   real(8), parameter:: VERIFICATION_PRECISION = 1e-4
   
   ! input
   integer:: n, n_groups
   integer:: npa(1:n_groups)
   real(8):: sqrt_s
   real(8):: m_step,m_precision ! maximization step and precision
   real(8):: X0(1:3*n+2)  ! maximization start point
   
   ! output
   real(8):: Xmax(1:3*n+2)! maximum point
   
   ! temp
   real(8):: squared_precision
   integer:: i,k,l,array_size
   real(8):: exit_counter
   real(8):: grad(1:n_groups)

   


   ! time counters
   integer:: time_scale,exec_start_time,exec_end_time 
   
   ! model
   real(8):: P3(0:3),P4(0:3)
   real(8):: dP3(0:3,1:3*n+2),dP4(0:3,1:3*n+2)
   real(8):: dlnA(1:3*n+2)
   
   ! check maximum
   real(8):: lnA,max_lnA
   real(8):: Xcheck(1:3*n+2),check_m_step
   
   
 end subroutine phi3_partial_maximization
 
 subroutine sub_momentums(n,sqrt_s,X,P3,P4)
   use constants_module
   implicit none

   ! input
   integer :: n
   real(8):: sqrt_s
   real(8):: X(1:3*n+2)
 
   ! output
   real(8):: P3(0:3),P4(0:3)
 end subroutine sub_momentums
 
 subroutine sub_A(n,sqrt_s,X,P3,A)
 
 use constants_module
 
 implicit none
 
 ! input
 integer::n
 real(8):: sqrt_s
 real(8):: X(1:3*n+2)
 
 ! output
 real(8):: A

 ! temp
 integer::b,c
 real(8):: P3(0:3)
 real(8):: px(1:n),py(1:n),y(1:n),PaX,PaY,P1(0:3)
 real(8):: slag1,slag2,slag3,slag4
 real(8):: p0(1:n),p_par(1:n),dp0(1:n,1:3*n+2),dpx(1:n,1:3*n+2),dpy(1:n,1:3*n+2),dp_par(1:n,1:3*n+2)
 
 end subroutine sub_A

 subroutine sub_d2A(n,sqrt_s,X,A,d2A)
 
 use constants_module
 
 implicit none
 
 ! input
 integer:: n
 real(8):: sqrt_s,X(1:3*n+2)
 
 ! output
 real(8):: d2A(1:3*n+2,1:3*n+2)
 
 ! temp 
 integer:: nomper1,nomper2
 real(8):: P3(0:3),dP3(0:3,1:3*n+2)
 real(8):: P4(0:3),dP4(0:3,1:3*n+2)
 real(8):: A,dLnA(1:3*n+2),d2LnA(1:3*n+2,1:3*n+2)
 
 end subroutine sub_d2A
 
 subroutine sub_d34A(n,sqrt_s,X,A_max,d3A,d4A)
 
 implicit none
 
 ! input
 integer:: n 
 real(8):: sqrt_s
 reaL(8):: X(1:3*n+2)
 real(8):: A_max     ! Scattering amplitude value in the maximum point.

 ! output 
 real(8):: d3A(1:n,1:n,1:n), d4A(1:n,1:n,1:n,1:n)
 
 ! temp
 integer:: a, b, c, d
 real(8):: y(1:n),d2lnA(1:3*n+2,1:3*n+2)
 real(8):: dlnA(1:3*n+2)
 real(8):: d3lnA(1:n,1:n,1:n), d4lnA(1:n,1:n,1:n,1:n)
 real(8):: temp,temp1
 real(8):: P3(0:3),P4(0:3)
 real(8):: dP3(0:3,1:3*n+2),dP4(0:4,1:3*n+2)
 
 end subroutine sub_d34A
 
 subroutine sub_d2Ainterf(n,c,npa,D,Dpx,Dinterf,Dpxinterf)
 implicit none

 integer:: n,c,npa(1:c)
 real(8):: D(1:n,1:n),Dpx(1:n+1,1:n+1),Dinterf(1:n,1:n),Dpxinterf(1:n+1,1:n+1),sumdia,diafacto
 integer:: ind(1:c,1:n),i,i1,j,c1,c2
 real(8):: sumnedia,nediafacto,proizv_facto,dla_raznix_c
 real(8):: sumdiapx,sumnediapx,dla_raznix_c_px,sumPanti

 end subroutine sub_d2Ainterf
 
 subroutine sub_d34Ainterf(n,c,npa,M3,M4,Mi3,Mi4)
 implicit none
 
 integer:: i,i1,i2,i3,i4
 integer:: j1,j2,j3,j4
 integer:: c1,c2,c3,c4,c
 real(8):: npa_fact
 integer:: n
 integer:: npa(1:c)
 real(8):: M3(1:n,1:n,1:n),M4(1:n,1:n,1:n,1:n)
 real(8):: Mi3(1:n,1:n,1:n),Mi4(1:n,1:n,1:n,1:n)
 real(8):: maindiag,diag12,diag13,diag23,nondiag
 real(8):: m4element(1:15),m4coef(1:15)
 integer:: counter
 real(8):: maindiagCoef,diag12Coef,diag13Coef,diag23Coef,nondiagCoef
 real(8):: temp
 
 end subroutine sub_d34Ainterf
 
 subroutine sub_d2lnAInterf(n,AInterf,Dinterf,Dpxinterf,lnDinteref,lnDpxinteref)

 implicit none

 ! input
 integer:: n
 real(8):: AInterf, Dinterf(1:n,1:n), Dpxinterf(1:n+1,1:n+1)
 
 ! output
 real(8):: lnDinteref(1:n,1:n),lnDpxinteref(1:n+1,1:n+1)

 ! temp
 integer:: i,j
 real(8):: temp
 
 end subroutine sub_d2lnAInterf
 
 subroutine sub_d34lnAinterf(n,AInterf,D2AInterf,D3AInterf,D4AInterf,d3lnAInterf,d4lnAInterf)
 
 implicit none
 
 ! input
 integer:: n
 real(8):: AInterf
 real(8):: D2AInterf(1:n,1:n),D3AInterf(1:n,1:n,1:n),D4AInterf(1:n,1:n,1:n,1:n)

 ! output
  real(8):: d3lnAInterf(1:n,1:n,1:n),d4lnAInterf(1:n,1:n,1:n,1:n)

 ! temp
 integer:: a,b,c,d
 real(8):: temp
 
 end subroutine sub_d34lnAinterf
 
 subroutine sub_buildPerMatrices(n_groups,npa,matricesCount,matrices)

 ! input
 integer:: n_groups,npa(1:n_groups)
 
 ! output 
 integer:: matricesCount
 integer, allocatable:: matrices(:,:,:)

 ! temp
 integer:: temp_matrices(1:999999,1:n_groups,1:n_groups)
 integer:: maxli(1:n_groups,1:n_groups),a,b,l11,l12,l21,l22
 integer:: nomer

 end subroutine sub_buildPerMatrices
 
 subroutine sub_interf_contrib(n,n_groups,npa,X,perCount,perMatrices,Y0_form,D2,D2px,D3,D4,contrib)

  implicit none
  
  ! input
  integer:: n, n_groups, npa(1:n_groups)
  real(8):: X(1:3*n+2) 
  integer:: perCount,perMatrices(1:perCount,1:n_groups,1:n_groups)
  real(8):: D2(1:n,1:n),D2px(1:n+1,1:n+1)
  real(8):: D3(1:n,1:n,1:n),D4(1:n,1:n,1:n,1:n)
 
  ! output
  real(8):: contrib
  
  ! temp
  integer:: ind,a,zap,nvs
  integer:: indexi1(1:npa(1)),indexi2(1:npa(2)),indexi3(1:npa(3)),Per(1:n)
  integer:: gra1,gra2,gra3
  real(8):: mnojitel1,mnojitel2,mnojitel3,mnojitel
  real(8):: Y(1:n)
  real(8):: interfvklad
  real(8):: interfvkladpx
  real(8):: Y0_form(1:perCount,1:n)
  
  end subroutine sub_interf_contrib
 
 subroutine sub_d2lnA (n,sqrt_s,X,d2lnA)

   use constants_module

   implicit none 
 
   ! input
   integer:: n
   real(8):: sqrt_s
   real(8):: X(1:3*n+2)
   
   ! output
   real(8):: d2lnA(1:3*n+2,1:3*n+2)
   
   ! temp
   integer::b,c,nomper1,nomper2
 
   real(8):: px(1:n),py(1:n),y(1:n),PaX,PaY,P1(0:3)
   real(8):: P3(0:3),P4(0:3),dP3(0:3,1:3*n+2),dP4(0:3,1:3*n+2)
   real(8):: d2P30(1:3*n+2,1:3*n+2),d2P40(1:3*n+2,1:3*n+2)
   real(8):: d2P3parvs(1:3*n+2,1:3*n+2),d2P4parvs(1:3*n+2,1:3*n+2)
 
   real(8):: p0(1:n),p_par(1:n),dp0(1:n,1:3*n+2),dpx(1:n,1:3*n+2),dpy(1:n,1:3*n+2),dp_par(1:n,1:3*n+2)
   real(8):: d2p0(1:n,1:3*n+2,1:3*n+2),d2p_par(1:n,1:3*n+2,1:3*n+2)
 
   real(8):: slag1,slag2,slag3,slag4,znamenatel
   real(8):: slaga1,slaga2,slaga3,slaga4
   real(8):: mnozh1,mnozh2,mnozh3,mnozh4,mnozh5,mnozh6,mnozh7,mnozh8
   real(8):: dmnozh1,dmnozh2,dmnozh3,dmnozh5,dmnozh7,dmnozh8
   real(8):: dslag1,dslag2,dslag3,dslag4,dznamenatel
   real(8):: dslaga1,dslaga2,dslaga3,dslaga4
   real(8):: Aa,dAa
 
 
 end subroutine sub_d2lnA
 
 subroutine sub_jacobi(matrix_size,matrix,eigenvalues,eigenvectors)

 logical, parameter:: CHECK_DIAGONALIZATION = .FALSE.
 logical, parameter:: PRINT_TIME_ELAPSED = .FALSE.
 logical, parameter:: PRESYMMETRIZATION_ENABLED = .TRUE.
 real(8), parameter:: DIAGONALIZATION_PRECISION = 1e-8


 integer:: matrix_size,n,iteration,index_i,index_j
 real(8):: matrix(1:matrix_size,1:matrix_size),eigenvalues(1:matrix_size)
 real(8):: eigenvectors(1:matrix_size,1:matrix_size),eigenvectorsOld(1:matrix_size,1:matrix_size)
 real(8):: matrixNew(1:matrix_size,1:matrix_size),matrixOld(1:matrix_size,1:matrix_size)

 real(8):: maxElement,maxElementOld
 real(8):: cosFi,sinFi,summ,signsin,b,Kangle,difference,fi
 integer:: exec_begining_time,exec_end_time,time_scale
 real(8):: sqr_b
 
 end subroutine sub_jacobi
 
 end interface
 
 sqrt_s = sqrt_s_gev/DIMEN
 
 maximization_step = 1e-3
 maximization_precision = 1e-8
 

 allocate(X(1:3*n+2),Xmax(1:3*n+2),npa(1:n_groups))
 allocate(D2A(1:3*n+2,1:3*n+2))
 allocate(D3A(1:n,1:n,1:n),D4A(1:n,1:n,1:n,1:n))
 
 allocate(D2Ay(1:n,1:n),D2Apx(1:n+1,1:n+1))
 allocate(D2AyInterf(1:n,1:n),D2ApxInterf(1:n+1,1:n+1))
 allocate(D3AInterf(1:n,1:n,1:n),D4AInterf(1:n,1:n,1:n,1:n))
 
 allocate(D2lnAyInterf(1:n,1:n),D2lnApxInterf(1:n+1,1:n+1))
 allocate(D3lnAInterf(1:n,1:n,1:n),D4lnAInterf(1:n,1:n,1:n,1:n))
 
 allocate(d2y_eigenvals(1:n), d2y_eigenvecs(1:n,1:n))
 allocate(vec(1:n),newVec(1:n),Mtemp(1:n,1:n))
 
 
 call fillGroups(n,n_groups,npa)

 call phi3_partial_maximization(n,n_groups,npa,sqrt_s,maximization_step,maximization_precision,X,Xmax)

 call sub_momentums(n,sqrt_s,Xmax,P3,P4)
 

 call sub_A(n,sqrt_s,Xmax,P3,A)
 
 interfFact = 1.0
 do i = 1,n_groups
   interfFact = interfFact*factorial(npa(i))
 enddo
 AInterf = A*interfFact
 
 call sub_d2A(n,sqrt_s,Xmax,A,D2A)
 call sub_d34A(n,sqrt_s,Xmax,A,d3A,d4A)

 D2Ay(1:n,1:n)=d2A(1:n,1:n)
 call sub_symmetrization2D(n,D2Ay)
   
 D2Apx(1:n,1:n)=d2A(n+1:2*n,n+1:2*n)
 D2Apx(n+1,1:n)=d2A(3*n+1,n+1:2*n)
 D2Apx(1:n,n+1)=d2A(n+1:2*n,3*n+1)
 D2Apx(n+1,n+1)=d2A(3*n+1,3*n+1)

 call sub_d2Ainterf(n,n_groups,npa,D2Ay,D2Apx,D2AyInterf,D2ApxInterf)
 call sub_d34Ainterf(n,n_groups,npa,D3A,D4A,D3AInterf,D4AInterf)

 call sub_d2lnAInterf(n,AInterf,D2AyInterf,D2ApxInterf,D2lnAyInterf,D2lnApxInterf)
 call sub_d34lnAinterf(n,AInterf,D2AyInterf,D3AInterf,D4AInterf,D3lnAInterf,D4lnAInterf)

 D2lnAyInterf = 0.5*D2lnAyInterf
 D2lnApxInterf = 0.5*D2lnApxInterf
 D3lnAInterf = (1.0/6.0)*D3lnAInterf
 D4lnAInterf = (1.0/24.0)*D4lnAInterf
 
 call sub_buildPerMatrices(n_groups,npa,perCount,perMat)

 allocate(Y0_form_approx(1:perCount,1:n))
 Y0_form_approx = 0.0
 
 call sub_interf_contrib(n,n_groups,npa,Xmax,perCount,perMat,Y0_form_approx,D2lnAyInterf &
                         ,D2lnApxInterf,D3lnAInterf,D4lnAInterf,cross_section)
  

 P1(0) = 0.5*sqrt_s
 P1(3) = sqrt(P1(0)*P1(0) - SQR_M)
 P1(1) = 0.0
 P1(2) = 0.0
 P2 = P1
 P2(3) = -P1(3)

 P12 = P1(0)*P2(0)
 do i=1,3
 P12 = P12 - P1(i)*P2(i)
 enddo
 inv_flow = sqrt(P12*P12 - SQR_M*SQR_M)
 
 cross_section = cross_section*AInterf*AInterf/(inv_flow*(P3(3)*P4(0)-P4(3)*P3(0)))
  
  
 ! coefficients
 integral_coef = 1.0/((2*PI)**(3*n + 6))
 integral_coef = integral_coef/(2**(n + 2))
 integral_coef = integral_coef/(sqrt(PI)**(3*n + 2)) 
 amplitude_coef = 1.0/((2*PI)**(8*n + 8)) ! |A|^2
 
 cross_section = cross_section*integral_coef*amplitude_coef
  
  
  
 ! copy data to info bundle
 info%n = n
 info%sqrt_s_gev = sqrt_s_gev
 info%cross_section = cross_section
 
!~  do i = 1,n
!~   info%Z0(i) = Xmax(i)
!~  enddo
!~  do i = 1,n
!~   info%Px0(i) = Xmax(n + i)
!~  enddo
!~  do i = 1,n
!~   info%Py0(i) = Xmax(2*n + i)
!~  enddo


 call sub_d2lnA (n,sqrt_s,Xmax,D2A)
 D2Ay(1:n,1:n)= D2A(1:n,1:n)
 call sub_jacobi(n,D2Ay,d2y_eigenvals,d2y_eigenvecs)
 
!~  print*, "####"
!~  d2Ay = TRANSPOSE(d2y_eigenvecs)
!~  d2Ay = MATMUL(d2y_eigenvecs,d2Ay)
!~  do i = 1,n
!~    print*, d2Ay(i,1:n)
!~  enddo
!~  call sub_determinant(n,d2Ay,cross_section)
!~  print*, cross_section
 
!~  info%d2y_eigenvals(1:n) = d2y_eigenvals(1:n)
!~  info%d2y_eigenvecs(1:n,1:n) = d2y_eigenvecs(1:n,1:n)

  do i = 1,n
   do k = 1,n
    Mtemp(i,k) = d2y_eigenvals(i)*d2y_eigenvecs(i,k)
   enddo
  enddo
  
  d2y_eigenvecs = TRANSPOSE(d2y_eigenvecs)
  Mtemp = MATMUL(d2y_eigenvecs,Mtemp)
  
!~   print*, checkEqual(d2Ay,d2Ay)
  
  do i = 1,n
    print*, d2Ay(i,1:n)
  enddo
  print*, "Symmetry: ", checkSymmetry(n,d2Ay(1:n,1:n))
  
 
!~  vec(1:n) = d2y_eigenvecs(1:n,1)
!~  newVec = MATMUL(d2Ay,vec)
!~  diff = 0.0
!~  do i = 1,n
!~    diff = diff + (newVec(i) - d2y_eigenvals(1)*vec(i))**2
!~  enddo 
!~  diff = sqrt(diff)
!~  print*, diff
 
 
   
 deallocate(X,Xmax,npa)
 deallocate(D2A,D2Ay,D2Apx,D3A,D4A)
 deallocate(D2AyInterf,D2ApxInterf,D3AInterf,D4AInterf)
 deallocate(D2lnAyInterf,D2lnApxInterf,D3lnAInterf,D4lnAInterf)
 deallocate(perMat)
 deallocate(vec,newVec,Mtemp)

end subroutine sub_get_process_info
