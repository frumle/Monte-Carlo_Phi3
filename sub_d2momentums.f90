subroutine sub_d2momentums(n,sqrt_s,y,px,py,PaX,PaY,P3,P4,d2P3parvs,d2P4parvs,d2P30,d2P40,dP3,dP4)

  use constants_module
  implicit none
  
  ! input
  integer:: n
  real(8):: sqrt_s
  real(8):: px(1:n),py(1:n),y(1:n),PaX,PaY
  ! output
  real(8):: P3(0:3),P4(0:3)
  real(8):: d2P3parvs(1:3*n+2,1:3*n+2),d2P4parvs(1:3*n+2,1:3*n+2)
  real(8):: d2P30(1:3*n+2,1:3*n+2),d2P40(1:3*n+2,1:3*n+2)
  real(8):: dP3(0:3,1:3*n+2),dP4(0:3,1:3*n+2)
  
  ! temp
  integer :: i,j
  real(8):: Panti_x,Panti_y,l,k,t,A1,B1,D,P3perp_kvad,P4perp_kvad
  real(8):: P3par,P4par,P30,P40,controlEnergy,podkor
  real(8):: dP3par_po_dt,dP3par_po_dP3x,dP3par_po_dP3y
  real(8):: dP3par_po_dPpar_pi,dP3par_po_dk,dE_pi_po_dyj(1:n)
  real(8):: dE_pi_po_dpxj(1:n),dE_pi_po_dpyj(1:n)
  real(8):: dPpar_pi_po_dyj(1:n),dPpar_pi_po_dpxj(1:n),dPpar_pi_po_dpyj(1:n)
  real(8):: dt_po_dyj(1:n),dt_po_dpxj(1:n),dt_po_dpyj(1:n)
  real(8):: dP3perpx_po_dpxj(1:n),dP3perpx_po_dPanti_x,dP3perpy_po_dPanti_y,dP3perpy_po_dpyj(1:n)
  real(8):: dP4perpx_po_dpxj(1:n),dP4perpx_po_dPanti_x,dP4perpy_po_dpyj(1:n),dP4perpy_po_dPanti_y
  real(8):: dk_po_dpxj(1:n),dk_po_dpyj(1:n),dk_po_dPanti_x,dk_po_dPanti_y
  real(8):: dl_po_dyj(1:n),dl_po_dpxj(1:n),dl_po_dpyj(1:n)
  real(8):: d2P3par_po_dt2,d2P3par_po_dk2,d2P3par_po_dkdt
  real(8):: d2P3par_po_dP3perp_x2,d2P3par_po_dP3perp_y2,d2P3par_po_dP3perp_xdP3perp_y
  real(8):: d2P3par_po_dP3perp_xdt,d2P3par_po_dP3perp_ydt,d2P3par_po_dP3perp_xdk,d2P3par_po_dP3perp_ydk
  real(8):: d2P3par_po_dPpar_pi2,d2P3par_po_dPpar_pidt,d2P3par_po_dPpar_pidP3perp_x,d2P3par_po_dPpar_pidP3perp_y
  real(8):: d2P3par_po_dPpar_pidk,dP3par_po_dl,d2P3par_po_dl2,dpodkor_po_dl,d2P3par_po_dPpar_pidl
  real(8):: d2P3par_po_dldk,d2P3par_po_dldt,d2P3par_po_dlP3perp_x,d2P3par_po_dlP3perp_y
  real(8):: d2Ppar_pi_po_dyidyj(1:n,1:n),d2Ppar_pi_po_dpxidpxj(1:n,1:n),d2Ppar_pi_po_dpyidpyj(1:n,1:n)
  real(8):: d2Ppar_pi_po_dyidpxj(1:n,1:n),d2Ppar_pi_po_dyidpyj(1:n,1:n),d2Ppar_pi_po_dpxidpyj(1:n,1:n)
  real(8):: d2E_pi_po_dyidyj(1:n,1:n),d2E_pi_po_dpxidpxj(1:n,1:n),d2E_pi_po_dpyidpyj(1:n,1:n)
  real(8):: d2E_pi_po_dyidpxj(1:n,1:n),d2E_pi_po_dyidpyj(1:n,1:n),d2E_pi_po_dpxidpyj(1:n,1:n)
  real(8):: d2t_po_dyidyj(1:n,1:n),d2t_po_dpxidpxj(1:n,1:n),d2t_po_dpyidpyj(1:n,1:n),d2t_po_dyidpxj(1:n,1:n)
  real(8):: d2t_po_dyidpyj(1:n,1:n),d2t_po_dpxidpyj(1:n,1:n),d2k_po_vsem2(1:3*n+2,1:3*n+2)
  real(8):: d2l_po_dyidyj(1:n,1:n),d2l_po_dpxidpxj(1:n,1:n),d2l_po_dpyidpyj(1:n,1:n),d2l_po_dyidpxj(1:n,1:n)
  real(8):: d2l_po_dyidpyj(1:n,1:n),d2l_po_dpyidpxj(1:n,1:n)
  real(8):: dPpar_pi_po_vsem(1:3*n+2),dl_po_vsem(1:3*n+2),dk_po_vsem(1:3*n+2),dt_po_vsem(1:3*n+2)
  real(8):: dP3perpx_po_vsem(1:3*n+2),dP3perpy_po_vsem(1:3*n+2),dP3par_po_vsem(1:3*n+2)
  real(8):: d2Ppar_pi_po_vsem2(1:3*n+2,1:3*n+2),d2l_po_vsem2(1:3*n+2,1:3*n+2),d2t_po_vsem2(1:3*n+2,1:3*n+2)
  real(8):: d2P3perp_x_po_vsem2(1:3*n+2,1:3*n+2),d2P3perp_y_po_vsem2(1:3*n+2,1:3*n+2),d2P3par_po_vsem21(1:3*n+2,1:3*n+2)
  real(8):: d2P3par_po_vsem22(1:3*n+2,1:3*n+2),d2P3par_po_vsem23(1:3*n+2,1:3*n+2),d2P3par_po_vsem24(1:3*n+2,1:3*n+2)
  real(8):: d2P3par_po_vsem25(1:3*n+2,1:3*n+2),d2P3par_po_vsem26(1:3*n+2,1:3*n+2),d2P3par_po_vsem2(1:3*n+2,1:3*n+2)
  real(8):: dP4par_po_vsem(1:3*n+2),d2P4par_po_vsem2(1:3*n+2,1:3*n+2),dP4perpx_po_vsem(1:3*n+2),dP4perpy_po_vsem(1:3*n+2)
  real(8):: dP40_po_vsem(1:3*n+2),dP30_po_vsem(1:3*n+2),d2P30_po_vsem2(1:3*n+2,1:3*n+2),d2P4perp_x_po_vsem2(1:3*n+2,1:3*n+2)
  real(8):: d2P4perp_y_po_vsem2(1:3*n+2,1:3*n+2),d2P40_po_vsem2(1:3*n+2,1:3*n+2)
  real(8):: dP3parvs(1:3*n+2),dP4parvs(1:3*n+2)
  real(8):: dP30(1:3*n+2),dP40(1:3*n+2)



Panti_x=PaX
Panti_y=PaY
call E_pi(MPI,n,px,py,y,A1)
call Ppar_pi(MPI,n,px,py,y,B1)




t=(sqrt_s-A1)*(sqrt_s-A1)
l=B1*B1-t
P3perp_kvad=P3perp_x(px,Panti_x,n)*P3perp_x(px,Panti_x,n)+P3perp_y(py,Panti_y,n)*P3perp_y(py,Panti_y,n)
P4perp_kvad=P4perp_x(px,Panti_x,n)*P4perp_x(px,Panti_x,n)+P4perp_y(py,Panti_y,n)*P4perp_y(py,Panti_y,n)
k=P4perp_kvad-P3perp_kvad


D = sqrt(t)*sqrt(4.0*(SQR_M+P3perp_kvad)*l+(l+k)*(l+k))
P3par=(B1*(l+k)+D)/(-2.0*l)
P4par=-P3par-B1
P30=sqrt(SQR_M+P3par*P3par+P3perp_kvad)
P40=sqrt(SQR_M+P4par*P4par+P4perp_kvad)
controlEnergy=sqrt_s-(P30+P40+A1)





P3(0)=P30
P3(1)=P3perp_x(px,Panti_x,n)
P3(2)=P3perp_y(py,Panti_y,n)
P3(3)=P3par

P4(0)=P40
P4(1)=P4perp_x(px,Panti_x,n)
P4(2)=P4perp_y(py,Panti_y,n)
P4(3)=P4par

!print*,"Mk",Mk,"SQR_M",SQR_M
!print*,"Mk",Mk,"P30*P30-(P3par*P3par+P3perp_kvad)",P30*P30-(P3par*P3par+P3perp_kvad)
!print*,P3par,P4par
!pervye proizvodnye

!scal1=P1(0)*P1(0)
!do i=1,3
!scal1=scal1-P1(i)*P1(i)
!enddo

!scal3=P3(0)*P3(0)
!do i=1,3
!scal3=scal3-P3(i)*P3(i)
!enddo

!scal13=(P1(0)-P3(0))*(P1(0)-P3(0))
!do i=1,3
!scal3=scal3-(P1(i)-P3(i))*(P1(i)-P3(i))
!enddo

!print*,"scal1",scal1,"M*M",M*M
!print*,"***scal3",scal3,"Mk",Mk
!print*,"scal13",scal13
!do i=0,3
!print*,"***P3(",i,")=",P3(i)
!enddo

dP3par_po_dt=(-1.0/(4.0*l*sqrt(t)))*sqrt(4.0*(SQR_M+P3perp_kvad)*l+(l+k)*(l+k))
!print*,"dP3par_po_dt",dP3par_po_dt
dP3par_po_dP3x=-2.0*sqrt(t)*P3perp_x(px,Panti_x,n)/sqrt(4.0*(SQR_M+P3perp_kvad)*l+(l+k)*(l+k))
!print*,"dP3par_po_dP3x",dP3par_po_dP3x
dP3par_po_dP3y=-2.0*sqrt(t)*P3perp_y(py,Panti_y,n)/sqrt(4.0*(SQR_M+P3perp_kvad)*l+(l+k)*(l+k))
!print*,"dP3par_po_dP3y",dP3par_po_dP3y
dP3par_po_dPpar_pi=(l+k)/(-2.0*l)
!print*,"dP3par_po_dPpar_pi",dP3par_po_dPpar_pi
dP3par_po_dk=B1/(-2.0*l)+(l+k)*sqrt(t)/(-2.0*l*sqrt(4.0*(SQR_M+P3perp_kvad)*l+(l+k)*(l+k)))
!print*,"dP3par_po_dk",dP3par_po_dk

do i=1,n
dE_pi_po_dyj(i)=mperp(MPI,px(i),py(i))*sinh(y(i))
enddo
!print*,"dE_pi_po_dy1",dE_pi_po_dyj(1)
!print*,"dE_pi_po_dy2",dE_pi_po_dyj(2)

do i=1,n
dE_pi_po_dpxj(i)=px(i)/(mperp(MPI,px(i),py(i)))*cosh(y(i))
enddo
!print*,"dE_pi_po_dpx1",dE_pi_po_dpxj(1)
!print*,"dE_pi_po_dpx2",dE_pi_po_dpxj(2)


do i=1,n
dE_pi_po_dpyj(i)=py(i)/(mperp(MPI,px(i),py(i)))*cosh(y(i))
enddo
!print*,"dE_pi_po_dpy1",dE_pi_po_dpyj(1)
!print*,"dE_pi_po_dpy2",dE_pi_po_dpyj(2)

do i=1,n
dPpar_pi_po_dyj(i)=mperp(MPI,px(i),py(i))*cosh(y(i))
enddo
!print*,"dPpar_pi_po_dy1",dPpar_pi_po_dyj(1)
!print*,"dPpar_pi_po_dy2",dPpar_pi_po_dyj(2)

do i=1,n
dPpar_pi_po_dpxj(i)=px(i)/(mperp(MPI,px(i),py(i)))*sinh(y(i))
enddo
!print*,"dPpar_pi_po_dpx1",dPpar_pi_po_dpxj(1)
!print*,"dPpar_pi_po_dpx2",dPpar_pi_po_dpxj(2)


do i=1,n
dPpar_pi_po_dpyj(i)=py(i)/(mperp(MPI,px(i),py(i)))*sinh(y(i))
enddo
!print*,"dPpar_pi_po_dpy1",dPpar_pi_po_dpyj(1)
!print*,"dPpar_pi_po_dpy2",dPpar_pi_po_dpyj(2)


dPpar_pi_po_vsem=0.0
do i=1,n
dPpar_pi_po_vsem(i)=dPpar_pi_po_dyj(i)
enddo
do i=n+1,2*n
dPpar_pi_po_vsem(i)=dPpar_pi_po_dpxj(i-n)
enddo
do i=2*n+1,3*n
dPpar_pi_po_vsem(i)=dPpar_pi_po_dpyj(i-2*n)
enddo
!do i=1,3*n+2
!print*,"dPpar_pi_po_vsem",dPpar_pi_po_vsem(i),"i",i
!enddo


do i=1,n
dt_po_dyj(i)=-2.0*(sqrt_s-A1)*dE_pi_po_dyj(i)
enddo
!print*,"dt_po_dy1",dt_po_dyj(1)
!print*,"dt_po_dy2",dt_po_dyj(2)

do i=1,n
dt_po_dpxj(i)=-2.0*(sqrt_s-A1)*dE_pi_po_dpxj(i)
enddo
!print*,"dt_po_dpx1",dt_po_dpxj(1)
!print*,"dt_po_dpx2",dt_po_dpxj(2)

do i=1,n
dt_po_dpyj(i)=-2.0*(sqrt_s-A1)*dE_pi_po_dpyj(i)
enddo
!print*,"dt_po_dpy1",dt_po_dpyj(1)
!print*,"dt_po_dpy2",dt_po_dpyj(2)


dt_po_vsem=0.0
do i=1,n
dt_po_vsem(i)=dt_po_dyj(i)
enddo
do i=n+1,2*n
dt_po_vsem(i)=dt_po_dpxj(i-n)
enddo
do i=2*n+1,3*n
dt_po_vsem(i)=dt_po_dpyj(i-2*n)
enddo
!do i=1,3*n+2
!print*,"dt_po_vsem",dt_po_vsem(i),"i",i
!enddo


do i=1,n
dP3perpx_po_dpxj(i)=-0.5
enddo
!print*,"dP3perpx_po_dpx1",dP3perpx_po_dpxj(1)
!print*,"dP3perpx_po_dpx2",dP3perpx_po_dpxj(2)

dP3perpx_po_dPanti_x=1.0


dP3perpx_po_vsem=0.0
do i=n+1,2*n
dP3perpx_po_vsem(i)=dP3perpx_po_dpxj(i-n)
enddo
dP3perpx_po_vsem(3*n+1)=dP3perpx_po_dPanti_x
!do i=1,3*n+2
!print*,"dP3perpx_po_vsem",dP3perpx_po_vsem(i),"i",i
!enddo


do i=1,n
dP3perpy_po_dpyj(i)=-0.5
enddo
!print*,"dP3perpy_po_dpy1",dP3perpy_po_dpyj(1)
!print*,"dP3perpy_po_dpy2",dP3perpy_po_dpyj(2)

dP3perpy_po_dPanti_y=1.0


dP3perpy_po_vsem=0.0
do i=2*n+1,3*n
dP3perpy_po_vsem(i)=dP3perpy_po_dpyj(i-2*n)
enddo
dP3perpy_po_vsem(3*n+2)=dP3perpy_po_dPanti_y
!do i=1,3*n+2
!print*,"dP3perpy_po_vsem",dP3perpy_po_vsem(i),"i",i
!enddo



do i=1,n
dP4perpx_po_dpxj(i)=-0.5
enddo



dP4perpx_po_dPanti_x=-1.0

dP4perpx_po_vsem=0.0
do i=n+1,2*n
dP4perpx_po_vsem(i)=dP4perpx_po_dpxj(i-n)
enddo
dP4perpx_po_vsem(3*n+1)=dP4perpx_po_dPanti_x
!do i=1,3*n+2
!print*,"dP4perpx_po_vsem",dP4perpx_po_vsem(i),"i",i
!enddo





do i=1,n
dP4perpy_po_dpyj(i)=-0.5
enddo

dP4perpy_po_dPanti_y=-1.0


dP4perpy_po_vsem=0.0
do i=2*n+1,3*n
dP4perpy_po_vsem(i)=dP4perpy_po_dpyj(i-2*n)
enddo
dP4perpy_po_vsem(3*n+2)=dP4perpy_po_dPanti_y
!do i=1,3*n+2
!print*,"dP4perpy_po_vsem",dP4perpy_po_vsem(i),"i",i
!enddo


do i=1,n
dk_po_dpxj(i)=P3perp_x(px,Panti_x,n)-P4perp_x(px,Panti_x,n)
enddo
!print*,"dk_po_dpx(1)",dk_po_dpxj(1)
!print*,"dk_po_dpx(2)",dk_po_dpxj(2)


do i=1,n
dk_po_dpyj(i)=P3perp_y(py,Panti_y,n)-P4perp_y(py,Panti_y,n)
enddo
!print*,"dk_po_dpy(1)",dk_po_dpyj(1)
!print*,"dk_po_dpy(2)",dk_po_dpyj(2)


dk_po_dPanti_x=-2.0*(P3perp_x(px,Panti_x,n)+P4perp_x(px,Panti_x,n))
!print*,"dk_po_dPanti_x",dk_po_dPanti_x

dk_po_dPanti_y=-2.0*(P3perp_y(py,Panti_y,n)+P4perp_y(py,Panti_y,n))
!print*,"dk_po_dPanti_y",dk_po_dPanti_y

dk_po_vsem=0.0
do i=n+1,2*n
dk_po_vsem(i)=dk_po_dpxj(i-n)
enddo
do i=2*n+1,3*n
dk_po_vsem(i)=dk_po_dpyj(i-2*n)
enddo
dk_po_vsem(3*n+1)=dk_po_dPanti_x
dk_po_vsem(3*n+2)=dk_po_dPanti_y
!do i=1,3*n+2
!print*,"dk_po_vsem",dk_po_vsem(i),"i",i
!enddo

podkor=4.0*(SQR_M+P3perp_kvad)*l+(l+k)*(l+k)


dP3par_po_dl=B1*(l+k)/(2.0*l*l)-B1/(2.0*l)+sqrt(t*podkor)/(2.0*l*l)
dP3par_po_dl=dP3par_po_dl-sqrt(t)*(4.0*(SQR_M+P3perp_kvad)+2.0*(l+k))/(4.0*l*sqrt(podkor))
!print*,"dP3par_po_dl",dP3par_po_dl



do i=1,n
dl_po_dyj(i)=2.0*B1*dPpar_pi_po_dyj(i)-dt_po_dyj(i)
enddo
!print*,"dl_po_dy1",dl_po_dyj(1)
!print*,"dl_po_dy2",dl_po_dyj(2)

do i=1,n
dl_po_dpxj(i)=2.0*B1*dPpar_pi_po_dpxj(i)-dt_po_dpxj(i)
enddo
!print*,"dl_po_dpxj1",dl_po_dpxj(1)
!print*,"dl_po_dpxj2",dl_po_dpxj(2)

do i=1,n
dl_po_dpyj(i)=2.0*B1*dPpar_pi_po_dpyj(i)-dt_po_dpyj(i)
enddo
!print*,"dl_po_dpyj1",dl_po_dpyj(1)
!print*,"dl_po_dpyj2",dl_po_dpyj(2)

dl_po_vsem=0.0
do i=1,n
dl_po_vsem(i)=dl_po_dyj(i)
enddo
do i=n+1,2*n
dl_po_vsem(i)=dl_po_dpxj(i-n)
enddo
do i=2*n+1,3*n
dl_po_vsem(i)=dl_po_dpyj(i-2*n)
enddo
!do i=1,3*n+2
!print*,"dl_po_vsem",dl_po_vsem(i),"i",i
!enddo



do i=1,3*n+2
dP3par_po_vsem(i)=dP3par_po_dPpar_pi*dPpar_pi_po_vsem(i)+dP3par_po_dl*dl_po_vsem(i)+dP3par_po_dk*dk_po_vsem(i)
dP3par_po_vsem(i)=dP3par_po_vsem(i)+dP3par_po_dt*dt_po_vsem(i)+dP3par_po_dP3x*dP3perpx_po_vsem(i)+dP3par_po_dP3y*dP3perpy_po_vsem(i)
enddo
!do i=1,3*n+2
!print*,"dP3par_po_vsem",dP3par_po_vsem(i),"i",i
!write(210912,*)"dP3par_po_vsem",dP3par_po_vsem(i),"i",i
!enddo

do i=1,3*n+2
dP4par_po_vsem(i)=-dP3par_po_vsem(i)-dPpar_pi_po_vsem(i)
enddo
!do i=1,3*n+2
!print*,"dP4par_po_vsem",dP4par_po_vsem(i),"i",i
!write(2880912,*)"dP4par_po_vsem",dP4par_po_vsem(i),"i",i
!enddo


do i=1,3*n+2
dP40_po_vsem(i)=(P4par*dP4par_po_vsem(i)+P4perp_x(px,Panti_x,n)*dP4perpx_po_vsem(i)+P4perp_y(py,Panti_y,n)*dP4perpy_po_vsem(i))/P40
enddo
do i=1,3*n+2
!print*,"dP40_po_vsem",dP40_po_vsem(i),"i",i
!write(101020121,*)"dP40_po_vsem",dP40_po_vsem(i),"i",i
enddo


do i=1,3*n+2
dP30_po_vsem(i)=(P3par*dP3par_po_vsem(i)+P3perp_x(px,Panti_x,n)*dP3perpx_po_vsem(i)+P3perp_y(py,Panti_y,n)*dP3perpy_po_vsem(i))/P30
enddo
do i=1,3*n+2
!print*,"dP30_po_vsem",dP30_po_vsem(i),"i",i
!write(101020122,*)"dP30_po_vsem",dP30_po_vsem(i),"i",i
enddo




!vtorye proizvodnye

d2P3par_po_dt2=sqrt(podkor)/(8.0*l*sqrt(t)*sqrt(t)*sqrt(t))
!print*,"d2P3par_po_dt2",d2P3par_po_dt2

d2P3par_po_dk2=sqrt(t)*((podkor-(l+k)*(l+k))/(podkor*sqrt(podkor)))/(-2.0*l)
!print*,"d2P3par_po_dk2",d2P3par_po_dk2


d2P3par_po_dkdt=(l+k)/(-4.0*l*sqrt(t*podkor))
!print*,"d2P3par_po_dkdt",d2P3par_po_dkdt

d2P3par_po_dP3perp_x2=-2.0*sqrt(t)*(podkor-P3perp_x(px,Panti_x,n)*P3perp_x(px,Panti_x,n)*4.0*l)/(podkor*sqrt(podkor))
!print*,"d2P3par_po_dP3perp_x2",d2P3par_po_dP3perp_x2

d2P3par_po_dP3perp_y2=-2.0*sqrt(t)*(podkor-P3perp_y(py,Panti_y,n)*P3perp_y(py,Panti_y,n)*4.0*l)/(podkor*sqrt(podkor))
!print*,"d2P3par_po_dP3perp_y2",d2P3par_po_dP3perp_y2

d2P3par_po_dP3perp_xdP3perp_y=8.0*l*sqrt(t)*P3perp_x(px,Panti_x,n)*P3perp_y(py,Panti_y,n)/(sqrt(podkor)*sqrt(podkor)*sqrt(podkor))
!print*,"d2P3par_po_dP3perp_xdP3perp_y",d2P3par_po_dP3perp_xdP3perp_y

d2P3par_po_dP3perp_xdt=P3perp_x(px,Panti_x,n)/(-sqrt(t*podkor))
!print*,"d2P3par_po_dP3perp_xdt",d2P3par_po_dP3perp_xdt

d2P3par_po_dP3perp_ydt=P3perp_y(py,Panti_y,n)/(-sqrt(t*podkor))
!print*,"d2P3par_po_dP3perp_ydt",d2P3par_po_dP3perp_ydt

d2P3par_po_dP3perp_xdk=2.0*P3perp_x(px,Panti_x,n)*(l+k)*sqrt(t)/(sqrt(podkor)*sqrt(podkor)*sqrt(podkor))
!print*,"d2P3par_po_dP3perp_xdk",d2P3par_po_dP3perp_xdk

d2P3par_po_dP3perp_ydk=2.0*P3perp_y(py,Panti_y,n)*(l+k)*sqrt(t)/(sqrt(podkor)*sqrt(podkor)*sqrt(podkor))
!print*,"d2P3par_po_dP3perp_ydk",d2P3par_po_dP3perp_ydk

dpodkor_po_dl=2.0*(SQR_M+P3perp_kvad)+(l+k)
d2P3par_po_dl2=B1/(2.0*l*l)+(2.0*sqrt(t)*dpodkor_po_dl-l*sqrt(t))/(2.0*l*l*sqrt(podkor))
d2P3par_po_dl2=d2P3par_po_dl2-(2.0*sqrt(t*podkor)+B1*(l+2.0*k))/(2.0*l*l*l)
d2P3par_po_dl2=d2P3par_po_dl2+sqrt(t)*dpodkor_po_dl*dpodkor_po_dl/(2.0*l*sqrt(podkor)*sqrt(podkor)*sqrt(podkor))
!print*,"d2P3par_po_dl2",d2P3par_po_dl2

d2P3par_po_dPpar_pi2=0.0
d2P3par_po_dPpar_pidt=0.0
d2P3par_po_dPpar_pidP3perp_x=0.0
d2P3par_po_dPpar_pidP3perp_y=0.0

d2P3par_po_dPpar_pidk=1.0/(-2.0*l)
!print*,"d2P3par_po_dPpar_pidk",d2P3par_po_dPpar_pidk

d2P3par_po_dPpar_pidl=k/(2.0*l*l)
!print*,"d2P3par_po_dPpar_pidl",d2P3par_po_dPpar_pidl


d2P3par_po_dldk=B1/(2.0*l*l)+sqrt(t)*(l*sqrt(podkor)-(l+k)*(sqrt(podkor)+l*dpodkor_po_dl/sqrt(podkor)))/(-2.0*l*l*podkor)
!print*,"d2P3par_po_dldk",d2P3par_po_dldk

d2P3par_po_dldt=(dpodkor_po_dl*l-podkor)/(-4.0*l*l*sqrt(t*podkor))
!print*,"d2P3par_po_dldt",d2P3par_po_dldt

d2P3par_po_dlP3perp_x=2.0*P3perp_x(px,Panti_x,n)*sqrt(t)*dpodkor_po_dl/(sqrt(podkor)*sqrt(podkor)*sqrt(podkor))
!print*,"d2P3par_po_dlP3perp_x",d2P3par_po_dlP3perp_x

d2P3par_po_dlP3perp_y=2.0*P3perp_y(py,Panti_y,n)*sqrt(t)*dpodkor_po_dl/(sqrt(podkor)*sqrt(podkor)*sqrt(podkor))
!print*,"d2P3par_po_dlP3perp_y",d2P3par_po_dlP3perp_y

d2Ppar_pi_po_dyidyj=0.0
do j=1,n 
d2Ppar_pi_po_dyidyj(j,j)=mperp(MPI,px(j),py(j))*sinh(y(j))
enddo
!do j=1,n
!do i=1,n
!!print*,"d2Ppar_pi_po_dyidyj",d2Ppar_pi_po_dyidyj(j,i),"j",j,"i",i
!enddo
!enddo

d2Ppar_pi_po_dpxidpxj=0.0
do i=1,n 
d2Ppar_pi_po_dpxidpxj(i,i)=(MPI*MPI+py(i)*py(i))*sinh(y(i))
d2Ppar_pi_po_dpxidpxj(i,i)=d2Ppar_pi_po_dpxidpxj(i,i)/(mperp(MPI,px(i),py(i))*mperp(MPI,px(i),py(i))*mperp(MPI,px(i),py(i)))
enddo
!do j=1,n
!do i=1,n
!print*,"d2Ppar_pi_po_dpxidpxj",d2Ppar_pi_po_dpxidpxj(j,i),"j",j,"i",i
!enddo
!enddo


d2Ppar_pi_po_dpyidpyj=0.0
do i=1,n 
d2Ppar_pi_po_dpyidpyj(i,i)=(MPI*MPI+px(i)*px(i))*sinh(y(i))
d2Ppar_pi_po_dpyidpyj(i,i)=d2Ppar_pi_po_dpyidpyj(i,i)/(mperp(MPI,px(i),py(i))*mperp(MPI,px(i),py(i))*mperp(MPI,px(i),py(i)))
enddo
!do j=1,n
!do i=1,n
!print*,"d2Ppar_pi_po_dpyidpyj",d2Ppar_pi_po_dpyidpyj(j,i),"j",j,"i",i
!enddo
!enddo

d2Ppar_pi_po_dyidpxj=0.0
do i=1,n 
d2Ppar_pi_po_dyidpxj(i,i)=px(i)*cosh(y(i))/mperp(MPI,px(i),py(i))
enddo
!do j=1,n
!do i=1,n
!print*,"d2Ppar_pi_po_dyidpxj",d2Ppar_pi_po_dyidpxj(j,i),"j",j,"i",i
!enddo
!enddo


d2Ppar_pi_po_dyidpyj=0.0
do i=1,n 
d2Ppar_pi_po_dyidpyj(i,i)=py(i)*cosh(y(i))/mperp(MPI,px(i),py(i))
enddo
!do j=1,n
!do i=1,n
!print*,"d2Ppar_pi_po_dyidpyj",d2Ppar_pi_po_dyidpyj(j,i),"j",j,"i",i
!enddo
!enddo


d2Ppar_pi_po_dpxidpyj=0.0
do i=1,n 
d2Ppar_pi_po_dpxidpyj(i,i)=-py(i)*px(i)*sinh(y(i))/(mperp(MPI,px(i),py(i))*mperp(MPI,px(i),py(i))*mperp(MPI,px(i),py(i)))
enddo
!do j=1,n
!do i=1,n
!print*,"d2Ppar_pi_po_dpxidpyj",d2Ppar_pi_po_dpxidpyj(j,i),"j",j,"i",i
!enddo
!enddo


d2Ppar_pi_po_vsem2=0.0
do i=1,n
d2Ppar_pi_po_vsem2(i,i)=d2Ppar_pi_po_dyidyj(i,i)
d2Ppar_pi_po_vsem2(i,i+n)=d2Ppar_pi_po_dyidpxj(i,i)
d2Ppar_pi_po_vsem2(i,i+2*n)=d2Ppar_pi_po_dyidpyj(i,i)
d2Ppar_pi_po_vsem2(i+n,i)=d2Ppar_pi_po_dyidpxj(i,i)
d2Ppar_pi_po_vsem2(i+2*n,i)=d2Ppar_pi_po_dyidpyj(i,i)
enddo
do i=n+1,2*n
d2Ppar_pi_po_vsem2(i,i)=d2Ppar_pi_po_dpxidpxj(i-n,i-n)
d2Ppar_pi_po_vsem2(i,i+n)=d2Ppar_pi_po_dpxidpyj(i-n,i-n)
d2Ppar_pi_po_vsem2(i+n,i)=d2Ppar_pi_po_dpxidpyj(i-n,i-n)
enddo
do i=2*n+1,3*n
d2Ppar_pi_po_vsem2(i,i)=d2Ppar_pi_po_dpyidpyj(i-2*n,i-2*n)
enddo
!do j=1,3*n+2
!do i=1,3*n+2
!print*,"d2Ppar_pi_po_vsem2",d2Ppar_pi_po_vsem2(i,j),"j",j,"i",i
!enddo
!enddo



d2E_pi_po_dyidyj=0.0
do j=1,n 
d2E_pi_po_dyidyj(j,j)=mperp(MPI,px(j),py(j))*cosh(y(j))
enddo
!do j=1,n
!do i=1,n
!print*,"d2E_pi_po_dyidyj",d2E_pi_po_dyidyj(j,i),"j",j,"i",i
!enddo
!enddo


d2E_pi_po_dpxidpxj=0.0
do i=1,n 
d2E_pi_po_dpxidpxj(i,i)=(MPI*MPI+py(i)*py(i))*cosh(y(i))
d2E_pi_po_dpxidpxj(i,i)=d2E_pi_po_dpxidpxj(i,i)/(mperp(MPI,px(i),py(i))*mperp(MPI,px(i),py(i))*mperp(MPI,px(i),py(i)))
enddo
!do j=1,n
!do i=1,n
!print*,"d2E_pi_po_dpxidpxj",d2E_pi_po_dpxidpxj(j,i),"j",j,"i",i
!enddo
!enddo

d2E_pi_po_dpyidpyj=0.0
do i=1,n 
d2E_pi_po_dpyidpyj(i,i)=(MPI*MPI+px(i)*px(i))*cosh(y(i))
d2E_pi_po_dpyidpyj(i,i)=d2E_pi_po_dpyidpyj(i,i)/(mperp(MPI,px(i),py(i))*mperp(MPI,px(i),py(i))*mperp(MPI,px(i),py(i)))
enddo
!do j=1,n
!do i=1,n
!print*,"d2E_pi_po_dpyidpyj",d2E_pi_po_dpyidpyj(j,i),"j",j,"i",i
!enddo
!enddo

d2E_pi_po_dyidpxj=0.0
do i=1,n 
d2E_pi_po_dyidpxj(i,i)=px(i)*sinh(y(i))/mperp(MPI,px(i),py(i))
enddo
!do j=1,n
!do i=1,n
!print*,"d2E_pi_po_dyidpxj",d2E_pi_po_dyidpxj(j,i),"j",j,"i",i
!enddo
!enddo


d2E_pi_po_dyidpyj=0.0
do i=1,n 
d2E_pi_po_dyidpyj(i,i)=py(i)*sinh(y(i))/mperp(MPI,px(i),py(i))
enddo
!do j=1,n
!do i=1,n
!print*,"d2E_pi_po_dyidpyj",d2E_pi_po_dyidpyj(j,i),"j",j,"i",i
!enddo
!enddo

d2E_pi_po_dpxidpyj=0.0
do i=1,n 
d2E_pi_po_dpxidpyj(i,i)=-py(i)*px(i)*cosh(y(i))/(mperp(MPI,px(i),py(i))*mperp(MPI,px(i),py(i))*mperp(MPI,px(i),py(i)))
enddo
!do j=1,n
!do i=1,n
!print*,"d2E_pi_po_dpxidpyj",d2E_pi_po_dpxidpyj(j,i),"j",j,"i",i
!enddo
!enddo


d2t_po_dyidyj=0.0
do j=1,n
do i=1,n 
d2t_po_dyidyj(i,j)=-2.0*(-dE_pi_po_dyj(i)*dE_pi_po_dyj(j)+(sqrt_s-A1)*d2E_pi_po_dyidyj(i,j))
enddo
enddo
!do j=1,n
!do i=1,n
!print*,"d2t_po_dyidyj",d2t_po_dyidyj(i,j),"j",j,"i",i
!enddo
!enddo

d2t_po_dpxidpxj=0.0
do j=1,n
do i=1,n 
d2t_po_dpxidpxj(i,j)=-2.0*(-dE_pi_po_dpxj(i)*dE_pi_po_dpxj(j)+(sqrt_s-A1)*d2E_pi_po_dpxidpxj(i,j))
enddo
enddo
!do j=1,n
!do i=1,n
!print*,"d2t_po_dpxidpxj",d2t_po_dpxidpxj(i,j),"j",j,"i",i
!enddo
!enddo


d2t_po_dpyidpyj=0.0
do j=1,n
do i=1,n 
d2t_po_dpyidpyj(i,j)=-2.0*(-dE_pi_po_dpyj(i)*dE_pi_po_dpyj(j)+(sqrt_s-A1)*d2E_pi_po_dpyidpyj(i,j))
enddo
enddo
!do j=1,n
!do i=1,n
!print*,"d2t_po_dpyidpyj",d2t_po_dpyidpyj(i,j),"j",j,"i",i
!enddo
!enddo

d2t_po_dyidpxj=0.0
do j=1,n
do i=1,n 
d2t_po_dyidpxj(i,j)=-2.0*(-dE_pi_po_dpxj(i)*dE_pi_po_dyj(j)+(sqrt_s-A1)*d2E_pi_po_dyidpxj(i,j))
enddo
enddo
!do j=1,n
!do i=1,n
!print*,"d2t_po_dyidpxj",d2t_po_dyidpxj(i,j),"j",j,"i",i
!enddo
!enddo



d2t_po_dyidpyj=0.0
do j=1,n
do i=1,n 
d2t_po_dyidpyj(i,j)=-2.0*(-dE_pi_po_dpyj(i)*dE_pi_po_dyj(j)+(sqrt_s-A1)*d2E_pi_po_dyidpyj(i,j))
enddo
enddo
!do j=1,n
!do i=1,n
!print*,"d2t_po_dyidpyj",d2t_po_dyidpyj(i,j),"j",j,"i",i
!enddo
!enddo

d2t_po_dpxidpyj=0.0
do j=1,n
do i=1,n 
d2t_po_dpxidpyj(i,j)=-2.0*(-dE_pi_po_dpyj(j)*dE_pi_po_dpxj(i)+(sqrt_s-A1)*d2E_pi_po_dpxidpyj(i,j))
enddo
enddo
!do j=1,n
!do i=1,n
!print*,"d2t_po_dpxidpyj",d2t_po_dpxidpyj(i,j),"j",j,"i",i
!enddo
!enddo


d2t_po_vsem2=0.0
do i=1,n
do j=1,n
d2t_po_vsem2(i,j)=d2t_po_dyidyj(i,j)
d2t_po_vsem2(i,j+n)=d2t_po_dyidpxj(i,j)
d2t_po_vsem2(i,j+2*n)=d2t_po_dyidpyj(i,j)
d2t_po_vsem2(i+n,j)=d2t_po_dyidpxj(j,i)
d2t_po_vsem2(i+2*n,j)=d2t_po_dyidpyj(j,i)
enddo
enddo
do i=n+1,2*n
do j=n+1,2*n
d2t_po_vsem2(i,j)=d2t_po_dpxidpxj(i-n,j-n)
d2t_po_vsem2(i,j+n)=d2t_po_dpxidpyj(i-n,j-n)
d2t_po_vsem2(i+n,j)=d2t_po_dpxidpyj(j-n,i-n)
enddo
enddo
do i=2*n+1,3*n
do j=2*n+1,3*n
d2t_po_vsem2(i,j)=d2t_po_dpyidpyj(i-2*n,j-2*n)
enddo
enddo
!do j=1,3*n+2
!do i=1,3*n+2
!print*,"d2t_po_vsem2",d2t_po_vsem2(i,j),"j",j,"i",i
!enddo
!enddo



d2l_po_dyidyj=0.0
do j=1,n
do i=1,n 
d2l_po_dyidyj(i,j)=2.0*dPpar_pi_po_dyj(i)*dPpar_pi_po_dyj(j)+2.0*B1*d2Ppar_pi_po_dyidyj(i,j)-d2t_po_dyidyj(i,j)
enddo
enddo
!do j=1,n
!do i=1,n
!print*,"d2l_po_dyidyj",d2l_po_dyidyj(i,j),"j",j,"i",i
!enddo
!enddo

d2l_po_dpxidpxj=0.0
do j=1,n
do i=1,n 
d2l_po_dpxidpxj(i,j)=2.0*dPpar_pi_po_dpxj(i)*dPpar_pi_po_dpxj(j)+2.0*B1*d2Ppar_pi_po_dpxidpxj(i,j)-d2t_po_dpxidpxj(i,j)
enddo
enddo
!do j=1,n
!do i=1,n
!print*,"d2l_po_dpxidpxj",d2l_po_dpxidpxj(i,j),"j",j,"i",i
!enddo
!enddo

d2l_po_dpyidpyj=0.0
do j=1,n
do i=1,n 
d2l_po_dpyidpyj(i,j)=2.0*dPpar_pi_po_dpyj(i)*dPpar_pi_po_dpyj(j)+2.0*B1*d2Ppar_pi_po_dpyidpyj(i,j)-d2t_po_dpyidpyj(i,j)
enddo
enddo
!do j=1,n
!do i=1,n
!print*,"d2l_po_dpyidpyj",d2l_po_dpyidpyj(i,j),"j",j,"i",i
!enddo
!enddo

d2l_po_dyidpxj=0.0
do j=1,n
do i=1,n 
d2l_po_dyidpxj(i,j)=2.0*dPpar_pi_po_dpxj(i)*dPpar_pi_po_dyj(j)+2.0*B1*d2Ppar_pi_po_dyidpxj(i,j)-d2t_po_dyidpxj(i,j)
enddo
enddo
!do j=1,n
!do i=1,n
!print*,"d2l_po_dyidpxj",d2l_po_dyidpxj(i,j),"j",j,"i",i
!enddo
!enddo


d2l_po_dyidpyj=0.0
do j=1,n
do i=1,n 
d2l_po_dyidpyj(i,j)=2.0*dPpar_pi_po_dpyj(i)*dPpar_pi_po_dyj(j)+2.0*B1*d2Ppar_pi_po_dyidpyj(i,j)-d2t_po_dyidpyj(i,j)
enddo
enddo
!do j=1,n
!do i=1,n
!print*,"d2l_po_dyidpyj",d2l_po_dyidpyj(i,j),"j",j,"i",i
!enddo
!enddo


d2l_po_dpyidpxj=0.0
do j=1,n
do i=1,n 
d2l_po_dpyidpxj(i,j)=2.0*dPpar_pi_po_dpyj(j)*dPpar_pi_po_dpxj(i)+2.0*B1*d2Ppar_pi_po_dpxidpyj(i,j)-d2t_po_dpxidpyj(i,j)
enddo
enddo
!do j=1,n
!do i=1,n
!print*,"d2l_po_dpyidpxj",d2l_po_dpyidpxj(i,j),"j",j,"i",i
!enddo
!enddo

d2l_po_vsem2=0.0
do i=1,n
do j=1,n
d2l_po_vsem2(i,j)=d2l_po_dyidyj(i,j)
d2l_po_vsem2(i,j+n)=d2l_po_dyidpxj(i,j)
d2l_po_vsem2(i,j+2*n)=d2l_po_dyidpyj(i,j)
d2l_po_vsem2(i+n,j)=d2l_po_dyidpxj(j,i)
d2l_po_vsem2(i+2*n,j)=d2l_po_dyidpyj(j,i)
enddo
enddo
do i=n+1,2*n
do j=n+1,2*n
d2l_po_vsem2(i,j)=d2l_po_dpxidpxj(i-n,j-n)
d2l_po_vsem2(i,j+n)=d2l_po_dpyidpxj(i-n,j-n)
d2l_po_vsem2(i+n,j)=d2l_po_dpyidpxj(j-n,i-n)
enddo
enddo
do i=2*n+1,3*n
do j=2*n+1,3*n
d2l_po_vsem2(i,j)=d2l_po_dpyidpyj(i-2*n,j-2*n)
enddo
enddo
!do j=1,3*n+2
!do i=1,3*n+2
!print*,"d2l_po_vsem2",d2l_po_vsem2(i,j),"j",j,"i",i
!enddo
!enddo


d2k_po_vsem2=0.0
do i=n+1,2*n
d2k_po_vsem2(i,3*n+1)=2.0
enddo
do i=2*n+1,3*n
d2k_po_vsem2(i,3*n+2)=2.0
enddo
do i=n+1,2*n
d2k_po_vsem2(3*n+1,i)=2.0
enddo
do i=2*n+1,3*n
d2k_po_vsem2(3*n+2,i)=2.0
enddo
!do j=1,3*n+2
!do i=1,3*n+2
!print*,"d2k_po_vsem2",d2k_po_vsem2(i,j),"j",j,"i",i
!enddo
!enddo

d2P3perp_x_po_vsem2=0.0
d2P3perp_y_po_vsem2=0.0

d2P4perp_x_po_vsem2=0.0
d2P4perp_y_po_vsem2=0.0


do j=1,3*n+2
do i=1,3*n+2
d2P3par_po_vsem21(i,j)=(d2P3par_po_dPpar_pidl*dl_po_vsem(i)+d2P3par_po_dPpar_pidk*dk_po_vsem(i))*dPpar_pi_po_vsem(j)
d2P3par_po_vsem21(i,j)=d2P3par_po_vsem21(i,j)+dP3par_po_dPpar_pi*d2Ppar_pi_po_vsem2(i,j)

d2P3par_po_vsem22(i,j)=d2P3par_po_dPpar_pidl*dPpar_pi_po_vsem(i)*dl_po_vsem(j)+d2P3par_po_dl2*dl_po_vsem(i)*dl_po_vsem(j)
d2P3par_po_vsem22(i,j)=d2P3par_po_vsem22(i,j)+d2P3par_po_dldk*dk_po_vsem(i)*dl_po_vsem(j)
d2P3par_po_vsem22(i,j)=d2P3par_po_vsem22(i,j)+d2P3par_po_dldt*dt_po_vsem(i)*dl_po_vsem(j)
d2P3par_po_vsem22(i,j)=d2P3par_po_vsem22(i,j)+d2P3par_po_dlP3perp_x*dP3perpx_po_vsem(i)*dl_po_vsem(j)
d2P3par_po_vsem22(i,j)=d2P3par_po_vsem22(i,j)+d2P3par_po_dlP3perp_y*dP3perpy_po_vsem(i)*dl_po_vsem(j)
d2P3par_po_vsem22(i,j)=d2P3par_po_vsem22(i,j)+dP3par_po_dl*d2l_po_vsem2(i,j)

d2P3par_po_vsem23(i,j)=d2P3par_po_dPpar_pidk*dPpar_pi_po_vsem(i)*dk_po_vsem(j)+d2P3par_po_dldk*dl_po_vsem(i)*dk_po_vsem(j)
d2P3par_po_vsem23(i,j)=d2P3par_po_vsem23(i,j)+d2P3par_po_dk2*dk_po_vsem(i)*dk_po_vsem(j)
d2P3par_po_vsem23(i,j)=d2P3par_po_vsem23(i,j)+d2P3par_po_dkdt*dt_po_vsem(i)*dk_po_vsem(j)
d2P3par_po_vsem23(i,j)=d2P3par_po_vsem23(i,j)+d2P3par_po_dP3perp_xdk*dP3perpx_po_vsem(i)*dk_po_vsem(j)
d2P3par_po_vsem23(i,j)=d2P3par_po_vsem23(i,j)+d2P3par_po_dP3perp_ydk*dP3perpy_po_vsem(i)*dk_po_vsem(j)
d2P3par_po_vsem23(i,j)=d2P3par_po_vsem23(i,j)+dP3par_po_dk*d2k_po_vsem2(i,j)

d2P3par_po_vsem24(i,j)=d2P3par_po_dldt*dl_po_vsem(i)*dt_po_vsem(j)+d2P3par_po_dkdt*dk_po_vsem(i)*dt_po_vsem(j)
d2P3par_po_vsem24(i,j)=d2P3par_po_vsem24(i,j)+d2P3par_po_dt2*dt_po_vsem(i)*dt_po_vsem(j)
d2P3par_po_vsem24(i,j)=d2P3par_po_vsem24(i,j)+d2P3par_po_dP3perp_xdt*dP3perpx_po_vsem(i)*dt_po_vsem(j)
d2P3par_po_vsem24(i,j)=d2P3par_po_vsem24(i,j)+d2P3par_po_dP3perp_ydt*dP3perpy_po_vsem(i)*dt_po_vsem(j)
d2P3par_po_vsem24(i,j)=d2P3par_po_vsem24(i,j)+dP3par_po_dt*d2t_po_vsem2(i,j)

d2P3par_po_vsem25(i,j)=d2P3par_po_dlP3perp_x*dl_po_vsem(i)*dP3perpx_po_vsem(j)
d2P3par_po_vsem25(i,j)=d2P3par_po_vsem25(i,j)+d2P3par_po_dP3perp_xdk*dk_po_vsem(i)*dP3perpx_po_vsem(j)
d2P3par_po_vsem25(i,j)=d2P3par_po_vsem25(i,j)+d2P3par_po_dP3perp_xdt*dt_po_vsem(i)*dP3perpx_po_vsem(j)
d2P3par_po_vsem25(i,j)=d2P3par_po_vsem25(i,j)+d2P3par_po_dP3perp_x2*dP3perpx_po_vsem(i)*dP3perpx_po_vsem(j)
d2P3par_po_vsem25(i,j)=d2P3par_po_vsem25(i,j)+d2P3par_po_dP3perp_xdP3perp_y*dP3perpy_po_vsem(i)*dP3perpx_po_vsem(j)

d2P3par_po_vsem26(i,j)=d2P3par_po_dlP3perp_y*dl_po_vsem(i)*dP3perpy_po_vsem(j)
d2P3par_po_vsem26(i,j)=d2P3par_po_vsem26(i,j)+d2P3par_po_dP3perp_ydk*dk_po_vsem(i)*dP3perpy_po_vsem(j)
d2P3par_po_vsem26(i,j)=d2P3par_po_vsem26(i,j)+d2P3par_po_dP3perp_ydt*dt_po_vsem(i)*dP3perpy_po_vsem(j)
d2P3par_po_vsem26(i,j)=d2P3par_po_vsem26(i,j)+d2P3par_po_dP3perp_xdP3perp_y*dP3perpx_po_vsem(i)*dP3perpy_po_vsem(j)
d2P3par_po_vsem26(i,j)=d2P3par_po_vsem26(i,j)+d2P3par_po_dP3perp_y2*dP3perpy_po_vsem(i)*dP3perpy_po_vsem(j)

d2P3par_po_vsem2(i,j)=d2P3par_po_vsem21(i,j)+d2P3par_po_vsem22(i,j)+d2P3par_po_vsem23(i,j)+d2P3par_po_vsem24(i,j)
d2P3par_po_vsem2(i,j)=d2P3par_po_vsem2(i,j)+d2P3par_po_vsem25(i,j)+d2P3par_po_vsem26(i,j)
!print*,"d2P3par_po_vsem2",d2P3par_po_vsem2(i,j),"j",j,"i",i
!write(260912,*)"d2P3par_po_vsem2",d2P3par_po_vsem2(i,j),"j",j,"i",i
enddo
enddo

do j=1,3*n+2
do i=1,3*n+2
d2P4par_po_vsem2(i,j)=-d2P3par_po_vsem2(i,j)-d2Ppar_pi_po_vsem2(i,j)
!print*,"d2P4par_po_vsem2",d2P4par_po_vsem2(i,j),"j",j,"i",i
!write(280912,*)"d2P4par_po_vsem2",d2P4par_po_vsem2(i,j),"j",j,"i",i
enddo
enddo


do j=1,3*n+2
do i=1,3*n+2
d2P40_po_vsem2(i,j)=dP4par_po_vsem(i)*dP4par_po_vsem(j)+P4par*d2P4par_po_vsem2(i,j)+dP4perpx_po_vsem(i)*dP4perpx_po_vsem(j)
d2P40_po_vsem2(i,j)=d2P40_po_vsem2(i,j)+P4perp_x(px,Panti_x,n)*d2P4perp_x_po_vsem2(i,j)+dP4perpy_po_vsem(i)*dP4perpy_po_vsem(j)
d2P40_po_vsem2(i,j)=d2P40_po_vsem2(i,j)+P4perp_y(py,Panti_y,n)*d2P4perp_y_po_vsem2(i,j)-dP40_po_vsem(i)*dP40_po_vsem(j)
d2P40_po_vsem2(i,j)=d2P40_po_vsem2(i,j)/P40
!print*,"d2P40_po_vsem2",d2P40_po_vsem2(i,j),"j",j,"i",i
!write(101020123,*)"d2P40_po_vsem2",d2P40_po_vsem2(i,j),"j",j,"i",i
enddo
enddo



do j=1,3*n+2
do i=1,3*n+2
d2P30_po_vsem2(i,j)=dP3par_po_vsem(i)*dP3par_po_vsem(j)+P3par*d2P3par_po_vsem2(i,j)+dP3perpx_po_vsem(i)*dP3perpx_po_vsem(j)
d2P30_po_vsem2(i,j)=d2P30_po_vsem2(i,j)+P3perp_x(px,Panti_x,n)*d2P3perp_x_po_vsem2(i,j)+dP3perpy_po_vsem(i)*dP3perpy_po_vsem(j)
d2P30_po_vsem2(i,j)=d2P30_po_vsem2(i,j)+P3perp_y(py,Panti_y,n)*d2P3perp_y_po_vsem2(i,j)-dP30_po_vsem(i)*dP30_po_vsem(j)
d2P30_po_vsem2(i,j)=d2P30_po_vsem2(i,j)/P30
!print*,"d2P30_po_vsem2",d2P30_po_vsem2(i,j),"j",j,"i",i
!write(101020124,*)"d2P30_po_vsem2",d2P30_po_vsem2(i,j),"j",j,"i",i
enddo
enddo

dP3parvs=dP3par_po_vsem
dP4parvs=dP4par_po_vsem
d2P3parvs=d2P3par_po_vsem2
d2P4parvs=d2P4par_po_vsem2
dP40=dP40_po_vsem
dP30=dP30_po_vsem
d2P30=d2P30_po_vsem2
d2P40=d2P40_po_vsem2




dP3(0,1:3*n+2)=dP30(1:3*n+2)
dP3(1,1:3*n+2)=dP3perpx_po_vsem(1:3*n+2)
dP3(2,1:3*n+2)=dP3perpy_po_vsem(1:3*n+2)
dP3(3,1:3*n+2)=dP3parvs(1:3*n+2)
!do i=0,3
!do j=1,3*n+2
!print*,"dP3",dP3(i,j),"j",j,"i",i
!enddo
!enddo


dP4(0,1:3*n+2)=dP40(1:3*n+2)
dP4(1,1:3*n+2)=dP4perpx_po_vsem(1:3*n+2)
dP4(2,1:3*n+2)=dP4perpy_po_vsem(1:3*n+2)
dP4(3,1:3*n+2)=dP4parvs(1:3*n+2)
!do i=0,3
!do j=1,3*n+2
!print*,"dP4",dP4(i,j),"j",j,"i",i
!enddo
!enddo

contains


function mperp(MPI,x1,y1)

real(8)::MPI,mperp,x1,y1

mperp=sqrt(MPI*MPI+x1*x1+y1*y1)

end function mperp

function P3perp_x(px,Panti_x,n)

real(8):: px(1:n),Panti_x,P3perp_x,summa
integer :: n,i

summa=0.0

do i=1,n
summa=summa+px(i)
enddo

P3perp_x=-0.5*summa+Panti_x

end function P3perp_x

function P3perp_y(py,Panti_y,n)

real(8):: py(1:n),Panti_y,P3perp_y,summa1
integer :: n,i

summa1=0.0

do i=1,n
summa1=summa1+py(i)
enddo

P3perp_y=-0.5*summa1+Panti_y

end function P3perp_y

function P4perp_x(px,Panti_x,n)

real(8):: px(1:n),Panti_x,P4perp_x,summa
integer :: n,i

summa=0.0

do i=1,n
summa=summa+px(i)
enddo

P4perp_x=-0.5*summa-Panti_x

end function P4perp_x

function P4perp_y(py,Panti_y,n)

real(8):: py(1:n),Panti_y,P4perp_y,summa1
integer :: n,i

summa1=0.0

do i=1,n
summa1=summa1+py(i)
enddo

P4perp_y=-0.5*summa1-Panti_y

end function P4perp_y

subroutine E_pi(MPI,n,px,py,y,A)
implicit none

real(8)::MPI,px(1:n),py(1:n),y(1:n),A
integer :: n,i

A=0.0
do i=1,n
A=A+mperp(MPI,px(i),py(i))*cosh(y(i))
enddo

end subroutine E_pi

subroutine Ppar_pi(MPI,n,px,py,y,B)
implicit none

real(8):: MPI,px(1:n),py(1:n),y(1:n),B
integer :: n,i

B=0.0
do i=1,n
B=B+mperp(MPI,px(i),py(i))*sinh(y(i))
enddo

end subroutine Ppar_pi

end subroutine sub_d2momentums
