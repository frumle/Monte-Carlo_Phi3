subroutine sub_d34momentums(n,sqrt_s,y,P3,dP3,d2P3,d3P30,d3P3_par,d4P30,d4P3_par)
 
 use constants_module
 
 implicit none
 
 ! input
 integer:: n
 real(8):: sqrt_s
 real(8):: y(1:n)
 
 ! output
 real(8):: P3(0:1),dP3(0:1,1:n),d2P3(0:1,1:n,1:n) 
 real(8):: d3P30(1:n,1:n,1:n),d4P30(1:n,1:n,1:n,1:n)
 real(8):: d3P3_par(1:n,1:n,1:n),d4P3_par(1:n,1:n,1:n,1:n)

 ! temp 
 integer:: a,b,c,d,i
 real(8):: MPI2,MPI3,MPI4,E_,K,znam,temp,temp1,temp2,temp3
 
 ! Here P3(0:1)  , P3(0) = P3,0; P3(1) = P3_par ; P3y,P3y = 0 !
 real(8):: dK(1:n),d2K(1:n,1:n),d3K(1:n,1:n,1:n),d4K(1:n,1:n,1:n,1:n)
 real(8):: Pv,Ek,EP
 real(8):: P4(0:1)
 real(8):: dEk(1:n),dEP(1:n)
 real(8):: d2Ek(1:n,1:n),d2EP(1:n,1:n)
 real(8):: d3Ek(1:n,1:n,1:n),d3EP(1:n,1:n,1:n)
 real(8):: d4Ek(1:n,1:n,1:n,1:n),d4EP(1:n,1:n,1:n,1:n)

 ! 4speed
 real(8):: sinhy(1:n),coshy(1:n)
 real(8):: R_EP2,R_EP3,R_EP4
 real(8):: sqrt_znam
 real(8):: r_znam,r_sqrt_znam,r_znam15,r_znam25,r_znam35

 MPI2 = MPI*MPI
 MPI3 = MPI2*MPI
 MPI4 = MPI3*MPI


 do i = 1,n
  sinhy(i) = sinh(y(i))
  coshy(i) = cosh(y(i))
 enddo

 E_ = sqrt_s 
 do i = 1,n
 E_ = E_ - MPI*coshy(i)
 enddo
 
 Pv = 0.0
 do i = 1,n
  Pv = Pv + MPI*sinhy(i)
 enddo
   
   
  Ek = E_*E_
  EP = Ek - Pv*Pv 
  
  R_EP2 = 1.0/(EP*EP)
  R_EP3 = 1.0/(EP*EP*EP)
  R_EP4 = 1.0/(EP*EP*EP*EP)
   
!########################################################

  do a = 1,n
   dEk(a) = -MPI*sinhy(a)
  enddo
   dEk = dEk*E_*2.0 

  do a = 1,n
   do b = 1,n
     temp = MPI2*sinhy(a)*sinhy(b)
     if(a.eq.b) temp = temp - E_*MPI*coshy(a)
     d2Ek(a,b) = temp
   enddo
  enddo
  d2Ek = d2Ek*2.0
  
  do a = 1,n
   do b = a,n
    do c = b,n
      temp = 0.0
      if(b.eq.c) temp = temp + MPI2*sinhy(a)*coshy(b)
      if(a.eq.c) temp = temp + MPI2*sinhy(b)*coshy(a)
      if(a.eq.b) temp = temp + MPI2*sinhy(c)*coshy(a)
      if((a.eq.b).and.(a.eq.c)) temp = temp - E_*MPI*sinhy(a)
     
      temp = temp*2.0
     
      d3Ek(b,c,a) = temp
      d3Ek(b,a,c) = temp
      d3Ek(c,b,a) = temp
      d3Ek(c,a,b) = temp
      d3Ek(a,b,c) = temp
      d3Ek(a,c,b) = temp
      
      
    enddo
   enddo
  enddo

  
  
  do a = 1,n
   do b = a,n
    do c = b,n
     do d = c,n
      temp1 = 0.0
      if((b.eq.c).and.(b.eq.d)) temp1 = temp1 + MPI2*sinhy(a)*sinhy(b)
      if((a.eq.c).and.(a.eq.d)) temp1 = temp1 + MPI2*sinhy(b)*sinhy(a)
      if((a.eq.b).and.(a.eq.d)) temp1 = temp1 + MPI2*sinhy(c)*sinhy(a)
      if((a.eq.b).and.(a.eq.c)) temp1 = temp1 + MPI2*sinhy(d)*sinhy(a)

      
      temp2 = 0.0
      if((a.eq.b).and.(c.eq.d)) temp2 = temp2 + MPI2*coshy(a)*coshy(c)
      if((a.eq.c).and.(b.eq.d)) temp2 = temp2 + MPI2*coshy(a)*coshy(b)
      if((a.eq.d).and.(b.eq.c)) temp2 = temp2 + MPI2*coshy(a)*coshy(b)

      
      temp = temp1 + temp2
      if((a.eq.b).and.(a.eq.c).and.(a.eq.d)) temp = temp - E_*MPI*coshy(a)
      temp = temp*2.0
      
     d4Ek(a,b,c,d) = temp         
     d4Ek(a,b,d,c) = temp
     d4Ek(a,c,b,d) = temp
     d4Ek(a,c,d,b) = temp
     d4Ek(a,d,b,c) = temp
     d4Ek(a,d,c,b) = temp
     d4Ek(b,a,c,d) = temp
     d4Ek(b,a,d,c) = temp
     d4Ek(b,c,a,d) = temp
     d4Ek(b,c,d,a) = temp
     d4Ek(b,d,a,c) = temp
     d4Ek(b,d,c,a) = temp
     d4Ek(c,a,b,d) = temp
     d4Ek(c,a,d,b) = temp
     d4Ek(c,b,a,d) = temp
     d4Ek(c,b,d,a) = temp
     d4Ek(c,d,a,b) = temp
     d4Ek(c,d,b,a) = temp
     d4Ek(d,a,b,c) = temp
     d4Ek(d,a,c,b) = temp
     d4Ek(d,b,a,c) = temp
     d4Ek(d,b,c,a) = temp
     d4Ek(d,c,a,b) = temp
     d4Ek(d,c,b,a) = temp
      
      
      
     enddo
    enddo
   enddo
  enddo
  

!########################################################
   
  do a = 1,n
   dEP(a) = MPI*coshy(a)
  enddo
   dEP = dEk - dEP*Pv*2.0 
   


  do a = 1,n
   do b = 1,n
     temp = MPI2*coshy(a)*coshy(b)
     if(a.eq.b) temp = temp + Pv*MPI*sinhy(a)
     d2EP(a,b) = temp
   enddo
  enddo
  d2EP = d2Ek - d2EP*2.0
   
  do a = 1,n
   do b = a,n
    do c = b,n
      temp = 0.0
      if(b.eq.c) temp = temp + MPI2*coshy(a)*sinhy(b)
      if(a.eq.c) temp = temp + MPI2*coshy(b)*sinhy(a)
      if(a.eq.b) temp = temp + MPI2*coshy(c)*sinhy(a)
      if((a.eq.b).and.(a.eq.c)) temp = temp + Pv*MPI*coshy(a)
    
    d3EP(b,c,a) = temp
    d3EP(b,a,c) = temp
    d3EP(c,b,a) = temp
    d3EP(c,a,b) = temp
    d3EP(a,b,c) = temp
    d3EP(a,c,b) = temp
      
    enddo
   enddo
  enddo
  d3EP = d3Ek - d3EP*2.0
  
  
  
  
  do a = 1,n
   do b = a,n
    do c = b,n
     do d = c,n
      temp1 = 0.0
      if((b.eq.c).and.(b.eq.d)) temp1 = temp1 + MPI2*coshy(a)*coshy(b)
      if((a.eq.c).and.(a.eq.d)) temp1 = temp1 + MPI2*coshy(b)*coshy(a)
      if((a.eq.b).and.(a.eq.d)) temp1 = temp1 + MPI2*coshy(c)*coshy(a)
      if((a.eq.b).and.(a.eq.c)) temp1 = temp1 + MPI2*coshy(d)*coshy(a)

      
      temp2 = 0.0
      if((a.eq.b).and.(c.eq.d)) temp2 = temp2 + MPI2*sinhy(a)*sinhy(c)
      if((a.eq.c).and.(b.eq.d)) temp2 = temp2 + MPI2*sinhy(a)*sinhy(b)
      if((a.eq.d).and.(b.eq.c)) temp2 = temp2 + MPI2*sinhy(a)*sinhy(b)

      
      temp = temp1 + temp2
      if((a.eq.b).and.(a.eq.c).and.(a.eq.d)) temp = temp + Pv*MPI*sinhy(a)

     d4EP(a,b,c,d) = temp         
     d4EP(a,b,d,c) = temp
     d4EP(a,c,b,d) = temp
     d4EP(a,c,d,b) = temp
     d4EP(a,d,b,c) = temp
     d4EP(a,d,c,b) = temp
     d4EP(b,a,c,d) = temp
     d4EP(b,a,d,c) = temp
     d4EP(b,c,a,d) = temp
     d4EP(b,c,d,a) = temp
     d4EP(b,d,a,c) = temp
     d4EP(b,d,c,a) = temp
     d4EP(c,a,b,d) = temp
     d4EP(c,a,d,b) = temp
     d4EP(c,b,a,d) = temp
     d4EP(c,b,d,a) = temp
     d4EP(c,d,a,b) = temp
     d4EP(c,d,b,a) = temp
     d4EP(d,a,b,c) = temp
     d4EP(d,a,c,b) = temp
     d4EP(d,b,a,c) = temp
     d4EP(d,b,c,a) = temp
     d4EP(d,c,a,b) = temp
     d4EP(d,c,b,a) = temp
      
      
     enddo
    enddo
   enddo
  enddo
  d4EP = d4Ek - d4EP*2.0

                !  K and derivatives calculations
   
!##########################################################   
   
  K = Ek/4.0 - SQR_M*Ek/EP
 
  do a = 1,n
   dK(a) = 0.25*dEk(a) - SQR_M*(dEk(a)/EP - Ek*dEP(a)/(EP*EP))
  enddo

  do a = 1,n
   do b = 1,n
    temp = d2Ek(a,b)/EP - dEk(a)*dEP(b)/(EP*EP) - dEk(b)*DEP(a)/(EP*EP)
    temp = temp - Ek*d2EP(a,b)/(EP*EP) + 2.0*Ek*dEP(a)*dEP(b)/(EP*EP*EP)
    d2K(a,b) = 0.25*d2Ek(a,b) - SQR_M*temp
   enddo
  enddo

  do a = 1,n
   do b = a,n
    do c = b,n
     temp1 = - dEP(a)*d2Ek(b,c) - dEP(b)*d2Ek(a,c) - dEP(c)*d2Ek(a,b) !/b/b
     temp2 = - dEk(a)*d2EP(b,c) - dEk(b)*d2EP(a,c) - dEk(c)*d2EP(a,b) !/b/b
     temp = (temp1 + temp2)*R_EP2
     
     temp1 = dEk(a)*dEP(b)*dEP(c) + dEk(b)*dEP(a)*dEP(c) + dEk(c)*dEP(a)*dEP(b)
     temp2 = dEP(a)*d2EP(b,c) + dEP(b)*d2EP(a,c) + dEP(c)*d2EP(a,b)
!~      temp = temp + (temp1 + Ek*temp2)*2.0/(EP*EP*EP)
     temp = temp + (temp1 + Ek*temp2)*2.0*R_EP3
     
     temp = temp + d3Ek(a,b,c)/EP - Ek*d3EP(a,b,c)/(EP*EP)
!~      temp = temp - 6.0*Ek*dEP(a)*dEP(b)*dEP(c)/(EP*EP*EP*EP)
     temp = temp - 6.0*Ek*dEP(a)*dEP(b)*dEP(c)*R_EP4
     
     temp1 = 0.25*d3Ek(a,b,c) - SQR_M*temp
     
     
    d3K(b,c,a) = temp1
    d3K(b,a,c) = temp1
    d3K(c,b,a) = temp1
    d3K(c,a,b) = temp1
    d3K(a,b,c) = temp1
    d3K(a,c,b) = temp1
     
     
    enddo
   enddo
  enddo


  do a = 1,n
   do b = a,n
    do c = b,n
     do d = c,n
       ! d2A d2B
       temp1 = -d2Ek(a,b)*d2EP(c,d)
       temp1 = temp1 - d2Ek(a,c)*d2EP(b,d)
       temp1 = temp1 - d2Ek(a,d)*d2EP(b,c)
       temp1 = temp1 - d2Ek(b,c)*d2EP(a,d)
       temp1 = temp1 - d2Ek(b,c)*d2EP(a,d)
       temp1 = temp1 - d2Ek(c,d)*d2EP(a,b)
!~        temp = temp1/(EP*EP)
       temp = temp1*R_EP2
       
       ! dB dB d2A
       temp1 = dEP(a)*dEP(b)*d2Ek(c,d)
       temp1 = temp1 + dEP(a)*dEP(c)*d2Ek(b,d)
       temp1 = temp1 + dEP(a)*dEP(d)*d2Ek(b,c)
       temp1 = temp1 + dEP(b)*dEP(c)*d2Ek(a,d)
       temp1 = temp1 + dEP(b)*dEP(d)*d2Ek(a,c)
       temp1 = temp1 + dEP(c)*dEP(d)*d2Ek(a,b)
!~        temp = temp + 2.0*temp1/(EP*EP*EP)
       temp = temp + 2.0*temp1*R_EP3
       
       ! dA d3B
       
       temp1 = -dEk(a)*d3EP(b,c,d)
       temp1 = temp1 - dEk(b)*d3EP(a,c,d)
       temp1 = temp1 - dEk(c)*d3EP(a,b,d)
       temp1 = temp1 - dEk(d)*d3EP(a,b,c)
!~        temp = temp +  temp1/(EP*EP)
       temp = temp +  temp1*R_EP2
       
       ! dA dB d2B      
       temp1 = dEk(a)*dEP(b)*d2EP(c,d)
       temp1 = temp1 + dEk(a)*dEP(c)*d2EP(b,d)
       temp1 = temp1 + dEk(a)*dEP(d)*d2EP(b,c)
       temp1 = temp1 + dEk(b)*dEP(a)*d2EP(c,d)
       temp1 = temp1 + dEk(b)*dEP(c)*d2EP(a,d)
       temp1 = temp1 + dEk(b)*dEP(d)*d2EP(a,c)
       temp1 = temp1 + dEk(c)*dEP(a)*d2EP(b,d)
       temp1 = temp1 + dEk(c)*dEP(b)*d2EP(a,d)
       temp1 = temp1 + dEk(c)*dEP(d)*d2EP(a,b)
       temp1 = temp1 + dEk(d)*dEP(a)*d2EP(b,c)
       temp1 = temp1 + dEk(d)*dEP(b)*d2EP(a,c)
       temp1 = temp1 + dEk(d)*dEP(c)*d2EP(a,b)
!~        temp = temp + 2.0*temp1/(EP*EP*EP)
       temp = temp + 2.0*temp1*R_EP3
       
       !  dB d3A      
       temp1 = -dEP(a)*d3Ek(b,c,d)
       temp1 = temp1 - dEP(b)*d3Ek(a,c,d)
       temp1 = temp1 - dEP(c)*d3Ek(a,b,d)
       temp1 = temp1 - dEP(d)*d3Ek(a,b,c)
!~        temp = temp + temp1/(EP*EP)
       temp = temp + temp1*R_EP2
       
       !  dA dB dB dB       
       temp1 = -dEk(a)*dEP(b)*dEP(c)*dEP(d)
       temp1 = temp1 - dEk(b)*dEP(a)*dEP(c)*dEP(d)
       temp1 = temp1 - dEk(c)*dEP(a)*dEP(b)*dEP(d)
       temp1 = temp1 - dEk(d)*dEP(a)*dEP(b)*dEP(c)
!~        temp = temp + 6.0*temp1/(EP*EP*EP*EP)
       temp = temp + 6.0*temp1*R_EP4
       
       !  d2B d2B       
       temp1 = d2EP(a,b)*d2EP(c,d)
       temp1 = temp1 + d2EP(a,c)*d2EP(b,d)
       temp1 = temp1 + d2EP(a,d)*d2EP(b,c)
!~        temp = temp + 2.0*Ek*temp1/(EP*EP*EP)
       temp = temp + 2.0*Ek*temp1*R_EP3
       
       !  dB dB d2B       
       temp1 = -dEP(a)*dEP(b)*d2EP(c,d)
       temp1 = temp1 - dEP(a)*dEP(c)*d2EP(b,d)
       temp1 = temp1 - dEP(a)*dEP(d)*d2EP(b,c)
       temp1 = temp1 - dEP(b)*dEP(c)*d2EP(a,d)
       temp1 = temp1 - dEP(b)*dEP(d)*d2EP(a,c)
       temp1 = temp1 - dEP(c)*dEP(d)*d2EP(a,b)
!~        temp = temp + 6.0*Ek*temp1/(EP*EP*EP*EP)
       temp = temp + 6.0*Ek*temp1*R_EP4
       
       !  dB d3B    
       temp1 = dEP(a)*d3EP(b,c,d)
       temp1 = temp1 + dEP(b)*d3EP(a,c,d)
       temp1 = temp1 + dEP(c)*d3EP(a,b,d)
       temp1 = temp1 + dEP(d)*d3EP(a,b,c)
!~        temp = temp + 2.0*Ek*temp1/(EP*EP*EP)
       temp = temp + 2.0*Ek*temp1*R_EP3
       
       !   ????      
       !~  temp = temp - Ek*d4EP(a,b,c,d)/(EP*EP)
       !~  temp = temp + 24.0*Ek*dEP(a)*dEP(b)*dEP(c)*dEP(d)/(EP*EP*EP*EP*EP)
       !~  temp = temp + d4Ek(a,b,c,d)/(EP)
       temp = temp - Ek*d4EP(a,b,c,d)*R_EP2
       temp = temp + 24.0*Ek*dEP(a)*dEP(b)*dEP(c)*dEP(d)*R_EP4/EP
       temp = temp + d4Ek(a,b,c,d)/(EP)
       
      temp = 0.25*d4Ek(a,b,c,d) - SQR_M*temp
      
      
     d4K(a,b,c,d) = temp         
     d4K(a,b,d,c) = temp
     d4K(a,c,b,d) = temp
     d4K(a,c,d,b) = temp
     d4K(a,d,b,c) = temp
     d4K(a,d,c,b) = temp
     d4K(b,a,c,d) = temp
     d4K(b,a,d,c) = temp
     d4K(b,c,a,d) = temp
     d4K(b,c,d,a) = temp
     d4K(b,d,a,c) = temp
     d4K(b,d,c,a) = temp
     d4K(c,a,b,d) = temp
     d4K(c,a,d,b) = temp
     d4K(c,b,a,d) = temp
     d4K(c,b,d,a) = temp
     d4K(c,d,a,b) = temp
     d4K(c,d,b,a) = temp
     d4K(d,a,b,c) = temp
     d4K(d,a,c,b) = temp
     d4K(d,b,a,c) = temp
     d4K(d,b,c,a) = temp
     d4K(d,c,a,b) = temp
     d4K(d,c,b,a) = temp
      
      
      
      
     enddo
    enddo
   enddo
  enddo

  P3(1) = -0.5*Pv + sqrt(K)
  P4(1) = - P3(1) - Pv
  
  P3(0) = sqrt(SQR_M + P3(1)*P3(1))
  P4(0) = sqrt(SQR_M + P4(1)*P4(1))

 do a = 1,n
  dP3(1,a) = -0.5*MPI*coshy(a)+ 0.5*(K**(-0.5))*dK(a)
 enddo

 
 do b = 1,n
  do a = 1,n
   temp = 0.0
    if(a.eq.b) temp = temp - 0.5*MPI*sinhy(a)
   d2P3(1,b,a) = temp - 0.25*(K**(-1.5))*dK(b)*dK(a)+0.5*(K**(-0.5))*d2K(b,a)  
  enddo
 enddo


 do c = 1,n
  do b = c,n
   do a = b,n
   temp = 0.0
    if((a.eq.b).and.(a.eq.c)) temp = temp - 0.5*MPI*coshy(a)
    temp1 = temp
    temp = (3.0/8.0)*(K**(-2.5))*dK(c)*dK(b)*dK(a)
    temp = temp - 0.25*(K**(-1.5))*(d2K(c,b)*dK(a) + dK(b)*d2K(c,a) + dK(c)*d2K(b,a))
    temp1 =  temp1 + temp   + 0.5*(K**(-0.5))*d3K(c,b,a)
    
    d3P3_par(b,c,a) = temp1
    d3P3_par(b,a,c) = temp1
    d3P3_par(c,b,a) = temp1
    d3P3_par(c,a,b) = temp1
    d3P3_par(a,b,c) = temp1
    d3P3_par(a,c,b) = temp1
    
    
   enddo 
  enddo
 enddo


 do d = 1,n
  do c = d,n
   do b = c,n
    do a = b,n
    temp = 0.0
     if((a.eq.b).and.(a.eq.c).and.(a.eq.d)) temp = temp - 0.5*MPI*sinhy(a)
     temp = temp - (15.0/16.0)*(K**(-3.5))*dK(d)*dK(c)*dK(b)*dK(a)
      

  temp1 = dK(a)*dK(b)*d2K(c,d)+ dK(a)*dK(c)*d2K(b,d)+ dK(b)*dK(c)*d2K(a,d)+ dK(a)*dK(d)*d2K(b,c)+ dK(b)*dK(d)*d2K(a,c)
  temp1 = temp1 + dK(c)*dK(d)*d2K(a,b)
  temp1 = temp1*(3.0/8.0)*(K**(-2.5))
    
  temp2 = d2K(c,b)*d2K(d,a) + d2K(d,b)*d2K(c,a) + d2K(d,c)*d2K(b,a) 
  temp2 = -temp2*0.25*(K**(-1.5))


  temp3 = dK(a)*d3K(d,c,b)+ dK(b)*d3K(d,c,a)+ dK(c)*d3K(d,b,a) + dK(d)*d3K(c,b,a)
  temp3 = -temp3*0.25*(K**(-1.5))

  temp1 =  temp +  temp1 + temp2 + temp3 + 0.5*(K**(-0.5))*d4K(d,c,b,a)

     d4P3_par(a,b,c,d) = temp1
     d4P3_par(a,b,d,c) = temp1
     d4P3_par(a,c,b,d) = temp1
     d4P3_par(a,c,d,b) = temp1
     d4P3_par(a,d,b,c) = temp1
     d4P3_par(a,d,c,b) = temp1
     d4P3_par(b,a,c,d) = temp1
     d4P3_par(b,a,d,c) = temp1
     d4P3_par(b,c,a,d) = temp1
     d4P3_par(b,c,d,a) = temp1
     d4P3_par(b,d,a,c) = temp1
     d4P3_par(b,d,c,a) = temp1
     d4P3_par(c,a,b,d) = temp1
     d4P3_par(c,a,d,b) = temp1
     d4P3_par(c,b,a,d) = temp1
     d4P3_par(c,b,d,a) = temp1
     d4P3_par(c,d,a,b) = temp1
     d4P3_par(c,d,b,a) = temp1
     d4P3_par(d,a,b,c) = temp1
     d4P3_par(d,a,c,b) = temp1
     d4P3_par(d,b,a,c) = temp1
     d4P3_par(d,b,c,a) = temp1
     d4P3_par(d,c,a,b) = temp1
     d4P3_par(d,c,b,a) = temp1

     enddo 
   enddo
  enddo
 enddo

 call sub_symmetrization(n,d3P3_par,d4P3_par)


 znam = SQR_M + P3(1)*P3(1)
 
 
 ! 4speed
 r_znam = 1.0/znam
 
 sqrt_znam = sqrt(znam)
 r_sqrt_znam = 1.0/sqrt_znam
 
 r_znam15 = 1.0/(znam**1.5)
 r_znam25 = 1.0/(znam**2.5)
 r_znam35 = 1.0/(znam**3.5)

 
 
 
 
!~  P3(0) = sqrt(SQR_M + P3(1)*P3(1))
 P3(0) = sqrt(znam)

 do d = 1,n
  dP3(0,d)=dP3(1,d)*P3(1)*r_sqrt_znam
 enddo

 do c = 1,n
  do d = 1,n
!~   temp = dP3(1,c)*dP3(1,d)*(znam**(-0.5))
!~   temp = temp - P3(1)*P3(1)*dP3(1,c)*dP3(1,d)*(znam**(-1.5))
!~   d2P3(0,c,d) = temp + P3(1)*d2P3(1,c,d)*(znam**(-0.5))
    temp = dP3(1,c)*dP3(1,d)*r_sqrt_znam
    temp = temp - P3(1)*P3(1)*dP3(1,c)*dP3(1,d)*r_znam15
    d2P3(0,c,d) = temp + P3(1)*d2P3(1,c,d)*r_sqrt_znam
  enddo
 enddo

 do b = 1,n
  do c = b,n
   do d = c,n
!~     temp1 = 3.0*P3(1)*P3(1)*P3(1)*dP3(1,b)*dP3(1,c)*dP3(1,d)*(znam**(-2.5))
    temp1 = 3.0*P3(1)*P3(1)*P3(1)*dP3(1,b)*dP3(1,c)*dP3(1,d)*r_znam25
    
    temp2 = - 2.0*dP3(1,b)*dP3(1,c)*dP3(1,d)-P3(1)*d2P3(1,b,c)*dP3(1,d)-P3(1)*d2P3(1,b,d)*dP3(1,c)
    temp2 = temp2 - dP3(1,b)*dP3(1,c)*dP3(1,d) -  P3(1)*d2P3(1,c,d)*dP3(1,b)
!~     temp2 = P3(1)*temp2*(znam**(-1.5))
    temp2 = P3(1)*temp2*r_znam15
    
    temp3 = d2P3(1,b,c)*dP3(1,d)+d2P3(1,b,d)*dP3(1,c)+d2P3(1,c,d)*dP3(1,b)+P3(1)*d3P3_par(b,c,d)
!~     temp3 = temp3*(znam**(-0.5))
    temp3 = temp3*r_sqrt_znam
    
    temp1 = temp1 + temp2 + temp3 

    d3P30(b,c,d) = temp1
    d3P30(b,d,c) = temp1
    d3P30(c,b,d) = temp1
    d3P30(c,d,b) = temp1
    d3P30(d,b,c) = temp1
    d3P30(d,c,b) = temp1

   enddo
  enddo
 enddo


 do a = 1,n
  do b = a,n
   do c = b,n
    do d = c,n
     
     temp1 = 6.0*dP3(1,a)*dP3(1,b)*dP3(1,c)*dP3(1,d)/P3(1)
     temp1 = temp1 + dP3(1,a)*dP3(1,b)*d2P3(1,c,d)
     temp1 = temp1 + dP3(1,c)*dP3(1,d)*d2P3(1,a,b)
     temp1 = temp1 + dP3(1,b)*dP3(1,d)*d2P3(1,a,c)
     temp1 = temp1 + dP3(1,b)*dP3(1,c)*d2P3(1,a,d)
     temp1 = temp1 + dP3(1,a)*dP3(1,d)*d2P3(1,b,c)
     temp1 = temp1 + dP3(1,a)*dP3(1,c)*d2P3(1,b,d)
!~      temp1 = temp1*3.0*P3(1)*P3(1)*P3(1)/(znam**2.5)
     temp1 = temp1*3.0*P3(1)*P3(1)*P3(1)*r_znam25
     
     temp2 = -3.0*dP3(1,a)*dP3(1,b)*dP3(1,c)*dP3(1,d)/P3(1)
     temp2 = temp2 -3.0*dP3(1,c)*dP3(1,d)*d2P3(1,a,b)
     temp2 = temp2 -3.0*dP3(1,b)*dP3(1,d)*d2P3(1,a,c)
     temp2 = temp2 -3.0*dP3(1,b)*dP3(1,c)*d2P3(1,a,d)
     temp2 = temp2 -3.0*dP3(1,a)*dP3(1,d)*d2P3(1,b,c)
     temp2 = temp2 -3.0*dP3(1,a)*dP3(1,c)*d2P3(1,b,d)
     temp2 = temp2 -3.0*dP3(1,a)*dP3(1,b)*d2P3(1,c,d)
     
     temp2 = temp2 - P3(1)*d2P3(1,a,b)*d2P3(1,c,d)
     temp2 = temp2 - P3(1)*d2P3(1,a,c)*d2P3(1,b,d)
     temp2 = temp2 - P3(1)*d2P3(1,a,d)*d2P3(1,b,c)
     temp2 = temp2 - P3(1)*dP3(1,b)*d3P3_par(a,c,d)
     temp2 = temp2 - P3(1)*dP3(1,d)*d3P3_par(a,b,c)
     temp2 = temp2 - P3(1)*dP3(1,c)*d3P3_par(a,b,d)
     temp2 = temp2 - P3(1)*dP3(1,a)*d3P3_par(b,c,d)
!~      temp2 = temp2*P3(1)/(znam**1.5)
     temp2 = temp2*P3(1)*r_znam15
     
     temp3 = d2P3(1,a,b)*d2P3(1,c,d)
     temp3 = temp3 + d2P3(1,a,d)*d2P3(1,b,c)
     temp3 = temp3 + d2P3(1,a,c)*d2P3(1,b,d)
     temp3 = temp3 + dP3(1,a)*d3P3_par(b,c,d)
     temp3 = temp3 + dP3(1,b)*d3P3_par(a,c,d)
     temp3 = temp3 + dP3(1,c)*d3P3_par(a,b,d)
     temp3 = temp3 + dP3(1,d)*d3P3_par(a,b,c)
     temp3 = temp3 + P3(1)*d4P3_par(a,b,c,d)
!~      temp3 = temp3/sqrt(znam)
     temp3 = temp3*r_sqrt_znam
     
     temp = (P3(1)**4.0)*dP3(1,a)*dP3(1,b)*dP3(1,c)*dP3(1,d)
!~      temp = temp*15.0/(znam**3.5)
     temp = temp*15.0*r_znam35
     
     temp1 = temp1 + temp2 + temp3 - temp
     d4P30(a,b,c,d) = temp1
     d4P30(a,b,d,c) = temp1
     d4P30(a,c,b,d) = temp1
     d4P30(a,c,d,b) = temp1
     d4P30(a,d,b,c) = temp1
     d4P30(a,d,c,b) = temp1
     d4P30(b,a,c,d) = temp1
     d4P30(b,a,d,c) = temp1
     d4P30(b,c,a,d) = temp1
     d4P30(b,c,d,a) = temp1
     d4P30(b,d,a,c) = temp1
     d4P30(b,d,c,a) = temp1
     d4P30(c,a,b,d) = temp1
     d4P30(c,a,d,b) = temp1
     d4P30(c,b,a,d) = temp1
     d4P30(c,b,d,a) = temp1
     d4P30(c,d,a,b) = temp1
     d4P30(c,d,b,a) = temp1
     d4P30(d,a,b,c) = temp1
     d4P30(d,a,c,b) = temp1
     d4P30(d,b,a,c) = temp1
     d4P30(d,b,c,a) = temp1
     d4P30(d,c,a,b) = temp1
     d4P30(d,c,b,a) = temp1

     
    enddo
   enddo
  enddo
 enddo

  call sub_symmetrization(n,d3P30,d4P30)

contains

integer function delta(a,b)
integer:: a,b
  delta = 0
  if (a.EQ.b) delta = 1
end function delta

end subroutine sub_d34momentums
