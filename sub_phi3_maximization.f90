subroutine phi3_maximization(n,sqrt_s,m_step,m_precision,X0,Xmax)
   
   implicit none
   
   !~ Set "PRINT_TIME_ELAPSED = .TRUE." to print the time elapsed for maximization.
   logical, parameter:: PRINT_TIME_ELAPSED = .TRUE.
   
   ! Enable/Disable verification of the maximization 
   logical, parameter:: VERIFY_MAXIMUM = .TRUE.
   real(8), parameter:: VERIFICATION_PRECISION = 1e-4
   
   ! input
   integer:: n
   real(8):: sqrt_s
   real(8):: m_step,m_precision ! maximization step and precision
   real(8):: X0(1:3*n+2)  ! maximization start point
   
   ! output
   real(8):: Xmax(1:3*n+2)! maximum point
   
   ! temp
   real(8):: squared_precision
   integer:: i,array_size
   real(8):: exit_counter

   


   ! time counters
   integer:: time_scale,exec_start_time,exec_end_time 
   
   ! model
   real(8):: P3(0:3),P4(0:3)
   real(8):: dP3(0:3,1:3*n+2),dP4(0:3,1:3*n+2)
   real(8):: dlnA(1:3*n+2)
   
   ! check maximum
   real(8):: lnA,max_lnA
   real(8):: Xcheck(1:3*n+2),check_m_step
   



interface

subroutine sub_momentums(n,sqrt_s,X,P3,P4)
 use constants_module
 implicit none

 ! input
 integer :: n
 real(8):: sqrt_s
 real(8):: X(1:3*n+2)
 
 ! output
 real(8):: P3(0:3),P4(0:3)
 
 
 ! temp
 real(8):: px(1:n),py(1:n),y(1:n),PaX,PaY
 real(8):: Panti_x,Panti_y,l,k,t,A1,B1,D,P3perp_kvad,P4perp_kvad
 real(8):: P3par,P4par,P30,P40,controlEnergy
 
end subroutine sub_momentums

subroutine sub_dmomentums(n,sqrt_s,X,P3,P4,dP3,dP4)
 
 use constants_module
 
 implicit none

  ! input
  integer:: n
  real(8):: sqrt_s
  real(8):: X(1:3*n+2)
 
  ! output
  real(8):: P3(0:3), P4(0:3)
  real(8):: dP3(0:3,1:3*n+2),dP4(0:3,1:3*n+2)
 
  ! temp
  integer :: i
  real(8):: px(1:n),py(1:n),y(1:n),PaX,PaY
  real(8):: Panti_x,Panti_y,l,k,t,A1,B1,D,P3perp_kvad,P4perp_kvad 
  real(8):: P3par,P4par,P30,P40,controlEnergy,podkor
  real(8):: dP3par_po_dt,dP3par_po_dP3x,dP3par_po_dP3y
  real(8):: dP3par_po_dPpar_pi,dP3par_po_dk,dE_pi_po_dyj(1:n)
  real(8):: dE_pi_po_dpxj(1:n),dE_pi_po_dpyj(1:n)
  real(8):: dPpar_pi_po_dyj(1:n),dPpar_pi_po_dpxj(1:n),dPpar_pi_po_dpyj(1:n)
  real(8):: dt_po_dyj(1:n),dt_po_dpxj(1:n),dt_po_dpyj(1:n)
  real(8):: dP3perpx_po_dpxj(1:n),dP3perpx_po_dPanti_x,dP3perpy_po_dPanti_y,dP3perpy_po_dpyj(1:n)
  real(8):: dP4perpx_po_dpxj(1:n),dP4perpx_po_dPanti_x,dP4perpy_po_dpyj(1:n),dP4perpy_po_dPanti_y
  real(8):: dk_po_dpxj(1:n),dk_po_dpyj(1:n),dk_po_dPanti_x,dk_po_dPanti_y
  real(8):: dl_po_dyj(1:n),dl_po_dpxj(1:n),dl_po_dpyj(1:n)
  real(8):: dP3par_po_dl
  real(8):: dPpar_pi_po_vsem(1:3*n+2),dl_po_vsem(1:3*n+2),dk_po_vsem(1:3*n+2),dt_po_vsem(1:3*n+2)
  real(8):: dP3perpx_po_vsem(1:3*n+2),dP3perpy_po_vsem(1:3*n+2),dP3par_po_vsem(1:3*n+2)
  real(8):: dP4par_po_vsem(1:3*n+2),dP4perpx_po_vsem(1:3*n+2),dP4perpy_po_vsem(1:3*n+2)
  real(8):: dP40_po_vsem(1:3*n+2),dP30_po_vsem(1:3*n+2)
  real(8):: dP3parvs(1:3*n+2),dP4parvs(1:3*n+2)
  real(8):: dP30(1:3*n+2),dP40(1:3*n+2)
  
end subroutine sub_dmomentums

subroutine sub_lnA(n,sqrt_s,X,P3,lnA)
 
 use constants_module
 
 implicit none
 
 
 ! input
 integer:: n
 real(8):: sqrt_s
 real(8):: X(1:3*n+2)
 
 ! output
 real(8):: lnA

 ! temp
 integer:: b,c
 real(8):: P3(0:3)
 real(8):: px(1:n),py(1:n),y(1:n),PaX,PaY,P1(0:3)
 real(8):: slag1,slag2,slag3,slag4
 real(8):: p0(1:n),p_par(1:n)
 real(8):: temp
 
end subroutine sub_lnA 
 
subroutine sub_dlnA (n,sqrt_s,X,P3,dP3,dlnA)
  
  use constants_module
  
  implicit none 
 
  ! input
  integer:: n
  real(8):: sqrt_s
  real(8):: X(1:3*n+2)
  real(8):: P3(0:3),dP3(0:3,1:3*n+2)

  ! output
  real(8):: dlnA(1:3*n+2)
 
  ! temp
  integer:: b,c,nomper
  real(8):: px(1:n),py(1:n),y(1:n),PaX,PaY,P1(0:3)
  real(8):: p0(1:n),p_par(1:n),dp0(1:n,1:3*n+2),dpx(1:n,1:3*n+2),dpy(1:n,1:3*n+2),dp_par(1:n,1:3*n+2)
  real(8):: slag1,slag2,slag3,slag4,znamenatel
  real(8):: slaga1,slaga2,slaga3,slaga4
  real(8):: mnozh1,mnozh2,mnozh3,mnozh4,mnozh5,mnozh6,mnozh7,mnozh8
  
end subroutine sub_dlnA

end interface
   
 if(PRINT_TIME_ELAPSED)then
     !set milliseconds timescale
     time_scale = 1000
     !get begining time
     call system_clock(exec_start_time, time_scale)
 endif
   
array_size = 3*n+2   
squared_precision = m_precision**2.0
Xmax = X0
  
   
find_max: do
     
   call sub_dmomentums(n,sqrt_s,Xmax,P3,P4,dP3,dP4)
   call sub_dlnA (n,sqrt_s,Xmax,P3,dP3,dlnA)
   
   exit_counter = 0.0
   do i = 1, array_size
      exit_counter = exit_counter + dlnA(i)*dlnA(i)
   enddo
   
      print*, exit_counter


   if(exit_counter.lt.squared_precision) then
     exit find_max
   endif
   
   Xmax = Xmax + m_step*dlnA  
   
enddo find_max
   
   if(VERIFY_MAXIMUM) then
     check_m_step = m_step*1.2

        call sub_momentums(n,sqrt_s,Xmax,P3,P4)
        call sub_lnA(n,sqrt_s,Xmax,P3,max_lnA)

     
     Xcheck = Xmax
     do i = 1, array_size
        Xcheck(i) = Xmax(i) + check_m_step*dlnA(i)
           call sub_momentums(n,sqrt_s,Xcheck,P3,P4)
           call sub_lnA(n,sqrt_s,Xcheck,P3,lnA)
        if(abs(lnA - max_lnA).gt.VERIFICATION_PRECISION)then
          print*, "Bad maximization result for variable: ", i
        endif
        
        Xcheck(i) = Xmax(i) - check_m_step*dlnA(i)
           call sub_momentums(n,sqrt_s,Xcheck,P3,P4)
           call sub_lnA(n,sqrt_s,Xcheck,P3,lnA)
        if(abs(lnA - max_lnA).gt.VERIFICATION_PRECISION)then
          print*, "Bad maximization result for variable: ", i
        endif
        
        Xcheck(i) = Xmax(i)
     enddo
     
     Xcheck = Xmax + check_m_step*dlnA
     call sub_momentums(n,sqrt_s,Xcheck,P3,P4)
     call sub_lnA(n,sqrt_s,Xcheck,P3,lnA)
     if(abs(lnA - max_lnA).gt.VERIFICATION_PRECISION)then
        print*, "Bad maximization result for variable. (current|max)", lnA,max_lnA
     endif
     
     Xcheck = Xmax - check_m_step*dlnA
     call sub_momentums(n,sqrt_s,Xcheck,P3,P4)
     call sub_lnA(n,sqrt_s,Xcheck,P3,lnA)
     if(abs(lnA - max_lnA).gt.VERIFICATION_PRECISION)then
        print*, "Bad maximization result for variable. (current|max)", lnA,max_lnA
     endif
     
     if(VERIFY_MAXIMUM)then
       print*, "Maximum verified."
     endif 
    
   endif
     
   if(PRINT_TIME_ELAPSED)then
     call system_clock(exec_end_time)
     print*, "Maximizing completed. Time elapsed (ms): ", (exec_end_time - exec_start_time)
   endif
      

end subroutine phi3_maximization
