subroutine sub_symmetrization2D(n,M2)

 implicit none

 ! input
  integer:: n
  real(8):: M2(1:n,1:n)

 ! temp
 integer:: a,b
 real(8):: temp
 
 
do a = 1,n
 do b = a,n
    temp = (M2(a,b)+M2(b,a))/2.0
    M2(a,b) = temp
    M2(b,a) = temp
 enddo
enddo


end subroutine sub_symmetrization2D


