subroutine sub_dpsmall(n,X,p0,p_par,dp0,dpx,dpy,dp_par)
 
 use constants_module
 
 implicit none

 ! input
 integer:: n
 real(8):: X(1:3*n+2)
 
 ! output
 real(8):: p0(1:n),p_par(1:n),dp0(1:n,1:3*n+2),dpx(1:n,1:3*n+2),dpy(1:n,1:3*n+2),dp_par(1:n,1:3*n+2)
 
 ! temp
 integer:: nomper,c
 real(8):: px(1:n),py(1:n),y(1:n)
 real(8):: mperp(1:n)
 real(8):: coshy(1:n),sinhy(1:n)
 


y=X(1:n)
px=X(n+1:2*n)
py=X(2*n+1:3*n)


p0=0
p_par=0
dp0=0.0
dpx=0.0
dpy=0.0
dp_par=0.0



do c=1,n
 mperp(c) = sqrt(SQR_MPI+(px(c)**2.0)+(py(c)**2.0))
 coshy(c) = cosh(y(c))
 sinhy(c) = sinh(y(c))
 p0(c) = mperp(c)*coshy(c)
 p_par(c) = mperp(c)*sinhy(c)
enddo



do c = 1,n
 do nomper=1,3*n+2 
!~    dp0(c,nomper) = (px(c)*delta(c+n,nomper) + py(c)*delta(c+2*n,nomper))*cosh(y(c))
!~    dp0(c,nomper) = dp0(c,nomper)/sqrt(SQR_MPI+(px(c)**2.0)+(py(c)**2.0))
!~    dp0(c,nomper) = dp0(c,nomper)+sqrt(SQR_MPI+(px(c)**2.0)+(py(c)**2.0))*sinh(y(c))*delta(c,nomper)
   dp0(c,nomper) = (px(c)*delta(c+n,nomper) + py(c)*delta(c+2*n,nomper))*coshy(c)
   dp0(c,nomper) = dp0(c,nomper)/mperp(c)
   dp0(c,nomper) = dp0(c,nomper) + p_par(c)*delta(c,nomper)
 enddo
enddo

do c = 1,n
 do nomper=1,3*n+2
  dpx(c,nomper)=delta(c+n,nomper)
 enddo
enddo

do c = 1,n
 do nomper=1,3*n+2
  dpy(c,nomper)=delta(c+2*n,nomper)
 enddo
enddo

do c = 1,n
 do nomper=1,3*n+2
!~   dp_par(c,nomper)=(px(c)*delta(c+n,nomper)+py(c)*delta(c+2*n,nomper))*sinh(y(c))
!~   dp_par(c,nomper)=dp_par(c,nomper)/sqrt(SQR_MPI+(px(c)**2.0)+(py(c)**2.0))
!~   dp_par(c,nomper)=dp_par(c,nomper)+sqrt(SQR_MPI+(px(c)**2.0)+(py(c)**2.0))*cosh(y(c))*delta(c,nomper)
   dp_par(c,nomper) = (px(c)*delta(c+n,nomper) + py(c)*delta(c+2*n,nomper))*sinhy(c)
   dp_par(c,nomper) = dp_par(c,nomper)/mperp(c)
   dp_par(c,nomper) = dp_par(c,nomper) + p0(c)*delta(c,nomper)
 enddo
enddo


contains

integer function delta(a,b)
integer:: a,b
  if (a.EQ.b) then
   delta = 1
  else
   delta = 0
  endif
end function delta

end subroutine sub_dpsmall
