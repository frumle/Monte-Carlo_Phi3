subroutine sub_determinant(n1,D,detD)

implicit none

integer::n1,nol,znak,raz1,raz2
real(8)::D(1:n1,1:n1),detD,mnojitel
real(8),allocatable::D1(:,:),D2(:,:),D3(:,:)

detD=1.0 
raz1=n1 
raz2=raz1-1
allocate (D1(1:raz1,1:raz1))
D1=D

ras_det: do

if (raz1.eq.2) then
detD=(D1(1,1)*D1(2,2)-D1(2,1)*D1(1,2))*detD 
exit ras_det
endif

allocate (D2(1:raz2,1:raz2),D3(1:raz1,1:raz1))

call proverca_nula(raz1,D1,D3,nol,znak)

if (nol.eq.0) then 
detD=0.0
exit ras_det
endif

call ponijenie_razmernosti(raz1,D3,raz2,D2,mnojitel)

!print*,"znak",znak,"mnojitel",mnojitel
detD=detD*znak*mnojitel
deallocate(D1)
allocate (D1(1:raz2,1:raz2))
D1=D2
!print*,D2(1,1)
!print*,D2(1,2)
!print*,D2(2,1)
!print*,D2(2,2)
raz1=raz2
raz2=raz1-1
deallocate (D2,D3)

enddo ras_det


contains

subroutine proverca_nula(raz,D,D1,nol,znak)
implicit none
integer::raz,znak,a,nol
real(8)::D(1:raz,1:raz),D1(1:raz,1:raz),stroca(1:raz)

D1=D

perebor: do a=1,raz

if(D(a,1).ne.0.0) then

if (a.eq.1) then
nol=1
znak=1
exit perebor
else
stroca(1:raz)=D(1,1:raz)
D1(1,1:raz)=D(a,1:raz)
D1(a,1:raz)=stroca(1:raz)
znak=-1
nol=1
exit perebor
endif


endif

enddo perebor


if (a.eq.raz+1)  nol=0

end subroutine proverca_nula

subroutine ponijenie_razmernosti(raz1,D1,raz2,D2,mnojitel)
implicit none
integer:: raz1,raz2,a,b
real(8):: D1(1:raz1,1:raz1),D2(1:raz2,1:raz2),mnojitel,stroca(1:raz1)

mnojitel=D1(1,1)
do a=1,raz1
stroca(a)=D1(1,a)/D1(1,1)
enddo

D1(1,1:raz1)=stroca(1:raz1)

do a=2,raz1
do b=2,raz1
D2(a-1,b-1)=D1(a,b)-D1(a,1)*D1(1,b)
enddo
enddo

end subroutine ponijenie_razmernosti


end subroutine sub_determinant
