subroutine sub_momentums(n,sqrt_s,X,P3,P4)
 use constants_module
 implicit none

 ! input
 integer :: n
 real(8):: sqrt_s
 real(8):: X(1:3*n+2)
 
 ! output
 real(8):: P3(0:3),P4(0:3)
 
 
 ! temp
 real(8):: px(1:n),py(1:n),y(1:n),PaX,PaY
 real(8):: Panti_x,Panti_y,l,k,t,A1,B1,D,P3perp_kvad,P4perp_kvad
 real(8):: P3par,P4par,P30,P40,controlEnergy

  y(1:n) = X(1:n)
  px(1:n) = X(n+1:2*n)
  py(1:n) = X(2*n+1:3*n)
  PaX = X(3*n+1)
  PaY = X(3*n+2)


Panti_x=PaX
Panti_y=PaY
call E_pi(n,SQR_MPI,px,py,y,A1)
call Ppar_pi(n,SQR_MPI,px,py,y,B1)


t=(sqrt_s-A1)*(sqrt_s-A1)
l=B1*B1-t
P3perp_kvad=P3perp_x(px,Panti_x,n)*P3perp_x(px,Panti_x,n)+P3perp_y(py,Panti_y,n)*P3perp_y(py,Panti_y,n)
P4perp_kvad=P4perp_x(px,Panti_x,n)*P4perp_x(px,Panti_x,n)+P4perp_y(py,Panti_y,n)*P4perp_y(py,Panti_y,n)
k=P4perp_kvad-P3perp_kvad


D=sqrt(t)*sqrt(4.0*(SQR_M+P3perp_kvad)*l+(l+k)*(l+k))
P3par=(B1*(l+k)+D)/(-2.0*l)
P4par=-P3par-B1
P30=sqrt(sqr_M+P3par*P3par+P3perp_kvad)
P40=sqrt(sqr_M+P4par*P4par+P4perp_kvad)
controlEnergy=sqrt_s-(P30+P40+A1)

P3(0)=P30
P3(1)=P3perp_x(px,Panti_x,n)
P3(2)=P3perp_y(py,Panti_y,n)
P3(3)=P3par

P4(0)=P40
P4(1)=P4perp_x(px,Panti_x,n)
P4(2)=P4perp_y(py,Panti_y,n)
P4(3)=P4par

contains


function mperp(mass_sqr,x1,y1)

real(8):: mperp,x1,y1
real(8):: mass_sqr

mperp=sqrt(mass_sqr + x1*x1+y1*y1)

end function mperp

function P3perp_x(px,Panti_x,n)

real(8):: px(1:n),Panti_x,P3perp_x,summa
integer :: n,i

summa=0.0

do i=1,n
summa=summa+px(i)
enddo

P3perp_x=-0.5*summa+Panti_x

end function P3perp_x

function P3perp_y(py,Panti_y,n)

real(8):: py(1:n),Panti_y,P3perp_y,summa1
integer :: n,i

summa1=0.0

do i=1,n
summa1=summa1+py(i)
enddo

P3perp_y=-0.5*summa1+Panti_y

end function P3perp_y

function P4perp_x(px,Panti_x,n)

real(8):: px(1:n),Panti_x,P4perp_x,summa
integer :: n,i

summa=0.0

do i=1,n
summa=summa+px(i)
enddo

P4perp_x=-0.5*summa-Panti_x

end function P4perp_x

function P4perp_y(py,Panti_y,n)

real(8):: py(1:n),Panti_y,P4perp_y,summa1
integer :: n,i

summa1=0.0

do i=1,n
summa1=summa1+py(i)
enddo

P4perp_y=-0.5*summa1-Panti_y

end function P4perp_y

subroutine E_pi(n,sqr_mpi,px,py,y,A)
implicit none

real(8):: px(1:n),py(1:n),y(1:n),A
real(8):: sqr_mpi
integer :: n,i

A=0.0
do i=1,n
A=A+mperp(sqr_mpi,px(i),py(i))*cosh(y(i))
enddo

end subroutine E_pi

subroutine Ppar_pi(n,sqr_mpi,px,py,y,B)
implicit none

real(8):: px(1:n),py(1:n),y(1:n),B
real(8):: sqr_mpi
integer :: n,i

B=0.0
do i=1,n
B=B+mperp(sqr_mpi,px(i),py(i))*sinh(y(i))
enddo

end subroutine Ppar_pi

end subroutine sub_momentums
