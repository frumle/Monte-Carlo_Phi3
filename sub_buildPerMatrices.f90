subroutine sub_buildPerMatrices(n_groups,npa,matricesCount,matrices)

 ! input
 integer:: n_groups,npa(1:n_groups)
 
 ! output 
integer:: matricesCount
 integer, allocatable:: matrices(:,:,:)

 ! temp

integer:: temp_matrices(1:999999,1:n_groups,1:n_groups)
integer:: maxli(1:n_groups,1:n_groups),a,b,l11,l12,l21,l22
integer:: nomer

do a=1,n_groups
do b=1,n_groups
maxli(a,b)=min(npa(a),npa(b))
enddo
enddo

nomer=0
 do l11=0,maxli(1,1)
 do l12=0,maxli(1,2)
 do l21=0,maxli(2,1)
 do l22=0,maxli(1,1)

l13=npa(1)-l11-l12
l23=npa(2)-l21-l22
l31=npa(1)-l11-l21
l32=npa(2)-l12-l22
l33=npa(3)-l13-l23

if (l13 .ge. 0 .and. l23 .ge. 0 .and. l31 .ge. 0 .and. l32 .ge. 0 .and. l33 .ge. 0) then
nomer=nomer+1
temp_matrices(nomer,1,1)=l11
temp_matrices(nomer,1,2)=l12
temp_matrices(nomer,1,3)=l13
temp_matrices(nomer,2,1)=l21
temp_matrices(nomer,2,2)=l22
temp_matrices(nomer,2,3)=l23
temp_matrices(nomer,3,1)=l31
temp_matrices(nomer,3,2)=l32
temp_matrices(nomer,3,3)=l33
endif


enddo 
enddo 
enddo 
enddo 

matricesCount=nomer

if(allocated(matrices)) deallocate(matrices)

allocate(matrices(1:matricesCount,1:n_groups,1:n_groups))
matrices(1:matricesCount,1:n_groups,1:n_groups) = temp_matrices(1:matricesCount,1:n_groups,1:n_groups)



end subroutine sub_buildPerMatrices 
