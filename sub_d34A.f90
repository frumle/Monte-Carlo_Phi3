subroutine sub_d34A(n,sqrt_s,X,A_max,d3A,d4A)
 
 implicit none
 
 ! input
 integer:: n 
 real(8):: sqrt_s
 reaL(8):: X(1:3*n+2)
 real(8):: A_max     ! Scattering amplitude value in the maximum point.

 ! output 
 real(8):: d3A(1:n,1:n,1:n), d4A(1:n,1:n,1:n,1:n)
 
 ! temp
 integer:: a, b, c, d
 real(8):: y(1:n),d2lnA(1:3*n+2,1:3*n+2)
 real(8):: dlnA(1:3*n+2)
 real(8):: d3lnA(1:n,1:n,1:n), d4lnA(1:n,1:n,1:n,1:n)
 real(8):: temp,temp1
 real(8):: P3(0:3),P4(0:3)
 real(8):: dP3(0:3,1:3*n+2),dP4(0:4,1:3*n+2)

interface

subroutine sub_dmomentums(n,sqrt_s,X,P3,P4,dP3,dP4)
 
 use constants_module
 
 implicit none

  ! input
  integer:: n
  real(8):: sqrt_s
  real(8):: X(1:3*n+2)
 
  ! output
  real(8):: P3(0:3), P4(0:3)
  real(8):: dP3(0:3,1:3*n+2),dP4(0:3,1:3*n+2)
 
  ! temp
  integer :: i
  real(8):: px(1:n),py(1:n),y(1:n),PaX,PaY
  real(8):: Panti_x,Panti_y,l,k,t,A1,B1,D,P3perp_kvad,P4perp_kvad 
  real(8):: P3par,P4par,P30,P40,controlEnergy,podkor
  real(8):: dP3par_po_dt,dP3par_po_dP3x,dP3par_po_dP3y
  real(8):: dP3par_po_dPpar_pi,dP3par_po_dk,dE_pi_po_dyj(1:n)
  real(8):: dE_pi_po_dpxj(1:n),dE_pi_po_dpyj(1:n)
  real(8):: dPpar_pi_po_dyj(1:n),dPpar_pi_po_dpxj(1:n),dPpar_pi_po_dpyj(1:n)
  real(8):: dt_po_dyj(1:n),dt_po_dpxj(1:n),dt_po_dpyj(1:n)
  real(8):: dP3perpx_po_dpxj(1:n),dP3perpx_po_dPanti_x,dP3perpy_po_dPanti_y,dP3perpy_po_dpyj(1:n)
  real(8):: dP4perpx_po_dpxj(1:n),dP4perpx_po_dPanti_x,dP4perpy_po_dpyj(1:n),dP4perpy_po_dPanti_y
  real(8):: dk_po_dpxj(1:n),dk_po_dpyj(1:n),dk_po_dPanti_x,dk_po_dPanti_y
  real(8):: dl_po_dyj(1:n),dl_po_dpxj(1:n),dl_po_dpyj(1:n)
  real(8):: dP3par_po_dl
  real(8):: dPpar_pi_po_vsem(1:3*n+2),dl_po_vsem(1:3*n+2),dk_po_vsem(1:3*n+2),dt_po_vsem(1:3*n+2)
  real(8):: dP3perpx_po_vsem(1:3*n+2),dP3perpy_po_vsem(1:3*n+2),dP3par_po_vsem(1:3*n+2)
  real(8):: dP4par_po_vsem(1:3*n+2),dP4perpx_po_vsem(1:3*n+2),dP4perpy_po_vsem(1:3*n+2)
  real(8):: dP40_po_vsem(1:3*n+2),dP30_po_vsem(1:3*n+2)
  real(8):: dP3parvs(1:3*n+2),dP4parvs(1:3*n+2)
  real(8):: dP30(1:3*n+2),dP40(1:3*n+2)
  
end subroutine sub_dmomentums

subroutine sub_dlnA (n,sqrt_s,X,P3,dP3,dlnA)
  
  use constants_module
  
  implicit none 
 
  ! input
  integer:: n
  real(8):: sqrt_s
  real(8):: X(1:3*n+2)
  real(8):: P3(0:3),dP3(0:3,1:3*n+2)

  ! output
  real(8):: dlnA(1:3*n+2)
 
  ! temp
  integer:: b,c,nomper
  real(8):: px(1:n),py(1:n),y(1:n),PaX,PaY,P1(0:3)
  real(8):: p0(1:n),p_par(1:n),dp0(1:n,1:3*n+2),dpx(1:n,1:3*n+2),dpy(1:n,1:3*n+2),dp_par(1:n,1:3*n+2)
  real(8):: slag1,slag2,slag3,slag4,znamenatel
  real(8):: slaga1,slaga2,slaga3,slaga4
  real(8):: mnozh1,mnozh2,mnozh3,mnozh4,mnozh5,mnozh6,mnozh7,mnozh8
  
end subroutine sub_dlnA

subroutine sub_d2lnA (n,sqrt_s,X,d2lnA)

   use constants_module

   implicit none 
 
   ! input
   integer:: n
   real(8):: sqrt_s
   real(8):: X(1:3*n+2)
   
   ! output
   real(8):: d2lnA(1:3*n+2,1:3*n+2)
   
   ! temp
   integer::b,c,nomper1,nomper2
 
   real(8):: px(1:n),py(1:n),y(1:n),PaX,PaY,P1(0:3)
   real(8):: P3(0:3),P4(0:3),dP3(0:3,1:3*n+2),dP4(0:3,1:3*n+2)
   real(8):: d2P30(1:3*n+2,1:3*n+2),d2P40(1:3*n+2,1:3*n+2)
   real(8):: d2P3parvs(1:3*n+2,1:3*n+2),d2P4parvs(1:3*n+2,1:3*n+2)
 
   real(8):: p0(1:n),p_par(1:n),dp0(1:n,1:3*n+2),dpx(1:n,1:3*n+2),dpy(1:n,1:3*n+2),dp_par(1:n,1:3*n+2)
   real(8):: d2p0(1:n,1:3*n+2,1:3*n+2),d2p_par(1:n,1:3*n+2,1:3*n+2)
 
   real(8):: slag1,slag2,slag3,slag4,znamenatel
   real(8):: slaga1,slaga2,slaga3,slaga4
   real(8):: mnozh1,mnozh2,mnozh3,mnozh4,mnozh5,mnozh6,mnozh7,mnozh8
   real(8):: dmnozh1,dmnozh2,dmnozh3,dmnozh5,dmnozh7,dmnozh8
   real(8):: dslag1,dslag2,dslag3,dslag4,dznamenatel
   real(8):: dslaga1,dslaga2,dslaga3,dslaga4
   real(8):: Aa,dAa
   
end subroutine sub_d2lnA

subroutine sub_d34lnA(n,sqrt_s,y,d3lnA,d4lnA)

 use constants_module

 implicit none

 ! input
 integer:: n
 real(8):: sqrt_s
 real(8):: y(1:n)

 ! output
 real(8):: d3lnA(1:n,1:n,1:n),d4lnA(1:n,1:n,1:n,1:n)
  
 ! temp
 integer:: a,b,c,d,i
 real(8):: P1(0:3),S
 real(8):: P3(0:1),dP3(0:1,1:n),d2P3(0:1,1:n,1:n),d3P30(1:n,1:n,1:n)
 real(8):: d3P3_par(1:n,1:n,1:n),d4P30(1:n,1:n,1:n,1:n), d4P3_par(1:n,1:n,1:n,1:n)
 real(8):: sc,dsa,dsb,dsc,dsd,temp1
 real(8):: d4s
 real(8):: slag1,slag2 
 real(8):: mnozh1,mnozh2,mnozh3
 real(8):: coshy(1:n),sinhy(1:n)
 
end subroutine sub_d34lnA

end interface
 
 y(1:n) = X(1:n)
 
 call sub_dmomentums(n,sqrt_s,X,P3,P4,dP3,dP4)
 call sub_dlnA (n,sqrt_s,X,P3,dP3,dlnA)
 call sub_d2lnA (n,sqrt_s,X,d2lnA)
 call sub_d34lnA(n,sqrt_s,y,d3lnA,d4lnA)

 d3A = 0.0
 d4A = 0.0
 

!do b = 1,n
! do c = 1,n
!  do d = 1,n
!   d3A(b,c,d) = d3lnA(b,c,d)
!   do a = 1,n
!    d4A(a,b,c,d) = d4lnA(a,b,c,d)+d2lnA(a,b)*d2lnA(c,d)+d2lnA(a,d)*d2lnA(b,c)+d2lnA(a,c)*d2lnA(b,d)
!   enddo
!  enddo
! enddo
!enddo

! d3A = d3A*A_max
! d4A = d4A*A_max


do b = 1,n
 do c = b,n
  do d = c,n
  temp1 = dlnA(c)*d2lnA(b,d) + dlnA(b)*d2lnA(c,d) + dlnA(d)*d2lnA(b,c) + d3lnA(b,c,d)
  temp1 = temp1 + dlnA(b)*dlnA(c)*dlnA(d)
   
    d3A(b,c,d) = temp1
    d3A(b,d,c) = temp1
    d3A(c,b,d) = temp1
    d3A(c,d,b) = temp1
    d3A(d,b,c) = temp1
    d3A(d,c,b) = temp1
   
   do a = d,n
    temp = dlnA(d)*dlnA(c)*d2lnA(a,b) + d2lnA(c,d)*d2lnA(a,b) + dlnA(c)*d3lnA(a,b,d)
    temp = temp + dlnA(d)*d3lnA(a,b,c) + d4lnA(a,b,c,d) + dlnA(a)*dlnA(b)*dlnA(c)*dlnA(d)
    temp = temp + d2lnA(c,d)*dlnA(a)*dlnA(b) + dlnA(c)*d2lnA(a,d)*dlnA(b)+ dlnA(c)*dlnA(a)*d2lnA(b,d)
    temp = temp + dlnA(d)*d2lnA(a,c)*dlnA(b) + d3lnA(a,c,d)*dlnA(b) + d2lnA(a,c)*d2lnA(b,d)
    temp = temp + dlnA(a)*dlnA(d)*d2lnA(b,c) + d2lnA(a,d)*d2lnA(b,c) + dlnA(a)*d3lnA(b,c,d)
    
    d4A(a,b,c,d) = temp         
    d4A(a,b,d,c) = temp
    d4A(a,c,b,d) = temp
    d4A(a,c,d,b) = temp
    d4A(a,d,b,c) = temp
    d4A(a,d,c,b) = temp
    d4A(b,a,c,d) = temp
    d4A(b,a,d,c) = temp
    d4A(b,c,a,d) = temp
    d4A(b,c,d,a) = temp
    d4A(b,d,a,c) = temp
    d4A(b,d,c,a) = temp
    d4A(c,a,b,d) = temp
    d4A(c,a,d,b) = temp
    d4A(c,b,a,d) = temp
    d4A(c,b,d,a) = temp
    d4A(c,d,a,b) = temp
    d4A(c,d,b,a) = temp
    d4A(d,a,b,c) = temp
    d4A(d,a,c,b) = temp
    d4A(d,b,a,c) = temp
    d4A(d,b,c,a) = temp
    d4A(d,c,a,b) = temp
    d4A(d,c,b,a) = temp
    
   enddo
  enddo
 enddo
enddo

 d3A = d3A*A_max
 d4A = d4A*A_max


end subroutine sub_d34A
