subroutine sub_d2lnA (n,sqrt_s,X,d2lnA)

   use constants_module

   implicit none 
 
   ! input
   integer:: n
   real(8):: sqrt_s
   real(8):: X(1:3*n+2)
   
   ! output
   real(8):: d2lnA(1:3*n+2,1:3*n+2)
   
   ! temp
   integer::b,c,nomper1,nomper2
 
   real(8):: px(1:n),py(1:n),y(1:n),PaX,PaY,P1(0:3)
   real(8):: P3(0:3),P4(0:3),dP3(0:3,1:3*n+2),dP4(0:3,1:3*n+2)
   real(8):: d2P30(1:3*n+2,1:3*n+2),d2P40(1:3*n+2,1:3*n+2)
   real(8):: d2P3parvs(1:3*n+2,1:3*n+2),d2P4parvs(1:3*n+2,1:3*n+2)
 
   real(8):: p0(1:n),p_par(1:n),dp0(1:n,1:3*n+2),dpx(1:n,1:3*n+2),dpy(1:n,1:3*n+2),dp_par(1:n,1:3*n+2)
   real(8):: d2p0(1:n,1:3*n+2,1:3*n+2),d2p_par(1:n,1:3*n+2,1:3*n+2)
 
   real(8):: slag1,slag2,slag3,slag4,znamenatel
   real(8):: slaga1,slaga2,slaga3,slaga4
   real(8):: mnozh1,mnozh2,mnozh3,mnozh4,mnozh5,mnozh6,mnozh7,mnozh8
   real(8):: dmnozh1,dmnozh2,dmnozh3,dmnozh5,dmnozh7,dmnozh8
   real(8):: dslag1,dslag2,dslag3,dslag4,dznamenatel
   real(8):: dslaga1,dslaga2,dslaga3,dslaga4
   real(8):: Aa,dAa
 
 
interface

  subroutine sub_dpsmall(n,X,p0,p_par,dp0,dpx,dpy,dp_par)
 
    use constants_module
 
    implicit none

    ! input
    integer:: n
    real(8):: X(1:3*n+2)
 
    ! output
    real(8):: p0(1:n),p_par(1:n),dp0(1:n,1:3*n+2),dpx(1:n,1:3*n+2),dpy(1:n,1:3*n+2),dp_par(1:n,1:3*n+2)
 
    ! temp
    integer:: nomper,c
    real(8):: px(1:n),py(1:n),y(1:n)
    real(8):: mperp(1:n)
    real(8):: coshy(1:n),sinhy(1:n)
 
  end subroutine sub_dpsmall

  subroutine sub_d2psmall(n,X,d2p0,d2p_par)
    use constants_module
    implicit none
 
    ! input
    integer:: n
    real(8):: X(1:3*n+2)
   
    ! output
    real(8):: d2p0(1:n,1:3*n+2,1:3*n+2),d2p_par(1:n,1:3*n+2,1:3*n+2)

    ! temp
    real(8):: px(1:n),py(1:n),y(1:n)
    integer:: c,nomper1,nomper2
    real(8):: m_c,dm_c_nomper1,dm_c_nomper2,d2m_p1,d2m_p2,d2m 
    real(8):: coshy,sinhy
  end subroutine sub_d2psmall

  subroutine sub_d2momentums(n,sqrt_s,y,px,py,PaX,PaY,P3,P4,d2P3parvs,d2P4parvs,d2P30,d2P40,dP3,dP4)

  use constants_module
  implicit none
  
  ! input
  integer:: n
  real(8):: sqrt_s
  real(8):: px(1:n),py(1:n),y(1:n),PaX,PaY
  ! output
  real(8):: P3(0:3),P4(0:3)
  real(8):: d2P3parvs(1:3*n+2,1:3*n+2),d2P4parvs(1:3*n+2,1:3*n+2)
  real(8):: d2P30(1:3*n+2,1:3*n+2),d2P40(1:3*n+2,1:3*n+2)
  real(8):: dP3(0:3,1:3*n+2),dP4(0:3,1:3*n+2)
  
  ! temp
  integer :: i,j
  real(8):: Panti_x,Panti_y,l,k,t,A1,B1,D,P3perp_kvad,P4perp_kvad
  real(8):: P3par,P4par,P30,P40,controlEnergy,podkor
  real(8):: dP3par_po_dt,dP3par_po_dP3x,dP3par_po_dP3y
  real(8):: dP3par_po_dPpar_pi,dP3par_po_dk,dE_pi_po_dyj(1:n)
  real(8):: dE_pi_po_dpxj(1:n),dE_pi_po_dpyj(1:n)
  real(8):: dPpar_pi_po_dyj(1:n),dPpar_pi_po_dpxj(1:n),dPpar_pi_po_dpyj(1:n)
  real(8):: dt_po_dyj(1:n),dt_po_dpxj(1:n),dt_po_dpyj(1:n)
  real(8):: dP3perpx_po_dpxj(1:n),dP3perpx_po_dPanti_x,dP3perpy_po_dPanti_y,dP3perpy_po_dpyj(1:n)
  real(8):: dP4perpx_po_dpxj(1:n),dP4perpx_po_dPanti_x,dP4perpy_po_dpyj(1:n),dP4perpy_po_dPanti_y
  real(8):: dk_po_dpxj(1:n),dk_po_dpyj(1:n),dk_po_dPanti_x,dk_po_dPanti_y
  real(8):: dl_po_dyj(1:n),dl_po_dpxj(1:n),dl_po_dpyj(1:n)
  real(8):: d2P3par_po_dt2,d2P3par_po_dk2,d2P3par_po_dkdt
  real(8):: d2P3par_po_dP3perp_x2,d2P3par_po_dP3perp_y2,d2P3par_po_dP3perp_xdP3perp_y
  real(8):: d2P3par_po_dP3perp_xdt,d2P3par_po_dP3perp_ydt,d2P3par_po_dP3perp_xdk,d2P3par_po_dP3perp_ydk
  real(8):: d2P3par_po_dPpar_pi2,d2P3par_po_dPpar_pidt,d2P3par_po_dPpar_pidP3perp_x,d2P3par_po_dPpar_pidP3perp_y
  real(8):: d2P3par_po_dPpar_pidk,dP3par_po_dl,d2P3par_po_dl2,dpodkor_po_dl,d2P3par_po_dPpar_pidl
  real(8):: d2P3par_po_dldk,d2P3par_po_dldt,d2P3par_po_dlP3perp_x,d2P3par_po_dlP3perp_y
  real(8):: d2Ppar_pi_po_dyidyj(1:n,1:n),d2Ppar_pi_po_dpxidpxj(1:n,1:n),d2Ppar_pi_po_dpyidpyj(1:n,1:n)
  real(8):: d2Ppar_pi_po_dyidpxj(1:n,1:n),d2Ppar_pi_po_dyidpyj(1:n,1:n),d2Ppar_pi_po_dpxidpyj(1:n,1:n)
  real(8):: d2E_pi_po_dyidyj(1:n,1:n),d2E_pi_po_dpxidpxj(1:n,1:n),d2E_pi_po_dpyidpyj(1:n,1:n)
  real(8):: d2E_pi_po_dyidpxj(1:n,1:n),d2E_pi_po_dyidpyj(1:n,1:n),d2E_pi_po_dpxidpyj(1:n,1:n)
  real(8):: d2t_po_dyidyj(1:n,1:n),d2t_po_dpxidpxj(1:n,1:n),d2t_po_dpyidpyj(1:n,1:n),d2t_po_dyidpxj(1:n,1:n)
  real(8):: d2t_po_dyidpyj(1:n,1:n),d2t_po_dpxidpyj(1:n,1:n),d2k_po_vsem2(1:3*n+2,1:3*n+2)
  real(8):: d2l_po_dyidyj(1:n,1:n),d2l_po_dpxidpxj(1:n,1:n),d2l_po_dpyidpyj(1:n,1:n),d2l_po_dyidpxj(1:n,1:n)
  real(8):: d2l_po_dyidpyj(1:n,1:n),d2l_po_dpyidpxj(1:n,1:n)
  real(8):: dPpar_pi_po_vsem(1:3*n+2),dl_po_vsem(1:3*n+2),dk_po_vsem(1:3*n+2),dt_po_vsem(1:3*n+2)
  real(8):: dP3perpx_po_vsem(1:3*n+2),dP3perpy_po_vsem(1:3*n+2),dP3par_po_vsem(1:3*n+2)
  real(8):: d2Ppar_pi_po_vsem2(1:3*n+2,1:3*n+2),d2l_po_vsem2(1:3*n+2,1:3*n+2),d2t_po_vsem2(1:3*n+2,1:3*n+2)
  real(8):: d2P3perp_x_po_vsem2(1:3*n+2,1:3*n+2),d2P3perp_y_po_vsem2(1:3*n+2,1:3*n+2),d2P3par_po_vsem21(1:3*n+2,1:3*n+2)
  real(8):: d2P3par_po_vsem22(1:3*n+2,1:3*n+2),d2P3par_po_vsem23(1:3*n+2,1:3*n+2),d2P3par_po_vsem24(1:3*n+2,1:3*n+2)
  real(8):: d2P3par_po_vsem25(1:3*n+2,1:3*n+2),d2P3par_po_vsem26(1:3*n+2,1:3*n+2),d2P3par_po_vsem2(1:3*n+2,1:3*n+2)
  real(8):: dP4par_po_vsem(1:3*n+2),d2P4par_po_vsem2(1:3*n+2,1:3*n+2),dP4perpx_po_vsem(1:3*n+2),dP4perpy_po_vsem(1:3*n+2)
  real(8):: dP40_po_vsem(1:3*n+2),dP30_po_vsem(1:3*n+2),d2P30_po_vsem2(1:3*n+2,1:3*n+2),d2P4perp_x_po_vsem2(1:3*n+2,1:3*n+2)
  real(8):: d2P4perp_y_po_vsem2(1:3*n+2,1:3*n+2),d2P40_po_vsem2(1:3*n+2,1:3*n+2)
  real(8):: dP3parvs(1:3*n+2),dP4parvs(1:3*n+2)
  real(8):: dP30(1:3*n+2),dP40(1:3*n+2)
  end subroutine sub_d2momentums

end interface 
 
 
y=X(1:n)
px=X(n+1:2*n)
py=X(2*n+1:3*n)
PaX=X(3*n+1)
PaY=X(3*n+2)

P1(0)=sqrt_s/2
P1(1)=0.0
P1(2)=0.0
P1(3)=sqrt(((sqrt_s/2)**2.0)-SQR_M)


 d2lnA=0.0

 
 call sub_d2momentums(n,sqrt_s,y,px,py,PaX,PaY,P3,P4,d2P3parvs,d2P4parvs,d2P30,d2P40,dP3,dP4)
 call sub_dpsmall(n,X,p0,p_par,dp0,dpx,dpy,dp_par)
 call sub_d2psmall(n,X,d2p0,d2p_par)
 
do nomper1 = 1,3*n+2
do nomper2 = 1,3*n+2


 d2lnA(nomper1,nomper2)=0
 slag1=0
 slag2=0
 slag3=0
 slag4=0
 
 slaga1=0
 slaga2=0
 slaga3=0
 slaga4=0


do b=0,n

 slag1=P1(0)-P3(0)
  do c=1,b
  slag1=slag1-p0(c)
  enddo
 
 slag2=P1(1)-P3(1)
  do c=1,b
  slag2=slag2-px(c)
  enddo

 slag3=P1(2)-P3(2)
  do c=1,b
  slag3=slag3-py(c)
  enddo

 slag4=P1(3)-P3(3)
  do c=1,b
  slag4=slag4-p_par(c)
  enddo

 znamenatel = (SQR_MPI-(slag1**2.0)+(slag2**2.0)+(slag3**2.0)+(slag4**2.0))

 mnozh1=P1(0)-P3(0)
 do c = 1,b
 mnozh1=mnozh1-p0(c)
 enddo
 mnozh2=-dP3(0,nomper1)
 do c=1,b
 mnozh2=mnozh2-dp0(c,nomper1)
 enddo
 
  slaga1=-2.0*mnozh1*mnozh2

 mnozh3=P1(1)-P3(1)
 do c=1,b
 mnozh3=mnozh3-px(c)
 enddo
 mnozh4=-dP3(1,nomper1)
 do c=1,b
 mnozh4=mnozh4-dpx(c,nomper1)
 enddo

  slaga2=2.0*mnozh3*mnozh4
 
 mnozh5=P1(2)-P3(2)
 do c=1,b
 mnozh5=mnozh5-py(c)
 enddo
 mnozh6=-dP3(2,nomper1)
 do c=1,b
 mnozh6=mnozh6-dpy(c,nomper1)
 enddo
 
  slaga3=2.0*mnozh5*mnozh6
 
 mnozh7=P1(3)-P3(3)
 do c=1,b
 mnozh7=mnozh7-p_par(c)
 enddo
 mnozh8=-dP3(3,nomper1)
 do c=1,b
 mnozh8=mnozh8-dp_par(c,nomper1)
 enddo

  slaga4=2.0*mnozh7*mnozh8
!###################
 Aa=(slaga1+slaga2+slaga3+slaga4)

!#####################

 dmnozh1=-dP3(0,nomper2)
 do c = 1,b
 dmnozh1=dmnozh1-dp0(c,nomper2)
 enddo
 mnozh2=-dP3(0,nomper1)
 do c=1,b
 mnozh2=mnozh2-dp0(c,nomper1)
 enddo
 
 
 mnozh1=P1(0)-P3(0)
 do c = 1,b
 mnozh1=mnozh1-p0(c)
 enddo
 dmnozh2=-d2P30(nomper1,nomper2)
 do c=1,b
 dmnozh2=dmnozh2-d2p0(c,nomper1,nomper2)
 enddo

  dslaga1=-2.0*(dmnozh1*mnozh2+mnozh1*dmnozh2)

 dmnozh3=-dP3(1,nomper2)
 do c=1,b
 dmnozh3=dmnozh3-dpx(c,nomper2)
 enddo
 mnozh4=-dP3(1,nomper1)
 do c=1,b
 mnozh4=mnozh4-dpx(c,nomper1)
 enddo
 
 

  dslaga2=2.0*dmnozh3*mnozh4
 
 dmnozh5=-dP3(2,nomper2)
 do c=1,b
 dmnozh5=dmnozh5-dpy(c,nomper2)
 enddo
 mnozh6=-dP3(2,nomper1)
 do c=1,b
 mnozh6=mnozh6-dpy(c,nomper1)
 enddo

 
  dslaga3=2.0*dmnozh5*mnozh6
 
 dmnozh7=-dP3(3,nomper2)
 do c=1,b
 dmnozh7=dmnozh7-dp_par(c,nomper2)
 enddo
 mnozh8=-dP3(3,nomper1)
 do c=1,b
 mnozh8=mnozh8-dp_par(c,nomper1)
 enddo


 mnozh7=P1(3)-P3(3)
 do c=1,b
 mnozh7=mnozh7-p_par(c)
 enddo
 dmnozh8=-d2P3parvs(nomper1,nomper2)
 do c=1,b
 dmnozh8=dmnozh8-d2p_par(c,nomper1,nomper2)
 enddo



  dslaga4=2.0*(dmnozh7*mnozh8+mnozh7*dmnozh8)

 dAa=dslaga1+dslaga2+dslaga3+dslaga4
!#####################
 
  dslag1=-dP3(0,nomper2)
  do c=1,b
  dslag1=dslag1-dp0(c,nomper2)
  enddo
 
  dslag2=-dP3(1,nomper2)
  do c=1,b
  dslag2=dslag2-dpx(c,nomper2)
  enddo

  dslag3=-dP3(2,nomper2)
  do c=1,b
  dslag3=dslag3-dpy(c,nomper2)
  enddo

  dslag4=-dP3(3,nomper2)
  do c=1,b
  dslag4=dslag4-dp_par(c,nomper2)
  enddo

 dznamenatel = -2.0*slag1*dslag1+2.0*slag2*dslag2+2.0*slag3*dslag3+2.0*slag4*dslag4
 

d2lnA(nomper1,nomper2)=d2lnA(nomper1,nomper2)+((Aa*dznamenatel)/(znamenatel**2.0)-dAa/znamenatel)
enddo

enddo
enddo 
 
 
 
end subroutine sub_d2lnA
