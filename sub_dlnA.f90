subroutine sub_dlnA (n,sqrt_s,X,P3,dP3,dlnA)
  
  use constants_module
  
  implicit none 
 
  ! input
  integer:: n
  real(8):: sqrt_s
  real(8):: X(1:3*n+2)
  real(8):: P3(0:3),dP3(0:3,1:3*n+2)

  ! output
  real(8):: dlnA(1:3*n+2)
 
  ! temp
  integer:: b,c,nomper
  real(8):: px(1:n),py(1:n),y(1:n),PaX,PaY,P1(0:3)
  real(8):: p0(1:n),p_par(1:n),dp0(1:n,1:3*n+2),dpx(1:n,1:3*n+2),dpy(1:n,1:3*n+2),dp_par(1:n,1:3*n+2)
  real(8):: slag1,slag2,slag3,slag4,znamenatel
  real(8):: slaga1,slaga2,slaga3,slaga4
  real(8):: mnozh1,mnozh2,mnozh3,mnozh4,mnozh5,mnozh6,mnozh7,mnozh8
  
  interface
  
  subroutine sub_dpsmall(n,X,p0,p_par,dp0,dpx,dpy,dp_par)
 
    use constants_module
 
    implicit none

    ! input
    integer:: n
    real(8):: X(1:3*n+2)
 
    ! output
    real(8):: p0(1:n),p_par(1:n),dp0(1:n,1:3*n+2),dpx(1:n,1:3*n+2),dpy(1:n,1:3*n+2),dp_par(1:n,1:3*n+2)
 
    ! temp
    integer:: nomper,c
    real(8):: px(1:n),py(1:n),y(1:n)
    real(8):: mperp(1:n)
    
 end subroutine sub_dpsmall
  
  end interface
  
  y=X(1:n)
  px=X(n+1:2*n)
  py=X(2*n+1:3*n)
  PaX=X(3*n+1)
  PaY=X(3*n+2)


  P1(0)=sqrt_s/2
  P1(1)=0.0
  P1(2)=0.0
  P1(3)=sqrt(((sqrt_s/2)**2.0)-SQR_M)
  
  call sub_dpsmall(n,X,p0,p_par,dp0,dpx,dpy,dp_par)

  do nomper = 1,3*n+2
    dlnA(nomper)=0
    
    slag1=0
    slag2=0
    slag3=0
    slag4=0
 
    slaga1=0
    slaga2=0
    slaga3=0
    slaga4=0


    do b=0,n
      slag1=P1(0)-P3(0)
      do c=1,b
        slag1=slag1-p0(c)
      enddo
 
      slag2=P1(1)-P3(1)
      do c=1,b
        slag2=slag2-px(c)
      enddo

      slag3=P1(2)-P3(2)
      do c=1,b
        slag3=slag3-py(c)
      enddo
 
      slag4=P1(3)-P3(3)
      do c=1,b
        slag4=slag4-p_par(c)
      enddo
 
      znamenatel = (SQR_MPI-(slag1**2.0)+(slag2**2.0)+(slag3**2.0)+(slag4**2.0))
 
      mnozh1=P1(0)-P3(0)
      do c = 1,b
        mnozh1=mnozh1-p0(c)
      enddo
      mnozh2=-dP3(0,nomper)
      do c=1,b
        mnozh2=mnozh2-dp0(c,nomper)
      enddo
  
      slaga1=-2.0*mnozh1*mnozh2
       
      mnozh3=P1(1)-P3(1)
      do c=1,b
        mnozh3=mnozh3-px(c)
      enddo
      mnozh4=-dP3(1,nomper)
      do c=1,b
        mnozh4=mnozh4-dpx(c,nomper)
      enddo
 
      slaga2=2.0*mnozh3*mnozh4
  
     mnozh5=P1(2)-P3(2)
     do c=1,b
       mnozh5=mnozh5-py(c)
     enddo
     mnozh6=-dP3(2,nomper)
     do c=1,b
       mnozh6=mnozh6-dpy(c,nomper)
     enddo
  
     slaga3=2.0*mnozh5*mnozh6
  
     mnozh7 = P1(3)-P3(3)
     do c=1,b
       mnozh7=mnozh7-p_par(c)
     enddo
     mnozh8=-dP3(3,nomper)
     do c=1,b
       mnozh8=mnozh8-dp_par(c,nomper)
     enddo 

     slaga4=2.0*mnozh7*mnozh8 


    dlnA(nomper) = dlnA(nomper) - (slaga1+slaga2+slaga3+slaga4)/znamenatel
  enddo
enddo
 
end subroutine sub_dlnA
