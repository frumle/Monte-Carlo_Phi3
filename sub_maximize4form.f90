subroutine sub_maximize4form(n,Y0,Y0_form_approx,Per,D2,D3,D4,D2Form,pokazatel)
  implicit none
  
  ! input
  integer:: n, Per(1:n)
  real(8):: D2(1:n,1:n),D3(1:n,1:n,1:n),D4(1:n,1:n,1:n,1:n)
  real(8):: Y0(1:n), Y0_form_approx(1:n)
  
  ! output
  real(8):: D2Form(1:n,1:n), pokazatel
  
  ! temp
  
  integer:: i,j,k,l,t,d
  real(8):: Y0p(1:n)
  real(8):: D2per(1:n,1:n),D3per(1:n,1:n,1:n), D4per(1:n,1:n,1:n,1:n)
  real(8):: Tetta(1:n),sumtetta,precsn,max_step
  integer:: rk2,rk3
  real(8):: temp
  
do i = 1,n
 Y0p(Per(i)) = Y0(i)
 do j = 1,n
  D2per(Per(i),Per(j))  = D2(i,j)
  do k = 1,n
   D3per(Per(i),Per(j),Per(k))   = D3(i,j,k)
   do l = 1,n
    D4per(Per(i),Per(j),Per(k),Per(l)) =  D4(i,j,k,l) 
   enddo
  enddo
 enddo
enddo

 precsn = 2e-3
 max_step =  0.001
 
findmax: do
 Tetta = 0.0
 do t = 1,n
  do i = 1,n
   Tetta(t) = Tetta(t) + 2.0*D2(t,i)*(Y0_form_approx(i)-Y0(i))
   Tetta(t) = Tetta(t) + 2.0*D2per(t,i)*(Y0_form_approx(i)-Y0p(i))
   do j = i,n
   rk2 = 3*repeatCoef2(i,j)
    Tetta(t) = Tetta(t) + rk2*D3(t,i,j)*(Y0_form_approx(i)-Y0(i))*(Y0_form_approx(j)-Y0(j))
    Tetta(t) = Tetta(t) + rk2*D3per(t,i,j)*(Y0_form_approx(i)-Y0p(i))*(Y0_form_approx(j)-Y0p(j))
    do k = j,n
    rk3 = 4*repeatCoef3(i,j,k)
     Tetta(t) = Tetta(t) + rk3*D4(t,i,j,k)*(Y0_form_approx(i)-Y0(i))*(Y0_form_approx(j)-Y0(j))*(Y0_form_approx(k)-Y0(k))
     Tetta(t) = Tetta(t) + rk3*D4per(t,i,j,k)*(Y0_form_approx(i)-Y0p(i))*(Y0_form_approx(j)-Y0p(j))*(Y0_form_approx(k)-Y0p(k))
    enddo
   enddo
  enddo
 enddo

  sumtetta = 0.0
  do i = 1,n
   sumtetta = sumtetta + abs(Tetta(i))
  enddo

 if(sumtetta.le.precsn) then
  exit findmax
 endif
  
 do i = 1,n
  Y0_form_approx(i) = Y0_form_approx(i) + Tetta(i)*max_step
 enddo
 
enddo findmax


 D2Form = 0.0
 do t = 1,n
  do d = 1,n
   D2Form(t,d) = D2Form(t,d) + 2.0*D2(t,d)
   D2Form(t,d) = D2Form(t,d) + 2.0*D2per(t,d)
   do i = 1,n
    D2Form(t,d) = D2Form(t,d) + 6.0*D3(t,d,i)*(Y0_form_approx(i)-Y0(i))
    D2Form(t,d) = D2Form(t,d) + 6.0*D3per(t,d,i)*(Y0_form_approx(i)-Y0p(i))
    do j = 1,n
     D2Form(t,d) = D2Form(t,d) + 12.0*D4(t,d,i,j)*(Y0_form_approx(i)-Y0(i))*(Y0_form_approx(j)-Y0(j))
     D2Form(t,d) = D2Form(t,d) + 12.0*D4per(t,d,i,j)*(Y0_form_approx(i)-Y0p(i))*(Y0_form_approx(j)-Y0p(j))
    enddo
   enddo
  enddo
 enddo

pokazatel = 0.0

 do t = 1,n
  do i = 1,n
   pokazatel = pokazatel + D2(t,i)*(Y0_form_approx(t)-Y0(t))*(Y0_form_approx(i)-Y0(i))
   pokazatel = pokazatel + D2per(t,i)*(Y0_form_approx(t)-Y0p(t))*(Y0_form_approx(i)-Y0p(i))
   do j = 1,n
    pokazatel = pokazatel + D3(t,i,j)*(Y0_form_approx(t)-Y0(t))*(Y0_form_approx(i)-Y0(i))*(Y0_form_approx(j)-Y0(j))
    pokazatel = pokazatel + D3per(t,i,j)*(Y0_form_approx(t)-Y0p(t))*(Y0_form_approx(i)-Y0p(i))*(Y0_form_approx(j)-Y0p(j))
    do k = 1,n
     temp = D4(t,i,j,k)*(Y0_form_approx(t)-Y0(t))*(Y0_form_approx(i)-Y0(i))*(Y0_form_approx(j)-Y0(j))*(Y0_form_approx(k)-Y0(k))
     pokazatel = pokazatel + temp
     temp = D4per(t,i,j,k)*(Y0_form_approx(t)-Y0p(t))*(Y0_form_approx(i)-Y0p(i))
     pokazatel = pokazatel + temp*(Y0_form_approx(j)-Y0p(j))*(Y0_form_approx(k)-Y0p(k))
    enddo
   enddo
  enddo
 enddo
 
 contains 

integer function repeatCoef4(i,k,l,m)
 integer:: i,k,l,m
 
  if( (i.eq.k).and.(i.eq.l).and.(i.eq.m) ) then
  repeatCoef4 = 1
  return
 !2
 else if( (i.eq.k).and.(i.eq.l).and.(i.ne.m) ) then
  repeatCoef4 = 4
  return
 !3
 else if( (i.eq.k).and.(i.eq.m).and.(i.ne.l) ) then
  repeatCoef4 = 4
  return
 !4
 else if( (i.eq.l).and.(i.eq.m).and.(i.ne.k) ) then
  repeatCoef4 = 4
  return
 !5
 else if( (k.eq.l).and.(k.eq.m).and.(k.ne.i) ) then
  repeatCoef4 = 4
  return
 !6
 else if( (i.eq.k).and.(l.eq.m).and.(i.ne.l) ) then
  repeatCoef4 = 6
  return
 !7
 else if( (i.eq.k).and.(i.ne.l).and.(i.ne.m).and.(l.ne.m) ) then
  repeatCoef4 = 12
  return
 !8
 else if( (i.eq.l).and.(k.eq.m).and.(i.ne.k) ) then
  repeatCoef4 = 6
  return
 !9
 else if( (i.eq.l).and.(i.ne.k).and.(i.ne.m).and.(k.ne.m) ) then
  repeatCoef4 = 12
  return
 !10
 else if( (i.eq.m).and.(k.eq.l).and.(i.ne.k) ) then
  repeatCoef4 = 6
  return
 !11
 else if( (i.eq.m).and.(i.ne.k).and.(i.ne.l).and.(k.ne.l) ) then
  repeatCoef4 = 12
  return
 !12
 else if( (k.eq.m).and.(k.ne.l).and.(k.ne.i).and.(l.ne.i) ) then
  repeatCoef4 = 12
  return
 !13
 else if( (l.eq.m).and.(l.ne.k).and.(l.ne.i).and.(k.ne.i) ) then
  repeatCoef4 = 12
  return
 !14
 else if( (k.eq.l).and.(k.ne.m).and.(k.ne.i).and.(m.ne.i) ) then
   repeatCoef4 = 12
  return
 !15
 else if( (i.ne.k).and.(i.ne.l).and.(i.ne.m).and.(k.ne.l).and.(k.ne.m).and.(l.ne.m) ) then
  repeatCoef4 = 24
  return
 endif
 
end function repeatCoef4
 
integer function repeatCoef3(i,k,l)
 integer:: i,k,l
 
 if ((i.eq.k).and.(i.eq.l)) then      
  repeatCoef3 = 1
  return 
 else if ((i.eq.k).and.(i.ne.l)) then  
  repeatCoef3 = 3
  return 
 else if ((i.eq.l).and.(i.ne.k)) then
   repeatCoef3 = 3
  return 
 else if ((k.eq.l).and.(k.ne.i)) then  
    repeatCoef3 = 3
  return 
 else if ((i.ne.k).and.(i.ne.l)) then  
  repeatCoef3 = 6
  return 
 endif
 
 print*, "sub_maY0_form_approximize4form: BAD REPEAT COEF!"
 stop

end function repeatCoef3
 
integer function repeatCoef2(i,k)
 integer:: i,k
  if(i.eq.k) then
  repeatCoef2 = 1
  else
  repeatCoef2 = 2
  endif
  
end function repeatCoef2
 
end subroutine sub_maximize4form
